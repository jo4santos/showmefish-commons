var yargs = require('yargs');
var gulp = require('gulp');

// Build classes
var compiler = require('./build/compiler/index.js');
var AppConfigManager = compiler.AppConfigManager;
var ProjConfigManager = compiler.ProjConfigManager;
var AppCompiler = compiler.AppCompiler;
var GulpDefaultTasks = compiler.GulpDefaultTasks;

// =========================================================
// Command line argument processing
//

var argv = yargs.usage("$0 -e [live|stage|dev|...] -d")
    .alias("e", "env")
    .default('e', "stage")
    .describe("e", "The environment to build the package")
    .alias("d", "dist")
    .describe("d", "Distribution mode, js will be uglified")
    .argv;

// =========================================================
// AppCompiler (showmefish-commons) project configuration
// Overrides defaultProjConfig.js
//

var projConfig = {};

// =========================================================
// Create env variables and AppCompiler service
//

var appConfigManager = new AppConfigManager();
var appConfig = appConfigManager.getConfig(argv.e, argv.d);

var projConfigManager = new ProjConfigManager();
var projConfigCompiled = projConfigManager.getConfig(projConfig);

var appCompiler = new AppCompiler(projConfigCompiled, appConfig);
var defaultTasks = new GulpDefaultTasks(projConfigCompiled, appConfig, gulp, appCompiler);

// =========================================================
// Gulp Tasks
//

defaultTasks.registerDefaultGulpTasks(gulp);
