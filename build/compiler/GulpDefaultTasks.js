
var watch = require('gulp-watch');
var runSequence = require('run-sequence');

// NodeJS module definition:
module.exports = GulpDefaultTasks;

/**
 *
 * @param projConfig
 * @param appConfig
 * @param gulp
 * @param appCompiler
 * @constructor
 */
function GulpDefaultTasks (projConfig, appConfig, gulp, appCompiler) {
    this.projConfig = projConfig;
    this.appConfig = appConfig;
    this.compilerConfig = appConfig.compilerConfig || {};
    this.distributionMode = this.compilerConfig.distributionMode;
    this.gulp = gulp;
    this.appCompiler = appCompiler;
}

GulpDefaultTasks.prototype.registerDefaultGulpTasks = function() {
    var _this = this;
    var appCompiler = this.appCompiler;
    var gulp = this.gulp;

    gulp.task('compile', function(cb) {
        var sequence = runSequence.use(gulp);
        sequence('clean',
            'prepare-systemjs',
            ['compile-config',  'compile-app', 'compile-vendor'],
            // "karma-generate-tests" should be called before
            // "do-dist" and after "compile-app" since "do-dist" deletes src
            // folder when distMode is true but "karma-generate-tests"
            // uses a glob filter for "target/src" directory so it wont work
            'karma-generate-tests', 
            'do-dist',
            ['compile-html', 'generate-manifest-file', 'karma-generate-config'],
            'karma-tests',
            cb);
    }.bind(this));

    // Clean and config

    //just an alias maybe unnecessary
    gulp.task('clear', ['clean']);
    
    gulp.task('clean', function(){
        return appCompiler.cleanTargetFolder();
    });

    gulp.task('compile-config', function() {
        return appCompiler.compileConfig();
    });

    // App

    gulp.task('compile-app', ['compile-app-scss', 'compile-app-ts', 'compile-app-partials', 'compile-app-static']);

    gulp.task('compile-app-scss', function() {
        return appCompiler.compileAppScss();
    });
    
    gulp.task('compile-app-ts', function() {
        return appCompiler.compileAppTs();
    });

    gulp.task('compile-app-partials', function() {
        return appCompiler.compileAppPartials();
    });

    gulp.task('compile-app-static', function() {
        return appCompiler.compileAppStatic();
    });
    
    gulp.task('do-dist', function() {
        return appCompiler.doDist();
    });

    gulp.task('prepare-systemjs', function() {
        return appCompiler.prepareSystemJS();
    });
    // Vendor

    gulp.task('compile-vendor', ['compile-vendor-css', 'compile-vendor-js', 'compile-vendor-static']);

    gulp.task('compile-vendor-css', function() {
        return appCompiler.compileVendorCss();
    });

    gulp.task('compile-vendor-js', function() {
        return appCompiler.compileVendorJs();
    });

    gulp.task('compile-vendor-static', function() {
        return appCompiler.compileVendorStatic();
    });

    // Html

    gulp.task('compile-html', function() {
        return appCompiler.compileHtml();
    });

    // Lint

    gulp.task('lint', ["lint-app-ts"]);

    gulp.task('lint-app-ts', function() {
        return appCompiler.lintAppTsCode();
    });

    // Tests
    
    gulp.task('karma-generate-tests', function() {
        // If unit tests not enabled, return
        if (!_this.compilerConfig.runTests) {
            return;
        }
        return appCompiler.generateKarmaTestsFile();
    });
    
    gulp.task('karma-generate-config', function() {

        // If unit tests not enabled, return
        if (!_this.compilerConfig.runTests) {
            return;
        }

        return appCompiler.generateKarmaConfigFile();
    });

    gulp.task('karma-tests', function(cb) {

        // If unit tests not enabled, return
        if (!_this.compilerConfig.runTests) {
            return;
        }

        appCompiler.doKarmaTests(cb);
    });

    // Manifest

    gulp.task('generate-manifest-file', function() {

        // If appCache not enabled, return
        if (!_this.compilerConfig.generateAppCacheManifestFile) {
            return;
        }

        return appCompiler.generateManifestFile();
    });

    // Watch

    gulp.task('watch', function(cb){
        var sequence = runSequence.use(gulp);

        watch(appCompiler.projConfig.appSourcesScss, function(event) {
            watchLog(event);
            sequence('compile-app-scss');
        });
        watch(appCompiler.projConfig.appSourcesTs, function(event) {
            watchLog(event);
            sequence('compile-app-ts', 'karma-generate-config', 'karma-tests');
        });
        watch(appCompiler.projConfig.appSourcesStatic, function(event) {
            watchLog(event);
            sequence('compile-app-static');
        });
        watch(appCompiler.projConfig.appSourcesPartials, function(event) {
            watchLog(event);
            sequence('compile-app-partials');
        });
        watch(appCompiler.projConfig.appSourcesHtml, function(event) {
            watchLog(event);
            sequence('compile-html');
        });

        function watchLog(event) {
            console.log('File ' + event.path + ' was changed ...');
        }
    });

    gulp.task('default', ['watch']);
};
