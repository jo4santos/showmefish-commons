// Base imports
var defaultProjConfig = require('./defaultProjConfig.js');
var Extend = require('./Extend.js');

// NodeJS module definition:
module.exports = ProjConfigManager;

/**
 * ProjConfigManager accesses application config files
 *
 * @constructor
 */
function ProjConfigManager() {}

/**
 * The returned project config is the defaultProjConfig file extended with provided
 * projConfig
 *
 * @param projConfig {Object} Project Config {see defaultProjConfig}
 * @returns {*}
 */
ProjConfigManager.prototype.getConfig = function(projConfig) {
    return Extend.extend(defaultProjConfig, projConfig, true);
};
