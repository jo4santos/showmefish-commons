// Base imports
var fs = require('fs');
var jsonfile = require('jsonfile');
var colors = require('colors');

var Extend = require('./Extend.js');

// NodeJS module definition:
module.exports = AppConfigManager;

/**
 * AppConfigManager accesses application config files
 *
 * @constructor
 */
function AppConfigManager() {}

/**
 * The returned config is the common json file (/config/config.js) overridden by the
 * environment file (/config/config-$ENV.js)
 * The NODE_ENV is set with the config distribution mode (overridden by the passed
 * argument)
 *
 * @param environmentArg {string} Environment name (local, stage, live, ...)
 * @param forceDistributionModeArg {boolean} Is distribution mode forced?
 * @returns {*}
 */
AppConfigManager.prototype.getConfig = function(environmentArg, forceDistributionModeArg) {

    var config = this._readConfig(environmentArg, forceDistributionModeArg);

    if (!config.compilerConfig)
        config.compilerConfig = {};

    config.compilerConfig.env = environmentArg;

    // Argument as precedence by the config
    if (forceDistributionModeArg !== undefined) {
        config.compilerConfig.distributionMode = forceDistributionModeArg;
    } else {
        config.compilerConfig.distributionMode = Boolean(config.compilerConfig.distributionMode);
    }

    // Add package information
    config.packageInfo = {
        projectVersion: this._getProjectVersion(),
        gitRevision: this._getGitRevision()
    };

    console.info("Distribution mode is " + colors.magenta(config.compilerConfig.distributionMode));
    process.env.NODE_ENV = config.compilerConfig.distributionMode ? "production" : "development";

    return config;
};

AppConfigManager.prototype.getConfigAllEnvs = function(forceDistributionModeArg) {

    var config = {};
    config.live = this._readConfig("live", forceDistributionModeArg);

    config.compilerConfig = config.live.compilerConfig || {};

    config.appConfig = config.live.appConfig;

    // Argument as precedence by the config
    if (forceDistributionModeArg !== undefined) {
        config.compilerConfig.distributionMode = forceDistributionModeArg;
    } else {
        config.compilerConfig.distributionMode = Boolean(config.compilerConfig.distributionMode);
    }

    // Add package information
    config.packageInfo = {
        projectVersion: this._getProjectVersion(),
        gitRevision: this._getGitRevision()
    };

    console.info("Distribution mode is " + colors.magenta(config.compilerConfig.distributionMode));
    process.env.NODE_ENV = config.compilerConfig.distributionMode ? "production" : "development";

    return config;
};

AppConfigManager.prototype._readConfig = function(environment) {

    var commonCfgFilename = "config/config.json";
    var envCfgFilename = "config/config-"+environment+".json";
    var localCfgFilename = "config/config-local.json";

    var commonCfg, envCfg, localCfg, appCfg;

    console.info("Using common config " + colors.magenta(commonCfgFilename));

    try {
        commonCfg = jsonfile.readFileSync(commonCfgFilename);
        appCfg = commonCfg;
    } catch (e) {
        throw new Error(colors.red("Error: reading config file: %s", e.message), e);
    }

    console.info("Using environment config " + colors.magenta(envCfgFilename));

    try {
        envCfg = jsonfile.readFileSync(envCfgFilename);
        appCfg = Extend.extend(appCfg, envCfg, true);
    } catch (e) {
        console.log(colors.yellow("Warning: Error reading config file: ", e.message));
    }

    console.info("Overriding using local config " + colors.magenta(localCfgFilename));

    try {
        localCfg = jsonfile.readFileSync(localCfgFilename);
        appCfg = Extend.extend(appCfg, localCfg, true);
    } catch (e) {
        console.log(colors.yellow("Warning: Error reading config file: ", e.message));
    }

    return appCfg;
};

/**
 * Returns project version first from package.json, then from bower.json
 * @returns {string}
 */
AppConfigManager.prototype._getProjectVersion = function() {

    var nodeFile = "package.json";
    var bowerFile = "bower.json";
    var version;

    try {
        version = jsonfile.readFileSync(nodeFile).version;
        if (version) return version;
    } catch (err) {
        console.warn("Couldn't read " + nodeFile, err);
    }

    try {
        version = jsonfile.readFileSync(bowerFile).version;
        if (version) return version;
    } catch (err) {
        console.warn("Couldn't read " + nodeFile, err);
    }

    throw new Error(nodeFile + " or " + bowerFile + " must have a defined version");
};


/**
 * Returns last commit revision
 * @returns {string}
 */
AppConfigManager.prototype._getGitRevision = function() {
    var shaRegex = /\b[0-9a-f]{5,40}\b/i;

    try {
        var headContent = fs.readFileSync('.git/HEAD', {encoding:'utf8'}).trim();
        var origHeadContent = fs.readFileSync('.git/ORIG_HEAD', {encoding:'utf8'}).trim();
    } catch (err) {
        console.warn(colors.yellow("Couldn't read .git/HEAD nor .git/ORIG_HEAD files. Defined null revision."), err);
        return null;
    }

    if (shaRegex.test(headContent)) {
        return headContent;
    }

    else if (shaRegex.test(origHeadContent)) {
        return origHeadContent + "*";
    }

    console.warn(colors.yellow("Revisions identified on .git/HEAD and .git/ORIG_HEAD files are not in expected format. Defined null revision."));
};
