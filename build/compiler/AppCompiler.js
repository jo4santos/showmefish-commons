// Base imports
var del = require('del');
var minimatch = require("minimatch");

// Gulp imports
var gulp = require('gulp');
var debug = require('gulp-debug');
var gulpIf = require('gulp-if');
var filter = require('gulp-filter');
var concat = require('gulp-concat');
var order = require("gulp-order");
var uglify = require('gulp-uglify');
var inject = require('gulp-inject');
var rev = require("gulp-rev");
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var typescript = require('gulp-typescript');
var ngConstant = require('gulp-ng-constant');
var ngHtml2js = require('gulp-ng-html2js');
var ngAnnotate = require('gulp-ng-annotate');
var manifest = require('gulp-manifest');
var tslint = require("gulp-tslint");

var SystemJSBuilder = require('systemjs-builder');
var Q = require('q');

// Bower imports
var bowerFiles = require('main-bower-files');

// Karma server
var KarmaServer = require('karma').Server;

// NodeJS module definition:
module.exports = AppCompiler;

/**
 * Compiles the application (typescript and sass)
 *
 * @param projConfig {object} Object
 * @param appConfig {object}
 * @constructor
 */
function AppCompiler(projConfig, appConfig) {
    this.projConfig = projConfig;
    this.appConfig = appConfig;
    this.compilerConfig = appConfig.compilerConfig || {};
    this.distributionMode = this.compilerConfig.distributionMode;
    this.typescriptProject = typescript.createProject({
        target: 'ES5',
        declarationFiles: true,
        noExternalResolve: true,
        module: "system",
        experimentalDecorators: true
    });
}

/**
 *  Quality checks typescript code
 */
AppCompiler.prototype.lintAppTsCode = function() {

    return gulp.src(this.projConfig.appSourcesTs)
        .pipe(tslint())
        .pipe(tslint.report("prose", {
            emitError: false
        }));
};

/**
 *  Compile app SASS
 */
AppCompiler.prototype.compileAppScss = function(){

    var doGenerateSourceMaps = this.compilerConfig.generateSourceMaps;
    var doRev = this.distributionMode || this.compilerConfig.revFiles;

    return gulp.src(this.projConfig.appSourcesScss)

        // Sass compilation
        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.init()))
        .pipe(sass({
            sourceComments: !this.distributionMode,
            outputStyle: this.distributionMode ? 'compressed' : 'nested',
            errLogToConsole: true,
            includePaths: [
                "bower_components"
            ]}))
        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.write(".")))

        // Add fingerprints to file names
        .pipe(gulpIf(doRev, rev()))

        // Generate output
        .pipe(gulp.dest(this.projConfig.target));
};

/**
 *  Compile app typescript
 */
AppCompiler.prototype.compileAppTs = function(){
    
    return gulp.src(this.projConfig.appSourcesTs)    
        // Typescript compilation
        .pipe(typescript(this.typescriptProject))
        .pipe(ngAnnotate())
        .pipe(gulp.dest(this.projConfig.target + "/src/"));
};
/**
 * 
 * Bundles app js sources through systemjs builder
 * minifying and source maps are applied conditionally through config.json
 * 
 **/
AppCompiler.prototype.doDist = function(cb) {
    var dfdBundle = Q.defer();
    var dfdDelSrcJs = Q.defer();
    var dfdInjectModules = Q.defer();

    var distMode = this.distributionMode;
    var doSourceMaps = this.compilerConfig.generateSourceMaps;
    var doUglify = (this.distributionMode && this.compilerConfig.uglifyFilesOnDistributionMode) || this.compilerConfig.forceUglifyFiles;    
    var targetDir = this.projConfig.target;
    var outFile = targetDir + "/" + this.projConfig.appBundleJsFile;
    
    if (!distMode) {//noop when distMode is false
        var dfdDone = Q.defer();
        dfdDone.resolve();
        return dfdDone.promise;
    }
    
    var builder = new SystemJSBuilder();
    builder.loadConfig('systemjs.config.js').then(function() {
        builder.config({
            baseURL: targetDir //override baseURL to targetDir
        });
        var builderOptions = {
            minify: doUglify,
            sourceMaps: doSourceMaps
        };

        builder.bundle('src/scripts/**/*.js', outFile, builderOptions).then(function(output) {
            var src = output.source; // generated bundle source
            var maps = output.sourceMap; // generated bundle source map
            var modules = output.modules;  // array of module names defined in the bundle

            del(targetDir + '/src/').then(function() {
                dfdDelSrcJs.resolve();
            });

            //inject bundles into systemjs.config.js
            gulp.src(targetDir + "/systemjs.config.js")
                .on('data', function(chunk) {
                    var contents = String(chunk.contents);
                    var result = "\"" + modules.join("\",\n\"") + "\"" ;
                    result = "//inject:app:modules\n" + result;
                    chunk.contents = new Buffer(contents.replace("//inject:app:modules", result));
                })
                .pipe(gulp.dest(targetDir))
                .on("end", function() {
                    dfdInjectModules.resolve();
                });
            dfdBundle.resolve();
        }).catch(function(err) {
            dfdBundle.reject(err);
        });
    });    
    return Q.all([dfdBundle.promise, dfdDelSrcJs.promise, dfdInjectModules.promise]);
};
/**
 *  Compile app partials
 */
AppCompiler.prototype.compileAppPartials = function(){

    var doCompilePartials = this.distributionMode || this.compilerConfig.compilePartials;
    var doConcat = (this.distributionMode && this.compilerConfig.concatFilesOnDistributionMode) || this.compilerConfig.forceConcatFiles;
    var doRev = this.distributionMode || this.compilerConfig.revFiles;

    return gulp.src(this.projConfig.appSourcesPartials)

        // Partials to js
        .pipe(gulpIf(doCompilePartials, ngHtml2js({
            moduleName: this.projConfig.appPartialsModuleName
        })))

        // Js concat
        .pipe(gulpIf(doCompilePartials && doConcat, concat(this.projConfig.appTargetPartialsFile)))

        // Add fingerprints to file names
        .pipe(gulpIf(doCompilePartials && doRev, rev()))

        .pipe(gulp.dest(this.projConfig.target));
};

/**
 *  Compile app other assets
 */
AppCompiler.prototype.compileAppStatic = function(){

    return gulp.src(this.projConfig.appSourcesStatic)

        // Generate output
        .pipe(gulp.dest(this.projConfig.target));
};

/**
 * Compile vendor assets
 * @returns {*}
 */
AppCompiler.prototype.compileVendorJs = function() {

    // Process if in distribution mode or if in compilerConfig
    var doGenerateSourceMaps = this.compilerConfig.generateSourceMaps;
    var doUglify = (this.distributionMode && this.compilerConfig.uglifyFilesOnDistributionMode) || this.compilerConfig.forceUglifyFiles;
    var doConcat = (this.distributionMode && this.compilerConfig.concatFilesOnDistributionMode) || this.compilerConfig.forceConcatFiles;
    var doRev = this.distributionMode || this.compilerConfig.revFiles;

    var src, srcOpts;
    if (this.projConfig.vendorUseBower) {
        src = bowerFiles(this.projConfig.vendorSourcesJs);
        srcOpts = {base: 'bower_components'};
    } else {
        src = this.projConfig.vendorSourcesJs;
        srcOpts = {};
    }

    return gulp.src(src, srcOpts)

        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.init()))
        .pipe(gulpIf(doUglify, uglify({
            preserveComments: "some"
        })))
        .pipe(order(this.projConfig.vendorJsPreferredOrder))
        .pipe(gulpIf(doConcat, concat(this.projConfig.vendorTargetJsFile)))
        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.write(".")))
        .pipe(gulpIf(doRev, rev()))

        .pipe(gulp.dest(this.projConfig.vendorTarget));
};

/**
 * Compile vendor assets
 * @returns {*}
 */
AppCompiler.prototype.compileVendorCss = function() {

    var doGenerateSourceMaps = this.compilerConfig.generateSourceMaps;
    var doConcat = (this.distributionMode && this.compilerConfig.concatFilesOnDistributionMode) || this.compilerConfig.forceConcatFiles;
    var doRev = this.distributionMode || this.compilerConfig.revFiles;

    var src, srcOpts;
    if (this.projConfig.vendorUseBower) {
        src = bowerFiles(this.projConfig.vendorSourcesCss);
        srcOpts = {base: 'bower_components'};
    } else {
        src = this.projConfig.vendorSourcesCss;
        srcOpts = {};
    }

    return gulp.src(src, srcOpts)

        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.init()))
        .pipe(order(this.projConfig.vendorCssPreferredOrder))
        .pipe(gulpIf(doConcat, concat(this.projConfig.vendorTargetCssFile)))
        .pipe(gulpIf(doGenerateSourceMaps, sourcemaps.write(".")))
        .pipe(gulpIf(doRev, rev()))

        .pipe(gulp.dest(this.projConfig.vendorTarget));
};

/**
 * Compile vendor assets
 * @returns {*}
 */
AppCompiler.prototype.compileVendorStatic = function() {

    var src, srcOpts;
    if (this.projConfig.vendorUseBower) {
        src = bowerFiles(this.projConfig.vendorSourcesStatic);
        srcOpts = {base: 'bower_components'};
    } else {
        src = this.projConfig.vendorSourcesStatic;
        srcOpts = {};
    }

    return gulp.src(src, srcOpts)

        .pipe(gulp.dest(this.projConfig.vendorTarget));
};

/**
 * Compile application AppConfig.js, based on json.config file
 */
AppCompiler.prototype.compileConfig = function() {
    var doRev = this.distributionMode;

    return gulp.src('config/config.json')
        .pipe(ngConstant({
            name: this.projConfig.appConfigModuleName,
            constants: this.appConfig
        }))
        .pipe(gulpIf(doRev, rev()))
        // Writes config.js to dist/ folder
        .pipe(gulp.dest(this.projConfig.target));
};

AppCompiler.prototype.prepareSystemJS = function() {

    var dfdConf = Q.defer();
    var dfdLib = Q.defer();

    gulp.src('systemjs.config.js')
        .pipe(gulp.dest(this.projConfig.target))
        .on("end", function() {
            dfdConf.resolve();
        });
    gulp.src(['bower_components/system.js/dist/*.js', '!bower_components/system.js/dist/*.src.js'], {base: 'bower_components/'})
        .pipe(gulp.dest(this.projConfig.vendorTarget))
        .on("end", function() {
            dfdLib.resolve();
        });
    return Q.all([dfdConf.promise, dfdLib.promise]);
};

/**
 * Generate application html file
 */
AppCompiler.prototype.compileHtml = function() {

    var streamOpts = {read: false};

    // app css files
    var cssAppStream =
        gulp.src([
            this.projConfig.target + '/**/*.css',
            "!" + this.projConfig.vendorTarget + '/**/*.css'
        ], streamOpts)
            .pipe(order(this.projConfig.appCssPreferredOrder || []));

    // vendor javascript files
    var jsVendorStream =
        gulp.src([
            this.projConfig.vendorTarget + '/**/*.js',
            "!" + this.projConfig.vendorTarget + '/system.js',
            "!" + this.projConfig.vendorTarget + '/system.js/**/*.js'
        ], streamOpts)
            .pipe(order(this.projConfig.vendorJsPreferredOrder || []));

    // vendor css files
    var cssVendorStream =
        gulp.src([
            this.projConfig.vendorTarget + '/**/*.css'
        ], streamOpts)
            .pipe(order(this.projConfig.vendorCssPreferredOrder || []));

    var smfConfigStream = gulp.src(this.projConfig.target + "/config*.js");
    var smfPartialsStream = gulp.src(this.projConfig.target + "/dist/app-partials*.js");
    // inject index.html
    return gulp.src(this.projConfig.appSourcesHtml)
        .pipe(inject(cssAppStream, {
            starttag: '<!-- inject:app:css -->',
            ignorePath: this.projConfig.target,
            addRootSlash: false,
            transform: cssAlternateTransformer.bind(this, this.projConfig.appCssAlternateFiles)
        }))
        .pipe(inject(jsVendorStream, {
            starttag: '<!-- inject:vendor:js -->',
            ignorePath: this.projConfig.target,
            addRootSlash: false
        }))
        .pipe(inject(cssVendorStream, {
            starttag: '<!-- inject:vendor:css -->',
            ignorePath: this.projConfig.target,
            addRootSlash: false
        }))
        .pipe(inject(smfConfigStream, {
            starttag: '<!-- inject:smfConfig -->',
            ignorePath: this.projConfig.target,
            addRootSlash: false
        }))
        .pipe(inject(smfPartialsStream, {
            starttag: '<!-- inject:smfPartials -->',
            ignorePath: this.projConfig.target,
            addRootSlash: false
        }))

        .pipe(gulp.dest(this.projConfig.target));
};

/**
 *
 * Generate karma tests file 
 *
 */
AppCompiler.prototype.generateKarmaTestsFile = function() {

    function sysjsItemPathInjector(filepath, file, i, len) {
        var trailingComma = ( i < (len-1) ) ? "," : "";
        return 'System.import("' + filepath + '")' + trailingComma;
    };

    //spec files to be injected as System.import statements
    var jsSpecsStream = gulp.src([
                this.projConfig.target + '/src/**/*spec.js'
            ], {read: false});

    //generate karma.tests.js
    return gulp.src('karma.tests.js')
        .pipe(inject(
            jsSpecsStream,
            {
                starttag: '// inject:specs:js',
                endtag: '// endinject',
                ignorePath: this.projConfig.target,
                addRootSlash: false,
                transform: sysjsItemPathInjector
            }
        ))
        .pipe(gulp.dest(this.projConfig.target));
};

/**
 * Generate karma config file with bower dependencies
 *
 * @see https://github.com/drewhamlett/gulp-angular-starter/blob/master/Gulpfile.js
 */
AppCompiler.prototype.generateKarmaConfigFile = function() {

    var streamOpts = {read: false};

    // sysjs config and karma tests to be included as script tags
    var jsAppStream =
        gulp.src([
                this.projConfig.target + '/systemjs.config.js',
                this.projConfig.target + '/karma.tests.js'
            ], streamOpts);

    // vendor javascript files
    var jsVendorStream =
        gulp.src([
                this.projConfig.vendorTarget + '/**/*.js',
                "!" + this.projConfig.vendorTarget + '/system.js/'
            ], streamOpts)
            .pipe(order(this.projConfig.vendorJsPreferredOrder || []));

    //generate karma.conf.js
    return gulp.src('karma.conf.js')
        .pipe(inject(
            jsAppStream,
            {
                starttag: '// inject:app:js',
                endtag: '// endinject',
                ignorePath: this.projConfig.target,
                addRootSlash: false,
                transform: jsListItemPathInjector
            }
        ))
        .pipe(inject(
            jsVendorStream,
            {
                starttag: '// inject:vendor:js',
                endtag: '// endinject',
                ignorePath: this.projConfig.target,
                addRootSlash: false,
                transform: jsListItemPathInjector
            }
        ))
        .pipe(gulp.dest(this.projConfig.target));

    function jsListItemPathInjector(filepath, file, i, len) {
        return '"' + filepath + '",';
    };
};

/**
 * Do karma tests
 */
AppCompiler.prototype.doKarmaTests = function(cb) {

    new KarmaServer({
        configFile: process.cwd() + '/' + this.projConfig.target + '/karma.conf.js',
        singleRun: true,
        autoWatch: false
    }, function(){
        cb();
    }).start();
};

/**
 * Generate manifest file to run HTML application in offline mode
 */
AppCompiler.prototype.generateManifestFile = function() {

    return gulp.src(this.projConfig.appCache.sources)
        .pipe(manifest(this.projConfig.appCache.manifestOptions))
        .pipe(gulp.dest(this.projConfig.target));
};

/**
 * Clean target folder
 */
AppCompiler.prototype.cleanTargetFolder = function() {
    return del([this.projConfig.target]);
};

/**
 * Creates alternate stylesheet declaration, with key name, if some of cssAlternateFiles object
 * values match the filepath
 *
 * @param cssAlternateFiles
 * @param filepath
 * @param file
 * @param index
 * @param length
 * @param contents
 * @returns string
 */
function cssAlternateTransformer(cssAlternateFiles, filepath, file, index, length, contents) {

    var alternateName = false;

    for (var key in cssAlternateFiles) {
        if (cssAlternateFiles.hasOwnProperty(key)) {

            var pattern = cssAlternateFiles[key];
            var result = minimatch(filepath, pattern);

            if (result) {
                alternateName = key;
                break;
            }

        }
    }

    // It's an alternate CSS file
    if (alternateName) {
        return "<link rel='alternate stylesheet' id='"+alternateName+"' title='"+alternateName+"' href='"+filepath+"' >";
    }

    // Else ... It's a standard CSS file
    return inject.transform.apply(inject.transform, [filepath, file, index, length, contents]);
}
