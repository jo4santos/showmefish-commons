
var Extend = {

    /**
     * Extend target object with source properties
     * Similar to jQuery.extend(true, target, source), but overwrites arrays instead of extending them
     *
     * @param target
     * @param source
     * @param deep
     *
     * @returns {target}
     */
    extend: function (target, source, deep) {

        if (!target || !source)
            return source;

        if (Array.isArray(target))
            return source;

        var result = {};

        // Clone target to result
        Object.keys(target || {}).map(function (prop) {
            if (Object.prototype.hasOwnProperty.call(target, prop)) {
                result[prop] = target[prop];
            }
        });

        // Extend with source properties
        Object.keys(source || {}).map(function (prop) {

            if (Object.prototype.hasOwnProperty.call(source, prop)) {

                var targetValue = target[prop];
                var sourceValue = source[prop];

                // If deep copy and value is an object, call extend again
                if (deep && typeof sourceValue == "object") {
                    result[prop] = Extend.extend(targetValue, sourceValue, deep);
                }

                // Just copy the value
                else {
                    result[prop] = sourceValue;
                }
            }

        });

        return result;
    }
};

module.exports = Extend;