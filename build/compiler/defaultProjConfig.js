module.exports = {

    //
    // Generated modules names
    //

    // Partials module name
    "appPartialsModuleName": "tcPartials",

    // Partials module name
    "appConfigModuleName": "tcConfig",

    //
    // Compilation dirs
    //

    // The build directory root
    "target": "target",

    // The build directory for vendor assets
    "vendorTarget": "target/vendor",

    //
    // App configs
    //

    "appSources": "src/scripts/**/*.*",

    "appSourcesScss": [
        "src/scripts/**/*.scss",
        "src/styles/**/*.scss"
    ],

    "appSourcesTs": [
        "bower_components/web-commons/src/scripts/**/*.ts",
        "src/scripts/**/*.ts"
    ],

    // Which app JS files should be transformed? (uglified, concated, rev)
    "appSourcesJsTransformed": [
        "**/*.js",
        "!**/*.local.js"
    ],

    "appBundleJsFile": "dist/app.js",

    "appSourcesPartials": [
        "src/**/partials/*.html",
        "src/**/view/*.html"
    ],

    "appTargetPartialsFile": "dist/app-partials.js",

    "appSourcesStatic": [
        "src/**/*.*",
        "src/**/.*", // For .htaccess
        "bower_components/web-commons/src/**/*.*",
        "!**/*.scss",
        "!**/*.ts",
        "!**/partials/*.html",
        "!**/view/*.html"
    ],

    "appSourcesHtml": [
        "src/index.html"
    ],

    // There are alternate CSS files?
    "appCssAlternateFiles": {
        // "base": "**/base/index*.css",
        // "contactive": "**/contactive/index*.css",
        // "tpn": "**/tpn/index*.css"
    },

    // The order that the application JS files should be imported or concatenated
    "appJsPreferredOrder": [
        'lib/**/*',
        'partials/**/*',
        'scripts/config*.js',

        // Model and Util
        "scripts/tpn/commons/**/util/**/*",
        'scripts/tpn/commons/shared/model/**/*',
        'scripts/tpn/commons/messaging/origin/model/OriginId.js',
        'scripts/tpn/commons/messaging/origin/model/**/*',
        'scripts/tpn/commons/messaging/address/model/OriginAddressTypeEnum.js',
        'scripts/tpn/commons/messaging/address/model/OriginAddress.js',
        'scripts/tpn/commons/messaging/address/model/**/*',
        'scripts/tpn/commons/messaging/message/model/content/**/*',
        'scripts/tpn/commons/messaging/message/model/**/*',
        'scripts/tpn/commons/messaging/conversation/model/ConversationTypeEnum.js',
        'scripts/tpn/commons/messaging/conversation/model/Conversation.js',
        'scripts/tpn/commons/messaging/conversation/model/ConversationList.js',
        'scripts/tpn/commons/messaging/conversation/model/**/*',
        'scripts/tpn/commons/messaging/plugin/xmpp/model/XmppJidOriginId.js',
        'scripts/tpn/commons/messaging/plugin/xmpp/model/XmppMessageHeader.js',
        'scripts/tpn/commons/messaging/plugin/messageSync/model/UcOriginId.js',
        'scripts/tpn/commons/messaging/plugin/messageSync/model/UcMessageHeader.js',
        'scripts/tpn/commons/messaging/plugin/callEvent/model/CallEventOriginId.js',
        'scripts/tpn/commons/messaging/plugin/callEvent/model/CallEventMessageHeader.js',
        'scripts/tpn/commons/messaging/plugin/voicemail/model/VoicemailOriginId.js',
        'scripts/tpn/commons/messaging/plugin/voicemail/model/VoicemailMessageHeader.js',
        'scripts/tpn/commons/**/model/**/*',

        // Exceptions
        'scripts/tpn/commons/shared/exception/Exception.js', // Extended by other exceptions
        "scripts/tpn/commons/shared/exception/**/*",
        "scripts/tpn/commons/**/exception/**/*",

        // Configs, interfaces and model converters
        "scripts/tpn/commons/**/config/**/*",
        "scripts/tpn/commons/**/listener/**/*",
        "scripts/tpn/commons/**/converter/**/*",

        // APIs
        'scripts/tpn/commons/ucApi/shared/api/WebsocketStatus.js', // Used by WsApiClient
        'scripts/tpn/commons/**/api/ApiClient.js', // Used by all APIs
        'scripts/tpn/commons/**/api/WsApiClient.js', // Used by all APIs
        'scripts/tpn/commons/contactiveApi/item/api/**/*', // ContactItemApi extends ItemApi
        'scripts/tpn/commons/contactiveApi/item/service/**/*', // ContactItemService extends ItemService
        "scripts/tpn/commons/**/api/**/*",

        // Datasources
        "scripts/tpn/commons/**/datasource/**/*",

        // Services, controllers and resolvers
        "scripts/tpn/commons/**/service/**/*",
        "scripts/tpn/commons/**/controller/**/*",
        "scripts/tpn/commons/**/resolver/**/*",

        // Others
        "scripts/tpn/commons/**/directive/**/*",
        "scripts/tpn/commons/**/filter/**/*",
        "scripts/tpn/commons/**/*"
    ],

    // The order that the application CSS files should be imported or concatenated
    "appCssPreferredOrder": [],

    //
    // Vendor configs
    //

    "vendorUseBower": true,

    "vendorSourcesJs": [
        "**/*.js",
        "!**/system.js"
    ],

    "vendorSourcesCss": [
        "**/*.css"
    ],

    "vendorSourcesStatic": [
        "**/*.*",
        "!**/*.js",
        "!**/*.css"
    ],

    "vendorTargetJsFile": "vendor.js",
    "vendorTargetCssFile": "vendor.css",

    // The order that the vendor JS files should be imported or concatenated
    "vendorJsPreferredOrder": [
        "jquery/**",
        "lodash/**",
        "angular/**"
    ],

    // The order that the vendor CSS files should be imported or concatenated
    "vendorCssPreferredOrder": [],

    //
    // Offline HTML5 mode configs
    //

    "appCache": {

        // Files that will be part of app.manifest file
        "sources": ["target/**/*"],

        // Options provided to gulp-manifest plugin
        "manifestOptions": {
            hash: true,
            preferOnline: true,
            network: ['http://*', 'https://*', '*'],
            filename: 'app.manifest',
            exclude: 'app.manifest'
        }
    }
};
