"use strict"

module tpn.commons.filter {

    /**
     * Allow html content
     *
     * <any>{{someHtmlVariable | tc-unsafe}}</any>
     *
     */
    export var UnsafeFilterFactory = ["$sce", UnsafeFilter];
    function UnsafeFilter(
        $sce: ng.ISCEService
    ) {
        return $sce.trustAsHtml;
    }

}