module tpn.commons.filter {

    "use strict"

    /**
     * Convert seconds into datetime format to provide formating using angular date filter.
     *
     * <any>{{someHtmlVariable | tcSecondsToDateTime | date: 'mm:ss'}}</any>
     *
     */
    export var SecondsToDateTimeFilterFactory = [SecondsToDateTimeFilter];
    function SecondsToDateTimeFilter() {
        return (seconds) => {
            return new Date(1970, 0, 1).setSeconds(seconds);
        };
    }

}