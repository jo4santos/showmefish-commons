module tpn.commons.filter {

    "use strict";

    /**
     * Register tcActiveLink module
     */
    function registerAngularModule() {
        angular
            .module("tcFilters", [])
            .filter("tcUnsafe", UnsafeFilterFactory)
            .filter("tcSecondsToDateTime", SecondsToDateTimeFilterFactory);
    }

    angular.element(document).ready(registerAngularModule);
}
