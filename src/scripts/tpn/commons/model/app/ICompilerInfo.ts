"use strict";

module tpn.commons.model.app {

    /**
     * Application device configuration
     */
    export interface ICompilerConfig {

        distributionMode: boolean;
        uglifyFilesOnDistributionMode: boolean;
        concatFilesOnDistributionMode: boolean;
        forceUglifyFiles: boolean;
        forceConcatFiles: boolean;
        generateSourceMaps: boolean;
        compilePartials: boolean;
        revFiles: boolean;
        env: string;

        /**
         * HTML5 offline application enabled?
         */
        generateAppCacheManifestFile: boolean;
    }

}
