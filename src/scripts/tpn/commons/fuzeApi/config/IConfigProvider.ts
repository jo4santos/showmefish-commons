"use strict";

module tpn.commons.fuzeApi.config {

    export interface IConfigProvider extends ng.IServiceProvider {
        getToken():string;
        getApiEndpoint():string;
        getJoinEndpoint():string;
    }
}
