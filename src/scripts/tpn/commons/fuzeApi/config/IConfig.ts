"use strict";

module tpn.commons.fuzeApi.config {
    export interface IConfig {
        apiEndpoint: string;
        joinEndpoint: string;
        token: string;
    }
}
