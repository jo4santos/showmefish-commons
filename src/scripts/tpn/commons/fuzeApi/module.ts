module tpn.commons.fuzeApi {

    "use strict";

    /**
     * Register FuzeApi module
     */
    function registerAngularModule() {
        angular
            .module("tcFuzeApi", ["restangular"])

            .service("tcFuzeApiClient", api.ApiClient)

            .service("tcFuzeApiAuthApi", auth.api.AuthApi)
            .service("tcFuzeApiMeetingsApi", meetings.api.MeetingsApi)

            .service("tcFuzeApiFuzeService", service.FuzeService)
    }

    angular.element(document).ready(registerAngularModule);
}

