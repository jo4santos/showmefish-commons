"use strict";

module tpn.commons.fuzeApi.service {

    // tpn.commons.wardenApi
    import IUserAuthorizationPresenter          = tpn.commons.wardenApi.users.model.IUserAuthorizationPresenter;

    // tpn.commons.fuzeApi
    import AuthApi      = tpn.commons.fuzeApi.auth.api.AuthApi;
    import MeetingsApi  = tpn.commons.fuzeApi.meetings.api.MeetingsApi;
    import IMeeting     = tpn.commons.fuzeApi.meetings.model.IMeeting;
    import IAuthWardenSessionsResponse  = tpn.commons.fuzeApi.auth.api.IAuthWardenSessionsResponse;

    export class FuzeService {

        protected authApi:AuthApi;
        protected meetingsApi:MeetingsApi;

        public static $inject = [
            "tcFuzeApiAuthApi",
            "tcFuzeApiMeetingsApi"
        ];

        public constructor(
            authApi:AuthApi,
            meetingsApi:MeetingsApi
        ) {
            this.authApi = authApi;
            this.meetingsApi = meetingsApi;
        }

        /**
         * Gets a fuze token based on user credentials
         * @param username
         * @param password
         * @returns {IPromise<IUserAuthorizationPresenter>}
         */
        public getToken(email: string, password: string):ng.IPromise<string> {
            return this.authApi.postAuthLogin(email,password);
        }

        /**
         * Exchanges the warden data for a fuze token
         * @param userAuthorizationPresenter
         * @returns {ng.IPromise<string>}
         */
        public getTokenWithWarden(userAuthorizationPresenter: IUserAuthorizationPresenter):ng.IPromise<string> {
            return this.authApi.postAuthWardenSessions(userAuthorizationPresenter).then((response:IAuthWardenSessionsResponse) => {
                return response.token;
            });
        }

        /**
         * Get the list of fuze meetings
         * @returns {ng.IPromise<IMeeting[]>}
         */
        public getMeetingsList(): ng.IPromise<IMeeting[]> {
            return this.meetingsApi.getList();
        }

        /**
         * Create a meeting with start time "now".
         * @returns {ng.IPromise<IMeeting>}
         */
        public instantMeeting(subject?: string): ng.IPromise<IMeeting> {
            var startDate: Date = new Date();
            var startTime: string = tpn.commons.shared.util.DateUtils.toISODateString(startDate);
            return this.meetingsApi.create(startTime, subject);
        }

        /**
         * Delete a meeting. (Sets status to deleted)
         * @returns {ng.IPromise<any>}
         */
        public removeMeeting(id: string): ng.IPromise<any> {
            return this.meetingsApi.remove(id);
        }
    }
}