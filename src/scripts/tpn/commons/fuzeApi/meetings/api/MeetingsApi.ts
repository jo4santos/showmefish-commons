"use strict";

module tpn.commons.fuzeApi.meetings.api {

    import UrlEncodedUtils              = tpn.commons.shared.util.UrlEncodedUtils;

    import ApiClient                    = tpn.commons.fuzeApi.api.ApiClient;
    import IMeeting                     = tpn.commons.fuzeApi.meetings.model.IMeeting;

    /**
     * API for /mettings endpoint
     */

    export class MeetingsApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcFuzeApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        public create(startTime: string, subject: string = "New meeting"):ng.IPromise<IMeeting> {
            return this.apiClient.get()
                .one("")
                .post("meetings",UrlEncodedUtils.toQuery({"start_time":startTime, "subject": subject}));
        }


        /**
         * Gets the list of meetings for the authenticated user
         */
        public getList():ng.IPromise<IMeeting[]> {
            return this.apiClient.get()
                .one("meetings")
                .get({"status":""})
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }

        public remove(id: string):ng.IPromise<any> {
            return this.apiClient.get()
                .one("meetings", id)
                .remove();
        }
    }
}