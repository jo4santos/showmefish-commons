"use strict";

module tpn.commons.fuzeApi.meetings.model {

    export interface IMeeting {
        id: string;
        subject: string;

        /**
         * Start time of the meeting (RFC 3339. Ex: 2015-12-24T10:30:00Z)
         */
        start_time: string;
    }

}