"use strict";

module tpn.commons.fuzeApi.auth.api {

    export interface IAuthWardenSessionsResponse {
        token: string;
    }

}