"use strict";

module tpn.commons.fuzeApi.auth.api {

    // tpn.commons.shared
    import UrlEncodedUtils              = tpn.commons.shared.util.UrlEncodedUtils;

    // tpn.commons.wardenApi
    import IUserAuthorizationPresenter          = tpn.commons.wardenApi.users.model.IUserAuthorizationPresenter;

    // tpn.commons.fuzeApi
    import ApiClient                    = tpn.commons.fuzeApi.api.ApiClient;
    import IAuthWardenSessionsResponse  = tpn.commons.fuzeApi.auth.api.IAuthWardenSessionsResponse;

    /**
     * API for /auth endpoint
     */

    export class AuthApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcFuzeApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Gets a fuze token based on user credentials
         *
         * @param username
         * @param password
         * @returns {string}
         */
        public postAuthLogin(email:string,password: string):ng.IPromise<string> {
            return this.apiClient.get()
                .one("auth")
                .post("login",UrlEncodedUtils.toQuery({"email":email,"password":password}));
        }

        /**
         * Exchanges a warden token for a fuze token
         * @param userAuthorizationPresenter
         * @returns {IPromise<any>}
         */
        public postAuthWardenSessions(userAuthorizationPresenter:IUserAuthorizationPresenter):ng.IPromise<IAuthWardenSessionsResponse> {
            return this.apiClient.get()
                .one("auth")
                .one("warden")
                .post("sessions",UrlEncodedUtils.toQuery({"token":userAuthorizationPresenter.grant.token}));
        }
    }
}