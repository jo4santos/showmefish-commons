"use strict";

module tpn.commons.citadelApi.users.model {

    export interface IXmppAccountSettings {
        username: string;
        password: string;
        serverUrl: string;
    }

}