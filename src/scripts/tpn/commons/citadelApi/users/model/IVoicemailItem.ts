"use strict";

module tpn.commons.citadelApi.users.model {

    import IItem                    = tpn.commons.citadelApi.shared.model.IItem;
    import IVoicemailItemPayload    = tpn.commons.citadelApi.users.model.IVoicemailItemPayload;

    export interface IVoicemailItem extends IItem {
        payload: IVoicemailItemPayload;
    }

}