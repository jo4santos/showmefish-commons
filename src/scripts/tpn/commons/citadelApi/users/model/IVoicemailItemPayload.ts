"use strict";

module tpn.commons.citadelApi.users.model {

    export interface IVoicemailItemPayload {
        name: string;
        callerId: string;
        date: number;
        duration: number;
        read: boolean;
    }

}