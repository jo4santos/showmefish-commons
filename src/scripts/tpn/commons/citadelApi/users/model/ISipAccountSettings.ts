"use strict";

module tpn.commons.citadelApi.users.model {

    export interface ISipAccountSettings {
        username: string;
        password: string;
        sipDomain: string;
        enableSRTP: boolean;
    }

}