"use strict";

module tpn.commons.citadelApi.users.model {

    export interface IUserAccountSettings {
        extension: string;
    }

}