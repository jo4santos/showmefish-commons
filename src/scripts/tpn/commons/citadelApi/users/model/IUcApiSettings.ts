"use strict";

module tpn.commons.citadelApi.users.model {

    export interface IUcApiSettings {
        username: string;
        password: string;
        restUrl: string;
        websocketUrl: string;
    }

}
