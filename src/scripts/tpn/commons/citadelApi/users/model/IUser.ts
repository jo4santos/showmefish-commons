"use strict";

module tpn.commons.citadelApi.users.model {

    import IStringKeyable   = tpn.commons.shared.model.IStringKeyable;
    import IUserDevice      = tpn.commons.citadelApi.shared.model.IUserDevice;

    export interface IUser {
        userId: string;
        username: IStringKeyable;
        email: IStringKeyable;
        userDevices: Array<IUserDevice>;
    }

}