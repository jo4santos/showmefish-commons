"use strict";

module tpn.commons.citadelApi.users.api {

    import ApiClient                    = tpn.commons.citadelApi.api.ApiClient;
    import IUsersProvisioningOptions    = tpn.commons.citadelApi.users.api.IUsersProvisioningOptions;
    import IUsersProvisioningResponse   = tpn.commons.citadelApi.users.api.IUsersProvisioningResponse;

    /**
     * API for /users endpoint
     */

    export class UsersApi {

        public static VOICEMAIL_DOWNLOAD_TYPE: string = 'audio/x-wav';

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcCitadelApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Get provisioning information
         *
         * @params options Contains unique user identification and information regarding the device
         * @returns {IPromise<IUsersProvisioningResponse>}
         */
        public getProvisioning(options?: IUsersProvisioningOptions):ng.IPromise<IUsersProvisioningResponse> {

            var options: IUsersProvisioningOptions = options || <IUsersProvisioningOptions>{};
            options.userId = options.userId || "me";
            options.deviceId = options.deviceId || "current";

            return this.apiClient.get().all("users")
                .one(options.userId)
                .one("devices")
                .one(options.deviceType)
                .one(options.deviceId)
                .one("options")
                .get()
                .then((data:any)=> {
                    if (data && data.data) return data.data.plain();
                })
        }

        public getVoicemails(options?: IUsersVoicemailsOptions):ng.IPromise<IUsersVoicemailsResponse> {

            var options: IUsersVoicemailsOptions = options || <IUsersVoicemailsOptions>{};
            options.userId = options.userId || "me";

            return this.apiClient.get().all("users")
                .one(options.userId)
                .one("voicemails")
                .get()
                .then((data:any)=> {
                    if (data && data.data) return data.data.plain();
                })
        }

        public downloadVoicemail(itemId: number):ng.IPromise<Blob> {

            return this.apiClient.get().all("users")
                .one("me")
                .one("voicemails")
                .one(itemId.toString())
                .one("download")
                .get()
                .then((data:any) => {

                    var dataString: string = data.data;
                    var dataLength: number = dataString.length;
                    var dataArray: Uint8Array = new Uint8Array(dataLength);
                    for (var i = 0; i < dataLength; i++){
                        dataArray[i] = dataString.charCodeAt(i);
                    }

                    var dataBlob: Blob = new Blob([dataArray], {type: tpn.commons.citadelApi.users.api.UsersApi.VOICEMAIL_DOWNLOAD_TYPE});

                    return dataBlob;
                });
        }
    }
}