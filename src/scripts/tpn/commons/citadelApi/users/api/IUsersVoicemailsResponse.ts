"use strict";

module tpn.commons.citadelApi.users.api {

    import IVoicemailItem = tpn.commons.citadelApi.users.model.IVoicemailItem;

    export interface IUsersVoicemailsResponse {
        items: IVoicemailItem[];
    }

}