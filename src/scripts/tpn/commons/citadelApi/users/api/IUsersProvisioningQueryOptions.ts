"use strict";

module tpn.commons.citadelApi.users.api {

    export interface IUsersProvisioningOptions {
        userId?:string;
        deviceId?: string;
        deviceType?: string;
    }

}