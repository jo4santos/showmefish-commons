"use strict";

module tpn.commons.citadelApi.users.api {

    import ISipAccountSettings  = tpn.commons.citadelApi.users.model.ISipAccountSettings;
    import IUserAccountSettings = tpn.commons.citadelApi.users.model.IUserAccountSettings;
    import IXmppAccountSettings = tpn.commons.citadelApi.users.model.IXmppAccountSettings;
    import IUcApiSettings = tpn.commons.citadelApi.users.model.IUcApiSettings;

    export interface IUsersProvisioningResponse {
        userAccountSettings: IUserAccountSettings;
        sipAccountSettings: ISipAccountSettings[];
        xmppAccountSettings: IXmppAccountSettings;
        ucApiSettings: IUcApiSettings;
    }
}