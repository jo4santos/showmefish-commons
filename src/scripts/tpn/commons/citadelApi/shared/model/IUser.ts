"use strict";

module tpn.commons.citadelApi.shared.model {

    // tpn.commons.shared
    import IStringKeyable   = tpn.commons.shared.model.IStringKeyable;

    // tpn.commons.citadelApi
    import IUserDevice      = tpn.commons.citadelApi.shared.model.IUserDevice;

    export interface IUser {
        userId: string;
        username: IStringKeyable;
        email: IStringKeyable;
        userDevices: Array<IUserDevice>;
    }

}