"use strict";

module tpn.commons.citadelApi.shared.model {

    export interface IUserDevice {
        clientId: string;
        deviceType: string;
    }

}