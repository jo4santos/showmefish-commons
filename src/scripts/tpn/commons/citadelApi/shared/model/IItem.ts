"use strict";

module tpn.commons.citadelApi.shared.model {

    export interface IItem {
        itemId: number;
        revision: number;
        deleted: boolean;
    }

}