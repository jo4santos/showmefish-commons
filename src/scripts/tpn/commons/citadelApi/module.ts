module tpn.commons.citadelApi {

    "use strict";

    /**
     * Register CitadelApi module
     */
    function registerAngularModule() {
        angular
            .module("tcCitadelApi", ["restangular"])

            .service("tcCitadelApiClient", api.ApiClient)

            .service("tcCitadelApiAuthApi", auth.api.AuthApi)
            .service("tcCitadelApiUsersApi", users.api.UsersApi)
            .service("tcCitadelApiApplicationsApi", applications.api.ApplicationsApi)

            .service("tcCitadelApiCitadelService", shared.service.CitadelService)

            .service("tcCitadelApiTestApi", test.api.TestApi)
    }

    angular.element(document).ready(registerAngularModule);
}

