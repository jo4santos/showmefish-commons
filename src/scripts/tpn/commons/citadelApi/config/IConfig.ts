"use strict";

module tpn.commons.citadelApi.config {
    export interface IConfig {
        accessToken: string;
        apiEndpoint: string;
        appToken: string;
    }
}
