"use strict";

module tpn.commons.citadelApi.config {

    import IUserDevice = tpn.commons.citadelApi.shared.model.IUserDevice;

    export interface IConfigProvider extends ng.IServiceProvider {
        getAccessToken():string;
        getAppToken():string;
        getApiEndpoint():string;
        getUserDevice():IUserDevice;
    }
}
