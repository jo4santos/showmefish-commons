"use strict";

module tpn.commons.citadelApi.applications.model {

    export interface IUpdateInfo {
        id:string;
        version:string;
        payload:IUpdateInfoPayload;
    }

    export interface IUpdateInfoPayload {
        platform:string;
        platformVersion:string;
        installers:IUpdateInfoPayloadInstallers;
        updates:IUpdateInfoPayloadUpdates
    }

    export interface IUpdateInfoPayloadInstallers {
        win: string;
        osx: string;
    }

    export interface IUpdateInfoPayloadUpdates {
        win:IUpdateInfoPayloadUpdatesElement;
        osx:IUpdateInfoPayloadUpdatesElement;
    }

    export interface IUpdateInfoPayloadUpdatesElement {
        binariesUrl: string;
        staticsUrl: string;
    }
}