"use strict";

module tpn.commons.citadelApi.applications.api {

    import ApiClient                    = tpn.commons.citadelApi.api.ApiClient;
    import IApplicationsUpdatesOptions  = tpn.commons.citadelApi.applications.api.IApplicationsUpdatesOptions;
    import IUpdateInfo                  = tpn.commons.citadelApi.applications.model.IUpdateInfo;

    /**
     * API for /applications endpoint
     */

    export class ApplicationsApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcCitadelApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Get update information
         *
         * @params options
         * @returns {IPromise<IUpdateInfo>}
         */
        public getUpdates(options: IApplicationsUpdatesOptions):ng.IPromise<IUpdateInfo> {

            if(!options || !options.deviceType || !options.version) {
                return null;
            }

            return this.apiClient.get().all("applications")
                .one(options.deviceType)
                .one(options.version)
                .one("updates")
                .get()
                .then((data:any)=> {
                    if (data && data.data) return data.data.plain();
                })
        }
    }
}