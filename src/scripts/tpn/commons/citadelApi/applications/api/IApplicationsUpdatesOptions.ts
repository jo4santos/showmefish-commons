"use strict";

module tpn.commons.citadelApi.applications.api {

    export interface IApplicationsUpdatesOptions {
        deviceType: string;
        version: string;
    }

}