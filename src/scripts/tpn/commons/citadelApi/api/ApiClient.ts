module tpn.commons.citadelApi.api {

    "use strict";

    // restangular
    import IResponse    = restangular.IResponse;
    import IService     = restangular.IService;
    import IProvider    = restangular.IProvider;

    // tpn.commons.citadelApi
    import IConfigProvider = tpn.commons.citadelApi.config.IConfigProvider;

    export class ApiClient {

        public static CONTENT_TYPE_JSON = 'application/json';

        protected $log:ng.ILogService;

        protected restangular:IService;
        protected configProvider:IConfigProvider;
        protected tipiSessionId: string;

        public static $inject = ["$log", "Restangular", "citadelApiConfig"];

        public constructor($log:ng.ILogService,
                           restangular:IService,
                           configProvider:IConfigProvider) {
            this.$log = $log;
            this.restangular = restangular;
            this.configProvider = configProvider;
        }

        //
        // Public API
        //

        public get() {
            return this.restangular.withConfig(this.config.bind(this));
        }

        //
        // Misc
        //

        private config(provider:IProvider) {
            var token = this.configProvider.getAccessToken() || this.configProvider.getAppToken();

            provider.setFullResponse(true);
            provider.setBaseUrl(this.configProvider.getApiEndpoint());

            var defaultHeaders: any = {};

            defaultHeaders['Authorization'] = 'Bearer ' + token;
            defaultHeaders['X-Long-Encoding'] = 'string';
            defaultHeaders['Content-type'] = ApiClient.CONTENT_TYPE_JSON;
            if(this.tipiSessionId) {
                defaultHeaders['Tipi-Session-Id'] = this.tipiSessionId
            }

            provider.setDefaultHeaders(defaultHeaders);
            provider.setResponseInterceptor(this.responseInterceptor.bind(this));
            provider.setErrorInterceptor(this.errorInterceptor.bind(this));

            // Move .options function to .restangularOptions in returned objects
            // @see https://github.com/mgonto/restangular/issues/266
            provider["restangularFields"].options = 'restangularOptions';
        }

        private responseInterceptor(data:IApiResponseData, operation:string, what:string, url:string, response:IResponse, deferred:ng.IDeferred<any>) {
            if (response.status != 200 && response.status != 204) {
                this.errorInterceptor(response, deferred, data);
            } else {
                if (data.status && data.status != 0) {
                    this.errorInterceptor(response, deferred, data);
                } else {
                    if(response && response.headers("Tipi-Session-Id")) {
                        this.tipiSessionId = response.headers("Tipi-Session-Id");
                    }
                    if(response && response.data && response.data.data) {
                        return response.data.data;
                    }
                    return response.data;
                }
            }
        }

        private errorInterceptor(response:IResponse, deferred:ng.IDeferred<any>, data?:IApiResponseData) {
            this.$log.error("Server error", response, data);

            var status:number = response.status;
            var msg:string = undefined;
            if (data) {
                if (data.status) {
                    status = data.status;
                }
                if (data.msg) {
                    msg = data.msg;
                }
            }

            var exception = new ApiException(status, msg);

            if (response.status == 401 || (data && data.status == 401)) {
                this.$log.warn("SERVER_UNAUTHORIZED response");
            }

            deferred.reject(exception);
        }
    }

    interface IApiResponseData {
        msg: string;
        status: number;
        data: any;
    }

}