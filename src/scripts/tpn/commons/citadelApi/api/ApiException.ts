"use strict";

module tpn.commons.citadelApi.api {

    import Exception = tpn.commons.shared.exception.Exception;

    export class ApiException extends Exception {

        apiMessage:any;

        constructor(status?:any, apiMessage?:string) {
            super(ApiResponses.getMessageFor(status) || apiMessage, status);
            this.name = "CitadelApiException";
            this.apiMessage = apiMessage;
        }
    }
}
