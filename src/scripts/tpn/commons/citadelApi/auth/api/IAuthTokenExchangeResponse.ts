"use strict";

module tpn.commons.citadelApi.auth.api {

    import IUser = tpn.commons.citadelApi.shared.model.IUser;

    export interface IAuthTokenExchangeResponse {
        user: IUser;
        token: string;
    }

}