"use strict";

module tpn.commons.citadelApi.auth.api {

    import IAuthTokenExchangeOptions    = tpn.commons.citadelApi.auth.api.IAuthTokenExchangeOptions;
    import IAuthTokenExchangeResponse   = tpn.commons.citadelApi.auth.api.IAuthTokenExchangeResponse;
    import ApiClient                    = tpn.commons.citadelApi.api.ApiClient;

    /**
     * API for /auth endpoint
     */

    export class AuthApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcCitadelApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Exchanges token from warden (i.e.) for a citadel token
         *
         * @param options Contains token and information about who generated the token
         * @returns {IPromise<IAuthTokenExchangeResponse>}
         */
        public exchangeToken(options:IAuthTokenExchangeOptions):ng.IPromise<IAuthTokenExchangeResponse> {

            return this.apiClient
                .get()
                .one("auth")
                .post("token",options)
                .then((data:any)=> {
                    if (data && data.data) return data.data.plain();
                })
        }
    }
}