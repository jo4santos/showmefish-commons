"use strict";

module tpn.commons.citadelApi.auth.api {

    import IUserDevice = tpn.commons.citadelApi.shared.model.IUserDevice;

    export interface IAuthTokenExchangeOptions {

        /**
         * Who generated the token for exchange ex: warden
         */
        provider: string;

        /**
         * Token for exchange
         */
        token: string

        /**
         * Parameters of the user's device
         */
        device: IUserDevice;

    }

}