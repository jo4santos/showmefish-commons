"use strict";

module tpn.commons.citadelApi.test.api {

    import ApiClient = tpn.commons.citadelApi.api.ApiClient;
    import IPromise = ng.IPromise;

    /**
     * API for /test endpoint
     */

    export class TestApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcCitadelApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Tests connection to server
         *
         * @params options
         * @returns {IPromise<any>}
         */
        public test():IPromise<any> {

            return this.apiClient
                .get()
                .one("test")
                .get();
        }
    }
}