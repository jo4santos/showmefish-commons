"use strict";

module tpn.commons {

    /**
     * Register tcWebCommons module
     */
    function registerAngularModule() {

        try {
            angular.module('tcPartials');
        } catch (e) {
            angular.module('tcPartials', []);
        }

        // Need to be defined by client app:
        //.constant("tcUserSession", new model.auth.UserSession()) // Should be created a session object with model.auth.IUserSession structure

        // Should exist a config.json file with a appConfig property

        angular.module("tcWebCommons", ["tcPartials", "tcDirectives", "tcFilters", "tcXmppApi", "tcMessaging", "tcContactiveApi", "tcWardenApi", "tcCitadelApi", "tcUnifiedContact", "tcUcApi", "tcFuzeApi"]);
    }

    angular.element(document).ready(registerAngularModule);
}

