module tpn.commons.shared.util {

    export class AngularUtils {

        /**
         * Safely apply scope
         */
        static safeApply($scope: ng.IScope) {
            setTimeout(() => {
                $scope.$apply()
            });
        }

    }

}
