"use strict";

module tpn.commons.shared.util {

    export class StringUtils {

        /**
         * String.format('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
         *
         * @param format
         * @param values
         * @returns {string}
         */
        static format(format:string, ...values:string[]):string {
            return format.replace(/{(\d+)}/g, function (match, index) {
                return typeof values[index] != 'undefined' ? values[index] : match;
            });
        }

        /**
         * Returns string without special chars and lower case, ideal for searching
         *
         * @param source
         * @returns {string}
         */
        static normalize(source:string):string {
            return latinize(source).toLowerCase().trim();
        }

        /**
         * Returns string without spaces
         *
         * @param source
         * @returns {string}
         */
        static stripSpaces(source:string):string {
            return source.replace(/\s/g, '');
        }

        /**
         * Escapes a string to use in a regular expression. Example: replaces + for \+
         *
         * @param source
         * @returns {string}
         */
        static escapeRegExp(source):string {
            return source.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        }

        /**
         * Returns true if string is null of empty
         *
         * @param source
         * @returns {boolean}
         */
        static isEmpty(source:string):boolean {
            return source == null || source == "";
        }

        /**
         * Returns true if all strings are null of empty
         *
         * @param sources
         * @returns {boolean}
         */
        static areEmpty(...sources:string[]):boolean {
            for (var i = 0; i < sources.length; i++) {
                if (!StringUtils.isEmpty(sources[i])) {
                    return false;
                }
            }

            return true;
        }

        /**
         * Returns true if the strings are equal
         *
         * @param source1
         * @param source2
         * @returns {boolean}
         */
        static equals(source1:string, source2:string):boolean {
            if (source1 == source2) {
                return true;
            }

            if (source1 == null || source2 == null) {
                return false;
            }

            return source1.localeCompare(source2) == 0;
        }

        /**
         * Returns true if the strings are equal, ignoring the string case
         *
         * @param source1
         * @param source2
         * @returns {boolean}
         */
        static equalsIgnoreCase(source1:string, source2:string):boolean {
            if (source1 == source2) {
                return true;
            }

            if (source1 == null || source2 == null) {
                return false;
            }

            return source1.toUpperCase().localeCompare(source2.toUpperCase()) == 0;
        }

        /**
         * Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
         *
         * @param source1
         * @param source2
         * @returns {number}
         */
        static compare(source1:string, source2:string):number {
            if (source1 == source2) {
                return 0;
            }

            if (source1 == null) {
                return -1;
            }

            if (source2 == null) {
                return 1;
            }

            return source1.localeCompare(source2);
        }

        /**
         * Returns a negative integer, zero, or a positive integer as the first argument is shorter than, equal to, or longer than the second.
         *
         * @param source1
         * @param source2
         * @returns {number}
         */
        static compareLength(source1:string, source2:string):number {
            if (source1 == source2) {
                return 0;
            }

            if (source1 == null) {
                return -1;
            }

            if (source2 == null) {
                return 1;
            }

            var source1Length = source1.length;
            var source2Length = source2.length;

            if (source1Length < source2Length) {
                return -1;
            } else if (source1Length > source2Length) {
                return 1;
            } else {
                return 0;
            }
        }

        /**
         * Joins multiple strings adding a separator between them.
         *
         * @param separator
         * @param values
         * @returns {string}
         */
        static join(separator:string, values:string[]):string {
            var sep:string = "";
            var result:string = "";

            values.forEach((value:string)=> {
                result += sep + value;
                sep = separator;
            });

            return result;
        }

        /**
         * Escapes HTML tags in string
         *
         * @param text
         * @returns {any}
         */
        static escapeHtml(text):string {
            var map = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#039;'
            };

            return text.replace(/[&<>"']/g, function (m) {
                return map[m];
            });
        }

    }

}
