module tpn.commons.shared.util {

    /**
     * Class responsible to encode or decode objects to query params on the backend format
     */
    export class UrlEncodedUtils{

        public static toQuery(obj: any, createEmptyProperties: boolean = false, prefix?: string): string{
            var str = [];
            for(var p in obj){
                if (obj.hasOwnProperty(p)) {
                    var v = obj[p];
                    var k = p;

                    if(prefix) {
                        k = prefix + "[" + p + "]";
                    }

                    if (this.isObject(v)){
                        str.push(this.toQuery(v, false , k));
                    }
                    // Array? Send multiple parameters or an empty parameter
                    else if (this.isArray(v)){
                        if (v.length) {
                            for(var i = 0;i< v.length;i++){
                                str.push(encodeURIComponent(k) + "=" + encodeURIComponent(v[i]));
                            }
                        } else {
                            // Send an empty value if array is empty
                            str.push(encodeURIComponent(k) + "=");
                        }
                    }

                    // Value is null or empty string?
                    else if (v == null || v == "") {
                        // Send an empty value if array is empty
                        if (createEmptyProperties)
                            str.push(encodeURIComponent(k) + "=");
                    }

                    // Is not a function? Send it
                    else if (!this.isFunction(v)) {
                        str.push(encodeURIComponent(k) + "=" + encodeURIComponent(v));
                    }
                }
            }
            return str.join("&");
        }

        //From: https://gist.github.com/kares/956897
        //TODO: change this to support backend query arrays
        public static fromQuery(query: string, tryReplaceBooleans?: boolean):any {
            var obj = {}
            var e;
            var re = /([^&=]+)=?([^&]*)/g;
            var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
            var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
            if (query) {
                if (query.substr(0, 1) == '?') {
                    query = query.substr(1);
                }

                while (e = re.exec(query)) {
                    var k = decode(e[1]);
                    var v:any = decode(e[2]);
                    if (tryReplaceBooleans) {
                        if (v === 'true') {
                            v = true;
                        } else if (v === 'false') {
                            v = false;
                        }
                    }
                    if (obj[k] !== undefined) {
                        if (!UrlEncodedUtils.isArray(obj[k])) {
                            obj[k] = [obj[k]];
                        }
                        obj[k].push(v);
                    } else {
                        obj[k] = v;
                    }
                }
            }
            return obj;
        }

        private static isArray(obj): boolean {
            return obj != null && obj.constructor == Array;
        }

        private static isFunction(obj): boolean {
            return typeof obj === "function";
        }

        private static isObject(obj): boolean {
            return typeof obj == "object";
        }

    }

}