
module tpn.commons.shared.util {

    export class Cloner {

        static clone(object, deep = false)
        {
            if (object instanceof Array) {
                var copy = [];
                for (var i=0; i<object.length; i++) {
                    copy[i] = Cloner.clone(object[i]);
                }
                return copy;
            }

            else if (!(object instanceof Object)) return object;

            return $.extend(deep, {}, object);
        }
    }

}
