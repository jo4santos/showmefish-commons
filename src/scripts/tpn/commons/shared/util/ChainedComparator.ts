
module tpn.commons.shared.util {

    export class ChainedComparator<T> implements IComparator<T> {

        private comparators: IComparator<T>[];

        constructor(...comparators: IComparator<T>[]) {
            this.comparators = comparators;
        }

        public compare(o1: T, o2: T): number {
            for (var i = 0; i < this.comparators.length; i++) {
                var currentComparator: IComparator<T> = this.comparators[i];

                var result: number = currentComparator.compare(o1, o2);
                if (result != 0) {
                    return result;
                }
            }

            return 0;
        }

    }

}
