"use strict";

module tpn.commons.shared.util {

    export class DateUtils {

        /**
         *
         * @param millis
         * @returns {number}
         */
        static getMonthsFromMillis(millis:number):number {
            var d = new Date(millis);
            return 12 * d.getFullYear() + (d.getMonth() + 1);
        }

        /**
         * Convert date object to ISO string
         *
         * @param date
         * @returns {string}
         */
        static toISODateString(date: Date): string {

            return date.getUTCFullYear() + '-'
                + this.zeroPad(date.getUTCMonth() + 1) + '-'
                + this.zeroPad(date.getUTCDate()) + 'T'
                + this.zeroPad(date.getUTCHours()) + ':'
                + this.zeroPad(date.getUTCMinutes()) + ':'
                + this.zeroPad(date.getUTCSeconds()) + 'Z'
        }

        /**
         * Pad numbers with 0
         * @param n
         * @returns {string}
         */
        static zeroPad(n: number) {
            return n < 10 ? '0' + n : n.toString()
        }

    }

}
