
module tpn.commons.shared.util {

    export interface IComparator<T> {

        /**
         * @param o1 the first object to be compared.
         * @param o2 the second object to be compared.
         * Returns: a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
         */
        compare(o1: T, o2: T): number;

    }

}
