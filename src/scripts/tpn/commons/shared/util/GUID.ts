module tpn.commons.shared.util {

    export class GUID {

        public static generate(): string {
            function g() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return g() + g() + '-' + g() + '-' + g() + '-' +
                g() + '-' + g() + g() + g();
        }

    }

}