"use strict";

module tpn.commons.shared.listener {

    /**
     * Manages listeners and applies callbacks to them (indexed by channel)
     */
    export class IndexedListenerManager<T> {

        protected $log: ng.ILogService;
        protected callbacks: IIndexedListenerManagerOptions<T>;

        protected indexed: {[channel:string]: ListenerManager<T>} = {};
        protected list: T[] = [];

        /**
         * @param $log
         * @param callbacks
         * @param callbacks.onAddedListener Called after one listener was added
         * @param callbacks.onFirstListener Called before first listener is added (when there is only one listener)
         * @param callbacks.onClear Called after last listener is removed (when there are no more listeners)
         * @param callbacks.onFirstIndexListener Called before first listener is added to an index
         * @param callbacks.onClearIndex Called after last listener is removed from an index
         */
        constructor(
            $log: ng.ILogService,
            callbacks: IIndexedListenerManagerOptions<T> = {}
        ) {
            this.$log = $log;
            this.callbacks = callbacks;
        }

        /**
         * Applies callbackFn to all registered listeners on specified channel
         * @param channelKey
         * @param callbackFn
         * @param thisArg
         */
        forEach(channelKey: string, callbackFn: IListenerCallback<T>, thisArg?: any) {
            var manager = this.getChannelManager(channelKey, false);
            if (manager) {
                manager.forEach(callbackFn, thisArg);
            }
        }

        /**
         * After called, listener will start receiving updates from specified channel
         * When applyLastCallbackOnNewListener is true, listener will immediatlly be called with last callback (if any)
         *
         * @param channelKey
         * @param listener
         */
        addListener(channelKey: string, listener: T) {
            var manager = this.getChannelManager(channelKey, true);
            if (manager) {

                // Add to ListenerManager
                manager.addListener(listener);

                // Add to list
                this.list.push(listener);

                // Call onFirstListener if it's the first
                if (this.list.length == 1) {
                    this.onFirstListener(channelKey, listener);
                }

                // On added listener
                this.onAddedListener(channelKey, listener);
            }

        }

        /**
         * Removes listener
         * After called, listener will no more receive updates on specified channel
         *
         * @param channelKey
         * @param listener
         * @returns {boolean}
         */
        removeListener(channelKey: string, listener: T): boolean {
            var channel = this.getChannelManager(channelKey, false);
            if (channel) {

                // Remove from indexed
                channel.removeListener(listener);

                // Removed from list
                var idx = this.list.indexOf(listener);
                if (idx >= 0) {
                    this.list.splice(idx, 1);
                }

                // Call onClear if it's the last
                if (this.list.length == 0) {
                    this.onClear(channelKey);
                }
            }
            return false;
        }

        /**
         * Returns channel ListenerManager
         *
         * @param channelKey
         * @param createIfNotExists
         * @returns {ListenerManager<T>}
         */
        protected getChannelManager(channelKey: string, createIfNotExists: boolean = false): ListenerManager<T> {
            var channel = this.indexed[channelKey];

            if (!channel) {
                channel = this.createChannelListenerManager(channelKey);
            }

            return channel;
        }

        /**
         * Create channel
         * @param channelKey
         */
        protected createChannelListenerManager(channelKey: string): ListenerManager<T> {

            // Add to indexed
            this.indexed[channelKey] = new ListenerManager<T>(
                this.$log,
                {
                    onFirstListener: this.onFirstIndexListener.bind(this, channelKey),
                    onClear: this.onClearIndex.bind(this, channelKey)
                }
            );

            return this.indexed[channelKey];
        }

        //
        // Callbacks
        //

        protected onAddedListener(channelKey: string, listener: T) {
            if (this.callbacks.onAddedListener) {
                this.callbacks.onAddedListener(channelKey, listener);
            }
        }

        protected onFirstListener(channelKey: string, listener: T) {
            if (this.callbacks.onFirstListener) {
                this.callbacks.onFirstListener(channelKey, listener);
            }
        }

        protected onClear(channelKey: string) {
            if (this.callbacks.onClear) {
                this.callbacks.onClear(channelKey);
            }
        }

        protected onFirstIndexListener(channelKey: string, listener: T) {
            if (this.callbacks.onFirstIndexListener) {
                this.callbacks.onFirstIndexListener(channelKey, listener);
            }
        }

        protected onClearIndex(channelKey: string, listener: T) {
            if (this.callbacks.onClearIndex) {
                this.callbacks.onClearIndex(channelKey, listener);
            }
        }

    }

    export interface IIndexedListenerManagerOptions<T> {
        onAddedListener?: IIndexedCallback<T>,
        onFirstListener?: IIndexedCallback<T>,
        onClear?: IIndexedCallback<T>,
        onAddedIndexListener?: IIndexedCallback<T>,
        onFirstIndexListener?: IIndexedCallback<T>,
        onClearIndex?: IIndexedCallback<T>
    }

    export interface IIndexedCallback<T> {
        (key: string, listener?: T): void;
    }


}