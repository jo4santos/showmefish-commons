"use strict";

module tpn.commons.shared.listener {

    /**
     * Manages listeners and applies callbacks to them
     */
    export class ListenerManager<T> {

        protected $log: ng.ILogService;
        protected callbacks: IListenerManagerOptions<T>;

        protected listeners: T[] = [];

        /**
         * @param $log
         * @param callbacks
         * @param callbacks.onFirstListener Called before first listener is added (when there is only one listener)
         * @param callbacks.onClear Called after last listener is removed (when there are no more listeners)
         */
        constructor(
            $log: ng.ILogService,
            callbacks: IListenerManagerOptions<T> = {}
        ) {
            this.$log = $log;
            this.callbacks = callbacks;
        }

        /**
         * Applies callbackFn to all registed listeners
         * @param callbackFn
         * @param thisArg
         */
        forEach(callbackFn: IListenerCallback<T>, thisArg?: any) {
            this.listeners.forEach((listener) => {
                this.applyCallbackFunction(callbackFn, listener, thisArg);
            });
        }

        /**
         * After called, listener will start receiving updates from ListenerManager
         * When applyLastCallbackOnNewListener is true, listener will immediatlly be called with last callback (if any)
         *
         * @param listener
         */
        addListener(listener: T) {
            this.listeners.push(listener);
            this.onFirstListener(listener);
            this.onAddedListener(listener);
            return this;
        }

        /**
         * Removes listener
         * After called, listener will no more receive updates from ListenerManager
         *
         * @param listener
         * @returns {boolean}
         */
        removeListener(listener: T) {
            if (listener) {
                var index = this.listeners.indexOf(listener);
                if (index > -1) {
                    this.listeners.splice(index, 1);
                    this.onClear();
                    return true;
                }
            }
            return false;
        }

        /**
         * Calls callback function for identified listener
         * @param callbackFn
         * @param listener
         * @param thisArg
         */
        protected applyCallbackFunction(callbackFn: IListenerCallback<T>, listener: T, thisArg?: any) {
            try {
                callbackFn.call(thisArg, listener);
            } catch (e) {
                this.$log.error("Ignoring unexpected error calling listener", e);
            }
        }

        //
        // Callbacks
        //

        protected onAddedListener(listener: T) {
            if (this.callbacks.onAddedListener) {
                this.callbacks.onAddedListener(listener);
            }
        }

        protected onFirstListener(listener: T) {
            if (this.listeners.length == 1 && this.callbacks.onFirstListener) {
                this.callbacks.onFirstListener(listener);
            }
        }

        protected onClear() {
            if (!this.listeners.length && this.callbacks.onClear) {
                this.callbacks.onClear();
            }
        }

    }

    export interface IListenerManagerOptions<T> {
        onAddedListener?: IListenerCallback<T>,
        onFirstListener?: IListenerCallback<T>,
        onClear?: IListenerCallback<T>
    }

    export interface IListenerCallback<T> {
        (listener?: T): void;
    }

}