"use strict";

module tpn.commons.shared.messaging.listener {

    import IConversation = tpn.commons.shared.messaging.model.IConversation;

    export interface IDatasourceConversationListener {
        onNewConversation?(conversation: IConversation): void;
        onUpdatedConversation?(conversation: IConversation): void;
        onRemovedConversation?(conversation: IConversation): void;
    }

}