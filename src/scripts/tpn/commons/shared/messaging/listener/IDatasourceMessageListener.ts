"use strict";

module tpn.commons.shared.messaging.listener {

    // tpn.commons.shared.messaging
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import IMessageQueryOptions = tpn.commons.shared.messaging.model.IMessageQueryOptions;

    export interface IDatasourceMessageListener {
        onChangedMessages?(result: IMessagesChanged): void;
        onMarkAsRead?(conversationId: string, toInc: Date, numMarked: number): void;
        onRemovedMessages?(query: IMessageQueryOptions, numDeleted: number): void;
    }

    export interface IMessagesChanged {
        inserted?: IMessage[];
        updated?: IMessage[];
    }

}
