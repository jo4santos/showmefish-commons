"use strict";

module tpn.commons.shared.messaging.model {

    /**
     * Map for sorting fields
     *
     * Number should be:
     * 1 for ascending, -1 for descending
     */
    export interface ISortMap {
        [key: string]: SortOrder;
    }

}