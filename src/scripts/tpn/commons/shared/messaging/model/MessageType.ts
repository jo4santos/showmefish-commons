"use strict";

module tpn.commons.shared.messaging.model {

    export class MessageType {
        static CHAT = "chat";
        static ERROR = "error";
        static GROUP_CHAT = "groupchat";
        static HEAD_LINE = "headline";
        static NORMAL = "normal";

        // Non standard
        static IS_WRITING = "is_writing";
    }

}