"use strict";

module tpn.commons.shared.messaging.model {

    export interface IConversationQueryOptions {

        id?: string|string[];
        participants?: string[];

        limit?: number;
        offset?: number;
    }

}