"use strict";

module tpn.commons.shared.messaging.model {

    /**
     * @see http://xmpp.org/rfcs/rfc3921.html#messaging
     */
    export interface IMessage {

        /**
         * XMPP Message identifier
         * XmppApi should generate an UUID if there's no defined ID
         *
         * @see XmppApi.fillMessageWithDefaults
         */
        id?: string;

        /**
         * UC API message id
         */
        ucId?: string;

        /**
         * Message was read?
         */
        isRead?: boolean;

        /**
         * Message was delivered to the message sync server ?
         */
        isDelivered?: boolean;

        /**
         * Utility attribute to check if message was received
         */
        received?: boolean;

        /**
         * Complete jid from contact where was sent
         * Example: "fsantos@im.thinkingphones.com/tpn-comm"
         * When sending, XmppApi should set default to the current user
         *
         * @see XmppApi.fillMessageWithDefaults
         */
        from?: string;

        /**
         * Send message to this jid
         * Jid could be a groupchat identifier (<room@service>)
         * @see http://xmpp.org/extensions/xep-0045.html#message
         */
        to: string;

        /**
         * JID for conversation (to index messages by conversation)
         */
        conversationId: string;

        /**
         * UC API conversation id
         */
        conversationUcId?: string;

        /**
         * XMPP thread identifier
         * Example: "MQwHx6"
         */
        xmppThreadId?: string;

        /**
         * Message subject should change conversation subject
         * @see http://xmpp.org/rfcs/rfc3921.html#rfc.section.2.1.2.1
         */
        subject?: string

        /**
         * Message type
         * XmppApi should set default to MessageTypes.NORMAL
         *
         * @see MessageType
         * @see XmppApi.fillMessageWithDefaults
         */
        type?: string;

        /**
         * Message timestamp
         * XmppApi should set default to current timestamp
         *
         * @see XmppApi.fillMessageWithDefaults
         */
        timeSent?: Date;

        /**
         * Chat state
         * XmppApi should set default to ChatStates.ACTIVE
         *
         * @see ChatState
         * @see XmppApi.fillMessageWithDefaults
         */
        chatState?: string;

        /**
         * Message content / text
         */
        body?: any;

        /**
         * TODO: Sets the message language
         * Defaults to browser language
         *
         * @see XmppApi.fillMessageWithDefaults
         */
        lang?: string;
    }
}