"use strict";

module tpn.commons.shared.messaging.model {

    export class SortOrder {
        static ASC = 1;
        static DESC = -1;
    }

}