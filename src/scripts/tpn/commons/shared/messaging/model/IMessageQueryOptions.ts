"use strict";

module tpn.commons.shared.messaging.model {

    export interface IMessageQueryOptions
    extends IMessageAttributesQueryOptions, IPeriodQueryOptions, IPaginationQueryOptions, ISortQueryOptions {
        // text?: string;
    }

    export interface IMessageAttributesQueryOptions {
        id?: string|string[];
        conversationId?: string;
        isRead?: boolean;
        fromJids?: string[];
    }

    export interface IPeriodQueryOptions {
        from?: Date;    // FIXME: We should change this to startDate. It conflicts with the key "from" of the messages.
        to?: Date;
    }

    export interface IPaginationQueryOptions {
        limit?: number;
        offset?: number;
    }

    export interface ISortQueryOptions {
        sort?: ISortMap;
    }

}