"use strict";

module tpn.commons.shared.messaging.model {

    /**
     * XMPP conversation types
     */
    export class ConversationType {

        static CHAT = "chat";
        static GROUP_CHAT = "groupchat";
        
    }

}