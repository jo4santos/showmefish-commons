"use strict";

module tpn.commons.shared.messaging.model {

    export interface IConversationUpdate {
        conversationId: string;
        conversationUcId: string;
        unreadCount: number;
        deleted: boolean;
        lastMessage: IMessage;
    }
}
