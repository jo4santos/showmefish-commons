"use strict";

module tpn.commons.shared.messaging.model {


    // tpn.commons.contactiveApi
    import IContactItem = tpn.commons.contactiveApi.contact.model.IContactItem;

    // tpn.commons.unifiedContact
    import IUnifiedContact = tpn.commons.unifiedContact.model.IUnifiedContact;

    /**
     * XMPP conversation
     * @see http://xmpp.org/rfcs/rfc3921.html#rfc.section.2.1.2.3
     */
    export interface IConversation extends IConversationIds {

        /**
         * Matches with contact jid (1:1 chat) or chat room jid (chat room)
         * - User jid (<user@service>)
         * - Chat group identification (jid) (<room@service>)
         * @see http://xmpp.org/extensions/xep-0045.html#message
         */
        id: string;

        /**
         * Message sync conversation id
         */
        ucId?: string

        /**
         * Threads that this conversation matches
         */
        threads?: string[];

        /**
         * XMPP message types
         * @see ConversationType
         */
        type?: string;

        /**
         * XMPP conversation subject
         * @see http://xmpp.org/rfcs/rfc3921.html#rfc.section.2.1.2.1
         */
        subject?: string;

        /**
         * Conversation participants in a chat or group chat (jids)
         * Do not include the logged user
         */
        participantsJids: string[];

        /**
         * Contacts references
         * @transient
         */
        participants?: IUnifiedContact[];

        //
        // Messages
        //

        /**
         * Session messages
         * Should be always sorted in ascending order [older, newer]
         * @transient
         */
        messages?: IMessage[];

        /**
         * Unread messages
         */
        unreadCount?: number;

        /**
         * Last read message timeSent
         */
        lastRead?: Date;

        //
        // Misc
        //

        /**
         * This conversation is active?
         * @transient
         */
        active?: boolean;

        /**
         * Input text that was not still sent
         * @transient
         */
        inputText?: string;
    }

    export interface IConversationIds {

    }

}
