"use strict";

module tpn.commons.shared.messaging.datasource {

    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IConversationQueryOptions = tpn.commons.shared.messaging.model.IConversationQueryOptions;

    import IDatasourceConversationListener = tpn.commons.shared.messaging.listener.IDatasourceConversationListener;

    /**
     * Saves messages in NeDB storage, indexed by jids and thread identification
     * Should give the unread messages count
     * Should support message sync
     */
    export interface IConversationDatasource /* extends IDatasource<IDatasourceConversationListener> */ {

        /**
         * Returns empty array if there's no matched conversations
         * @param query
         */
        getConversations(query?: IConversationQueryOptions): ng.IPromise<IConversation[]>;

        /**
         * Returns null if conversation was not found
         * @param conversationId
         */
        getConversation(conversationId: string): ng.IPromise<IConversation>;
        insertConversation(conversation: IConversation): ng.IPromise<void>;
        updateConversation(conversation: IConversation, upsert?: boolean): ng.IPromise<void>;
        removeConversation(conversation: IConversation): ng.IPromise<void>;

        registerListener(callback:IDatasourceConversationListener);
        deregisterListener(callback:IDatasourceConversationListener);
    }
}
