"use strict";

module tpn.commons.shared.messaging.datasource {

    // tpn.commons.shared.messaging
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import IMessageAttributesQueryOptions = tpn.commons.shared.messaging.model.IMessageAttributesQueryOptions;
    import IDatasourceMessageListener = tpn.commons.shared.messaging.listener.IDatasourceMessageListener;
    import IMessageQueryOptions = tpn.commons.shared.messaging.model.IMessageQueryOptions;

    /**
     * Saves messages in NeDB storage, indexed by jids and thread identification
     * Should give the unread messages count
     * Should support message sync
     */
    export interface IMessageDatasource /* extends IDatasource<IDatasourceMessageListener> */ {

        getMessage(query: IMessageQueryOptions): ng.IPromise<IMessage>;
        getMessages(query: IMessageQueryOptions): ng.IPromise<IMessage[]>;
        getMessagesCount(query: IMessageAttributesQueryOptions): ng.IPromise<number>;
        insertMessages(messages: IMessage[]): ng.IPromise<void>;
        updateMessages(messages: IMessage[], upsert?: boolean): ng.IPromise<void>;
        markMessagesAdRead(conversationId: string, toInc: Date):ng.IPromise<void>;

        removeMessages(query: IMessageQueryOptions): ng.IPromise<void>;

        registerListener(callback:IDatasourceMessageListener);
        deregisterListener(callback:IDatasourceMessageListener);
    }
}
