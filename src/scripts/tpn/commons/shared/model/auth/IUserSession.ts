"use strict";

module tpn.commons.shared.model.auth {

    export interface IUserSession {
        isUserLogged(): boolean;
        hasUserRole(role: string): boolean;
    }
}