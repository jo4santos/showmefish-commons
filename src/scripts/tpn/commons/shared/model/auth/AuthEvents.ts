"use strict";

module tpn.commons.shared.model.auth {

    export class AuthEvents {
        static LOGGED_IN = "AUTH_LOGGED_IN";
        static LOGGED_OUT = "AUTH_LOGGED_OUT";
    }
}
