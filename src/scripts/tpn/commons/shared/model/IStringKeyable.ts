"use strict";

module tpn.commons.shared.model {

    export interface IStringKeyable {
        value?: string;
        isKey?: boolean;
    }

}

