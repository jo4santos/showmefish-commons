"use strict";

module tpn.commons.shared.model.app {

    /**
     * Application device configuration
     */
    export interface IAppConfig {

        //
        // Application / Device settings
        //

        /**
         * Application base path
         */
        basePath?: string;

        //
        // Systems connection
        //

    }

}
