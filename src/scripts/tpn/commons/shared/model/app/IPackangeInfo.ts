"use strict";

module tpn.commons.shared.model.app {

    /**
     * Application device configuration
     */
    export interface IPackageInfo {

        /**
         * Project version
         */
        projectVersion: string;

        /**
         * Project GIT revision
         */
        gitRevision: string;
    }

}
