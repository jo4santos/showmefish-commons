// Type definitions for libphonenumber 0.9
// Project: https://github.com/nathanhammond/libphonenumber
// Definitions by: Filipe Costa <https://github.com/nevcos/>
// Definitions: https://github.com/contactive/klink-admin

declare module libphonenumber {
    interface PhoneUtils
    {
        isPossibleNumber(phoneNumber:string, regionCode:string): boolean;
        isPossibleNumberWithReason(phoneNumber:string, regionCode:string): boolean;
        isValidNumber(phoneNumber:string, regionCode:string): boolean;
        isValidNumberForRegion(phoneNumber:string, regionCode:string): boolean;
        getregionCodeForNumber(phoneNumber:string, regionCode:string): string;
        getNumberType(phoneNumber:string, regionCode:string): string;
        formatE164(phoneNumber:string, regionCode:string): string;
        formatNational(phoneNumber:string, regionCode:string): string;
        formatInternational(phoneNumber:string, regionCode:string): string;
        formatInOriginalFormat(phoneNumber:string, regionCode:string): string;
        formatOutOfCountryCallingNumber(phoneNumber:string, regionCode:string, target): string;
    }
}

declare var phoneUtils:libphonenumber.PhoneUtils;
