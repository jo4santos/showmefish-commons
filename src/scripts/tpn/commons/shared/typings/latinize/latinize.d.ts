// Type definitions for latinize 0.2
// Project: https://github.com/dundalek/latinize
// Definitions by: José Santos <https://github.com/jo4santos/>

declare interface ILatinize {
    (string): string;
}

declare var latinize: ILatinize;
