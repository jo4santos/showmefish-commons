// Type definitions for ua-parser-js 0.7
// Project: https://github.com/faisalman/ua-parser-js
// Definitions by: Filipe Costa <https://github.com/nevcos/>
// Definitions: https://github.com/contactive/klink-admin

declare module uaParserJs {
    interface UAParser
    {
        new(): UAParser;
        setUA(ua:string): void;
        getResult(): UAInfo;
    }

    interface UAInfo {
        ua: string;
        browser: BrowserUAInfo;
        cpu: CPUUAInfo;
        device: DeviceUAInfo;
        engine: EngineUAInfo;
        os: OSUAInfo;
    }

    interface BrowserUAInfo {
        major: string;
        name: string;
        version: string;
    }

    interface CPUUAInfo {
        architecture: string;
    }

    interface DeviceUAInfo {
        model: string;
        type: string;
        vendor: string;
    }

    interface EngineUAInfo {
        name: string;
        version: string;
    }

    interface OSUAInfo {
        name: string;
        version: string;
    }
}

declare var UAParser:uaParserJs.UAParser;
