"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class UnauthorizedException extends Exception {

        constructor(message:string = "Unauthorized", code?:any, cause?:any) {
            super(message, code, cause);
            this.name = "UnauthorizedException";
        }
    }

}