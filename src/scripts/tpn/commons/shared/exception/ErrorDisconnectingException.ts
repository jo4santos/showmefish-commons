"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class ErrorDisconnectingException extends Exception {

        constructor(message:string = "Can't disconnect from the server", cause?:any) {
            super(message, cause);
            this.name = "ErrorConnectingException";
        }
    }

}