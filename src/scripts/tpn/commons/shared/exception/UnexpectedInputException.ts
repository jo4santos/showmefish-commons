"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class UnexpectedInputException extends Exception {

        constructor(message:string = "Unexpected input", cause?: any) {
            super(message, cause);
            this.name = "UnexpectedInputException";
        }
    }

}