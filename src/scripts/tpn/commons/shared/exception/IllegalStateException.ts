module tpn.commons.shared.exception {

    "use strict";

    export class IllegalStateException extends Exception {

        constructor(message?:string, code?:any, cause?:any) {
            super(message, code, cause);
            this.name = "IllegalStateException";
        }
    }


}
