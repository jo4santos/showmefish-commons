"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class NotConnectedException extends Exception {

        constructor(message:string = "Not connected to server") {
            super(message);
            this.name = "NotConnectedException";
        }
    }

}