"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class ErrorConnectingException extends Exception {

        constructor(message:string = "Can't connect to server", code?: number, cause?: any) {
            super(message, code, cause);
            this.name = "ErrorConnectingException";
        }
    }

}