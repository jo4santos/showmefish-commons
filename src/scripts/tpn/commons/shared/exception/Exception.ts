module tpn.commons.shared.exception {

    "use strict";

    /**
     * TODO: In a future version of TypeScript (after Jun 17, 2015), replace implements by extends by extends
     * For now we just need to have the (new Error()).stack hack, that give us some more lines of stack for the
     * Exception creation
     *
     * @see http://stackoverflow.com/a/31038470/198787
     */
    export class Exception implements Error {

        name: string;
        message: string;
        stack: string;
        code: any;
        cause: any;

        constructor(message?:string, code?:any, cause?:any) {
            this.name = "Exception";
            this.message = message;
            this.stack = (<any>new Error()).stack;
            this.code = code;
            this.cause = cause;

            this.appendCauseStack();
        }

        protected appendCauseStack() {
            if (this.cause) {
                this.stack += "\nCaused by: ";

                if (this.cause.stack) {
                    this.stack += this.cause.stack;
                } else if (this.cause.message) {
                    this.stack += this.cause.message;
                } else {
                    this.stack += this.cause;
                }
            }
        }
    }


}
