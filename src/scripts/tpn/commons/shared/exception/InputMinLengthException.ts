"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class InputMinLengthException extends Exception {

        constructor(message:string = "Insert at least 3 characters to search") {
            super(message);
            this.name = "InputMinLengthException";
        }
    }

}