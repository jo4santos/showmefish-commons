"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class AlreadyConnectedException extends Exception {

        constructor(message:string = "Already connected to server") {
            super(message);
            this.name = "AlreadyConnectedException";
        }
    }

}