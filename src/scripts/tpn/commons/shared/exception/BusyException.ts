"use strict";

module tpn.commons.shared.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class BusyException extends Exception {

        constructor(message:string = "Not ready for received call") {
            super(message);
            this.name = "BusyException";
        }
    }

}