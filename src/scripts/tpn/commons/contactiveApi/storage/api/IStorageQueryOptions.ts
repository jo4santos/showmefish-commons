"use strict";

module tpn.commons.contactiveApi.storage.api {

    export interface IStorageQueryOptions {
        minRevision?: number;
    }

}