"use strict";

module tpn.commons.contactiveApi.storage.api {

    import ApiClient = tpn.commons.contactiveApi.api.ApiClient;
    import IPromise = ng.IPromise;
    import IStorageQueryOptions = tpn.commons.contactiveApi.storage.api.IStorageQueryOptions;
    import IStorageQueryResponse = tpn.commons.contactiveApi.storage.model.IStorage;

    /**
     * API for /users/me/storage endpoint
     */

    export class StorageApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcContactiveApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Gets storage for current user
         *
         * @params options
         * @returns {IPromise<IStorageQueryResponse>}
         */
        public get(options?:IStorageQueryOptions):IPromise<IStorageQueryResponse> {

            if (options) {
                options.minRevision = options.minRevision || 0;
            }

            return this.apiClient
                .get()
                .all("users/me")
                .one("storage")
                .get(options)
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }
    }
}