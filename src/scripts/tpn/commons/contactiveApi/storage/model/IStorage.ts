"use strict";

module tpn.commons.contactiveApi.storage.model {

    export interface IStorage {
        created: number;
        headRevision: number;
        lastTimeNotified: number;
        modified: number;
        totalItems: number;
        usedSpace: number;
        userId: string;
    }

}