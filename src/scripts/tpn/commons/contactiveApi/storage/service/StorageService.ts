"use strict";

module tpn.commons.contactiveApi.storage.service {

    import IPromise = ng.IPromise;
    import IStorage = tpn.commons.contactiveApi.storage.model.IStorage;
    import IStorageQueryOptions = tpn.commons.contactiveApi.storage.api.IStorageQueryOptions;
    import StorageApi = tpn.commons.contactiveApi.storage.api.StorageApi;

    export class StorageService {

        protected $q:ng.IQService;
        protected storageApi:StorageApi;

        public static $inject = ["$q", "tcContactiveApiStorageApi"];

        public constructor($q:ng.IQService,
                           StorageApi:StorageApi) {
            this.$q = $q;
            this.storageApi = StorageApi;
        }

        /**
         * Get storage information from user private storage using usersApi
         *
         * @param options
         *
         * @returns {IPromise<IStorageQueryResponse>}
         */
        public get(options?:IStorageQueryOptions):ng.IPromise<IStorage> {
            return this.storageApi.get(options).then((response:IStorage) => {
                return response;
            });
        }
    }
}