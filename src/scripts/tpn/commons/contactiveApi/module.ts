module tpn.commons.contactiveApi {

    "use strict";

    /**
     * Register ContactiveApi module
     */
    function registerAngularModule() {
        angular
            .module("tcContactiveApi", ["restangular"])

            .service("tcContactiveApiClient", api.ApiClient)

            .service("tcContactiveApiItemApi", item.api.ItemApi)
            .service("tcContactiveApiItemService", item.service.ItemService)

            .service("tcContactiveApiContactItemApi", contact.api.ContactItemApi)
            .service("tcContactiveApiContactItemService", contact.service.ContactItemService)

            .service("tcContactiveApiServicesAuthApi", services.api.AuthApi)
            .service("tcContactiveApiServicesAuthService", services.service.AuthService)

            .service("tcContactiveApiStorageApi", storage.api.StorageApi)
            .service("tcContactiveApiStorageService", storage.service.StorageService)

            .service("tcContactiveApiTestApi", test.api.TestApi)
            .service("tcContactiveApiTestService", test.service.TestService)
    }

    angular.element(document).ready(registerAngularModule);
}

