"use strict";

module tpn.commons.contactiveApi.shared.model {

    export interface IUserDevice {
        deviceId: number;
        clientId: string;
        pushToken: string;
        deviceType: string;
        model: string;
        name: string;
        appVersion: string;
        osType: string;
        osVersion: string;
        pushEnabled: boolean;
    }

}