"use strict";

module tpn.commons.contactiveApi.contact.datasource {

    import IItemDatasource = tpn.commons.contactiveApi.item.datasource.IItemDatasource;
    import IItemMap = tpn.commons.contactiveApi.item.model.IItemMap;
    import IContactItem = tpn.commons.contactiveApi.contact.model.IContactItem;

    export interface IContactItemDatasource extends IItemDatasource<IContactItem> {
        getAllGroupedName?(field:string,searchText:string): ng.IPromise<IItemMap<IContactItem>>;
        getByGroupId(groupId: string): ng.IPromise<IContactItem[]>;
        clear(): ng.IPromise<void>;
    }

}