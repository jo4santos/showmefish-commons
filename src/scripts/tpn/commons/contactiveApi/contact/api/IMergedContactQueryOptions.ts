"use strict";

module tpn.commons.contactiveApi.contact.api {

    export interface IMergedContactQueryOptions {
        refreshOrigins?: boolean;
    }

}