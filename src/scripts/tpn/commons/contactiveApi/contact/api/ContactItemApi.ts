"use strict";

module tpn.commons.contactiveApi.contact.api {

    import ApiClient        = tpn.commons.contactiveApi.api.ApiClient;
    import ItemApi          = tpn.commons.contactiveApi.item.api.ItemApi;
    import IContactItem     = tpn.commons.contactiveApi.contact.model.IContactItem;
    import IContactGroup    = tpn.commons.contactiveApi.contact.model.IContactGroup;
    import ItemType         = tpn.commons.contactiveApi.item.model.ItemType;

    import StringUtils      = tpn.commons.shared.util.StringUtils;

    /**
     * API for /users/me endpoint
     */

    export class ContactItemApi extends ItemApi<IContactItem> {

        public static $inject = ["$q", "tcContactiveApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {

            super($q, apiClient, ItemType.CONTACT);
        }

        public getMergedContact(groupId:string,options?:IMergedContactQueryOptions):ng.IPromise<any> {

            if (options) {
                options.refreshOrigins = options.refreshOrigins || false;
            }

            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .one("groups")
                .one(groupId)
                .one("merged")
                .get(options)
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }

        public refreshItemGroup(groupId: string):ng.IPromise<IContactGroup> {
            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .one("groups")
                .one(groupId)
                .post("refresh")
                .then((data:any)=> {
                    if (data) return data.plain();
                });
        }

        /**
         * Forces the merging of items into a contact group
         *
         * Endpoint: PUT /users/{userId}/contacts/groups/{groupId}/items/{itemIds}
         *
         * @param groupId
         * @param itemIds
         * @returns {IPromise<T[]>}
         */
        public mergeForce(groupId: string, itemIds: string[]):ng.IPromise<IContactItem[]> {
            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .one("groups")
                .one(groupId)
                .one("items")
                .one(StringUtils.join(",", itemIds))
                .put()
                .then((data:any)=> {
                    if (data) return data.plain();
                });
        }

        /**
         * Forces the unmerging of items from a contact group
         *
         * Endpoint: DELETE /users/{userId}/contacts/groups/{groupId}/items/{itemIds}
         *
         * @param groupId
         * @param itemIds
         * @returns {IPromise<T[]>}
         */
        public unmergeForce(groupId: string, itemIds: string[]):ng.IPromise<IContactItem[]> {
            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .one("groups")
                .one(groupId)
                .one("items")
                .one(StringUtils.join(",", itemIds))
                .remove()
                .then((data:any)=> {
                    if (data) return data.plain();
                });
        }

    }
}