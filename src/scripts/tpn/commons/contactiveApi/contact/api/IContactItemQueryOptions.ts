"use strict";

module tpn.commons.contactiveApi.contact.api {

    import IItemQueryOptions = tpn.commons.contactiveApi.item.api.IItemQueryOptions;

    export interface IContactItemQueryOptions extends IItemQueryOptions {
        chatIds?: string;
    }

}