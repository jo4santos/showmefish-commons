"use strict";

module tpn.commons.contactiveApi.contact.model {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IAddress {

        address1: string;
        address2: string;
        city: string;
        state: string;
        zip: string;
        country: string;
        latitude: string;
        longitude: string;

    }

    export class Address {

        private model: IAddress;

        constructor(model: IAddress) {
            this.model = model;
        }

        public getFormattedAddressOneLine(): string {
            var addresses: string[] = [];
            if (!StringUtils.isEmpty(this.model.address1)) {
                addresses.push(this.model.address1);
            }
            if (!StringUtils.isEmpty(this.model.address2) && StringUtils.join(" ", addresses).indexOf(this.model.address2) != -1) {
                addresses.push(this.model.address2);
            }
            if (!StringUtils.isEmpty(this.model.city) && StringUtils.join(" ", addresses).indexOf(this.model.city) != -1) {
                addresses.push(this.model.city);
            }
            if (!StringUtils.isEmpty(this.model.state) && StringUtils.join(" ", addresses).indexOf(this.model.state) != -1) {
                addresses.push(this.model.state);
            }
            if (!StringUtils.isEmpty(this.model.zip) && StringUtils.join(" ", addresses).indexOf(this.model.zip) != -1) {
                addresses.push(this.model.zip);
            }
            if (!StringUtils.isEmpty(this.model.country) && StringUtils.join(" ", addresses).indexOf(this.model.country) != -1) {
                addresses.push(this.model.country);
            }
            return StringUtils.join(", ", addresses);
        }

        public equals(another: Address): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.address1, another.model.address1) &&
                   StringUtils.equals(this.model.address2, another.model.address2) &&
                   StringUtils.equals(this.model.city, another.model.city) &&
                   StringUtils.equals(this.model.state, another.model.state) &&
                   StringUtils.equals(this.model.zip, another.model.zip) &&
                   StringUtils.equals(this.model.country, another.model.country) &&
                   StringUtils.equals(this.model.latitude, another.model.latitude) &&
                   StringUtils.equals(this.model.longitude, another.model.longitude);
        }

        public compareTo(another: Address): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            return StringUtils.compareLength(this.getFormattedAddressOneLine(), another.getFormattedAddressOneLine());
        }
    }

}