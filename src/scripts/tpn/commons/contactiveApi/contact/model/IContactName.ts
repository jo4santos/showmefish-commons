"use strict";

module tpn.commons.contactiveApi.contact.model {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IContactName {

        firstName: string;
        middleName: string;
        lastName: string;
        suffix: string;
        prefix: string;


        normalized: string;
    }

    export class ContactName {

        private model: IContactName;

        constructor(model: IContactName) {
            this.model = model;
        }

        public getFullName(): string {
            var names: string[] = [];
            if (!StringUtils.isEmpty(this.model.firstName)) {
                names.push(this.model.firstName.trim());
            }
            if (!StringUtils.isEmpty(this.model.middleName)) {
                names.push(this.model.middleName.trim());
            }
            if (!StringUtils.isEmpty(this.model.lastName)) {
                names.push(this.model.lastName.trim());
            }
            return StringUtils.join(" ", names);
        }

        public equals(another: ContactName): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.firstName, another.model.firstName) &&
                   StringUtils.equals(this.model.middleName, another.model.middleName) &&
                   StringUtils.equals(this.model.lastName, another.model.lastName);
        }

        public compareTo(another: ContactName): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            return StringUtils.compare(this.getFullName(), another.getFullName());
        }
    }

}