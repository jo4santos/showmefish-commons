"use strict";

module tpn.commons.contactiveApi.contact.model {

    export class ContactEmailType {

        static PERSONAL = "personal";
        static WORK = "work";
        static OTHER = "other";

    }

}