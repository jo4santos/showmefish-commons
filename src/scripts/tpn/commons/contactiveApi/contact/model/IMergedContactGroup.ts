"use strict";

module tpn.commons.contactiveApi.contact.model {

    export interface IMergedContactGroup extends IContact {

        origins: IContactItemOrigin[];

        groupId: string;

        about: string[];

        chat: IInstantMessaging[];

    }

}