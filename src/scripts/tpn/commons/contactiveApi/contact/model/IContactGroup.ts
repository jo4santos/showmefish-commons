"use strict";

module tpn.commons.contactiveApi.contact.model {

    export interface IContactGroup {

        groupId: string;

        contacts: IContactItem[];

    }

}