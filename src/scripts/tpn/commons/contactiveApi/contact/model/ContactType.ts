"use strict";

module tpn.commons.contactiveApi.contact.model {

    export class ContactType {

        static SINGLE = "single";
        static INSTITUTION = "institution";

    }

}