"use strict";

module tpn.commons.contactiveApi.contact.model {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IInstantMessaging {

        originName: string;
        originImId: string;

    }

    export class InstantMessaging {

        private model: IInstantMessaging;

        constructor(model: IInstantMessaging) {
            this.model = model;
        }

        public equals(another: InstantMessaging): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.originName, another.model.originName) &&
                   StringUtils.equals(this.model.originImId, another.model.originImId);
        }

        public compareTo(another: InstantMessaging): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            var compareResult: number = StringUtils.compare(this.model.originName, another.model.originName);
            if (compareResult == 0) {
                compareResult = StringUtils.compare(this.model.originImId, another.model.originImId);
            }

            return compareResult;
        }
    }

}