"use strict";

module tpn.commons.contactiveApi.contact.model {

    export interface IContact {

        name: IContactName;

        contactType: string;

        picture: IContactPicture[];

        url: string[];

        address: IAddress[];

        statusUpdate: IContactStatus[];

        work: work.IWork[];

        education: IEducation[];

        email: IContactEmail[];

        phone: IContactPhone[];

        nickname: string[];

    }

}