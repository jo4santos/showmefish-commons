"use strict";

module tpn.commons.contactiveApi.contact.model {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IContactStatus {

        message: string;
        statusId: string;
        time: number;
        statusUrl: string;
        previewPictureUrl: string;
        mediaUrl: string;
        mediaContentType: string;
        mediaDescription: string;

    }

    export class ContactStatus {

        private model: IContactStatus;

        constructor(model: IContactStatus) {
            this.model = model;
        }

        public equals(another: ContactStatus): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.statusId, another.model.statusId);
        }

        public compareTo(another: ContactStatus): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            if (this.model.time < another.model.time) {
                return -1;
            } else if (this.model.time > another.model.time) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}