"use strict";

module tpn.commons.contactiveApi.contact.model {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IContactEmail {

        email: string;
        type: string;

    }

    export class ContactEmail {

        static GOOGLE_DOMAIN: string = "gmail.com";
        static FB_DOMAIN: string = "facebook.com";

        private model: IContactEmail;

        private domain: string;
        private account: string;

        constructor(model: IContactEmail) {
            this.model = model;

            if (!StringUtils.isEmpty(model.email)) {
                var result: string[] = model.email.split("@");
                if (result.length == 2) {
                    this.domain = result[1].toLowerCase();
                    this.account = result[0];
                    if (StringUtils.equalsIgnoreCase(this.domain, ContactEmail.GOOGLE_DOMAIN)) {
                        this.account = this.account.replace(".", "");
                    }
                } else {
                    this.account = model.email;
                }
            }
        }

        public equals(another: ContactEmail): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.domain, another.domain) && StringUtils.equals(this.account, another.account);
        }

        public compareTo(another: ContactEmail): number {
            if (this.equals(another)) {
                return 0;
            }
            if (!StringUtils.equalsIgnoreCase(this.domain, another.domain)) {
                if (StringUtils.equalsIgnoreCase(this.domain, ContactEmail.FB_DOMAIN)) {
                    return 1;
                } else if (StringUtils.equalsIgnoreCase(another.domain, ContactEmail.FB_DOMAIN)) {
                    return -1;
                }
            }
            var compareResult: number = StringUtils.compare(this.domain, another.domain);
            if (compareResult == 0) {
                compareResult = StringUtils.compare(this.account, another.account);
            }

            return compareResult;
        }
    }

}