"use strict";

module tpn.commons.contactiveApi.contact.model {

    import IItemWithOrigin = tpn.commons.contactiveApi.item.model.IItemWithOrigin;

    export interface IContactItem extends IContact, IItemWithOrigin {

        groupId: string;

        about: string;

        chat: IInstantMessaging[];

    }

}