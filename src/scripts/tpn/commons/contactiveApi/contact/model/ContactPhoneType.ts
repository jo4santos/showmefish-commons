"use strict";

module tpn.commons.contactiveApi.contact.model {

    export class ContactPhoneType {

        static HOME = "home";
        static WORK = "work";
        static MOBILE = "mobile";
        static FAX = "fax";
        static OTHER = "other";

    }

}