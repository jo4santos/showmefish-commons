"use strict";

module tpn.commons.contactiveApi.contact.model {

    import IPinnableModel = tpn.commons.contactiveApi.contact.model.common.IPinnableModel;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IContactPhone extends IPinnableModel {

        countryCode: number;
        original: string;
        normalized: string;
        type: string;
        extension: string;
        geocode: string;


        e164Format: string;
    }

    export class ContactPhone {

        private model: IContactPhone;

        constructor(model: IContactPhone) {
            this.model = model;
        }

        public equals(another: ContactPhone): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.original, another.model.original);
        }


        public compareTo(another: ContactPhone): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            return StringUtils.compare(this.model.original, another.model.original);
        }
    }

}