"use strict";

module tpn.commons.contactiveApi.contact.model {

    export interface IContactItemOrigin {

        originName: string;
        originItemId: string;
        originAccount: string;

    }

}