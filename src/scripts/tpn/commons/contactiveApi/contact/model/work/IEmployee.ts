"use strict";

module tpn.commons.contactiveApi.contact.model.work {

    export interface IEmployee {

        name: IContactName;
        position: string;

    }

}