"use strict";

module tpn.commons.contactiveApi.contact.model.work {

    export interface ICompanyInfo {

        name: string;
        address: IAddress;

    }

}