"use strict";

module tpn.commons.contactiveApi.contact.model.work {

    import IOriginIdentifiableModel = tpn.commons.contactiveApi.contact.model.common.IOriginIdentifiableModel;

    import StringUtils = tpn.commons.shared.util.StringUtils;
    import DateUtils = tpn.commons.shared.util.DateUtils;

    export interface IWork extends IOriginIdentifiableModel {

        active: boolean;
        company: string;
        position: string;
        started: number;
        finished: number;
        manager: IEmployee;
        subordinate: IEmployee[];
        department: string;
        division: string;
        alias: string;
        location: IAddress;
        summary: string;
        companyInfo: ICompanyInfo;

    }

    export class Work {

        private model: IWork;

        constructor(model: IWork) {
            this.model = model;
        }

        public equals(another: Work): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.company, another.model.company) &&
                   StringUtils.equals(this.model.position, another.model.position);
        }

        public compareTo(another: Work): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model.active != another.model.active) {
                if (this.model.active) {
                    return 1;
                } else {
                    return -1;
                }
            }

            if ((this.model.started == 0) && (another.model.started == 0)) {
                var compareResult: number = StringUtils.compare(this.model.company, another.model.company);
                if (compareResult == 0) {
                    compareResult = StringUtils.compare(this.model.position, another.model.position);
                }

                return compareResult;
            } else if (this.model.started == 0) {
                return 1;
            } else if (another.model.started == 0) {
                return -1;
            }

            var monthStarted: number = DateUtils.getMonthsFromMillis(this.model.started);
            var anotherMonthStarted: number = DateUtils.getMonthsFromMillis(another.model.started);

            if (monthStarted == anotherMonthStarted) {
                var monthFinished: number = DateUtils.getMonthsFromMillis(this.model.finished);
                var anotherMonthFinished: number = DateUtils.getMonthsFromMillis(another.model.finished);
                if (anotherMonthFinished == monthFinished) {
                    var compareResult: number = StringUtils.compare(this.model.company, another.model.company);
                    if (compareResult == 0) {
                        compareResult = StringUtils.compare(this.model.position, another.model.position);
                    }

                    return compareResult;
                }
            } else {
                if (monthStarted > anotherMonthStarted) {
                    if (StringUtils.isEmpty(this.model.position) && !StringUtils.isEmpty(another.model.position)) {
                        return 1;
                    }
                    return -1;
                } else {
                    if (!StringUtils.isEmpty(this.model.position) && StringUtils.isEmpty(another.model.position)) {
                        return -1;
                    }
                    return 1;
                }
            }
            return 0;
        }
    }

}