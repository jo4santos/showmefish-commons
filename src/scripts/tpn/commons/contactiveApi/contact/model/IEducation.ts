"use strict";

module tpn.commons.contactiveApi.contact.model {

    import IOriginIdentifiableModel = tpn.commons.contactiveApi.contact.model.common.IOriginIdentifiableModel;

    import StringUtils = tpn.commons.shared.util.StringUtils;
    import DateUtils = tpn.commons.shared.util.DateUtils;

    export interface IEducation extends IOriginIdentifiableModel {

        institution: string;
        degree: string;
        started: number;
        finished: number;

    }

    export class Education {

        private model: IEducation;

        constructor(model: IEducation) {
            this.model = model;
        }

        public equals(another: Education): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.institution, another.model.institution) &&
                   StringUtils.equals(this.model.degree, another.model.degree);
        }

        public compareTo(another: Education): number {
            if (this.equals(another)) {
                return 0;
            }

            if ((this.model.started == 0) && (another.model.started == 0)) {
                var compareResult: number = StringUtils.compare(this.model.institution, another.model.institution);
                if (compareResult == 0) {
                    compareResult = StringUtils.compare(this.model.degree, another.model.degree);
                }

                return compareResult;
            } else if (this.model.started == 0) {
                return 1;
            } else if (another.model.started == 0) {
                return -1;
            }

            var monthStarted: number = DateUtils.getMonthsFromMillis(this.model.started);
            var anotherMonthStarted: number = DateUtils.getMonthsFromMillis(another.model.started);

            if (monthStarted == anotherMonthStarted) {
                var monthFinished = DateUtils.getMonthsFromMillis(this.model.finished);
                var anotherMonthFinished = DateUtils.getMonthsFromMillis(another.model.finished);
                if (anotherMonthFinished == monthFinished) {
                    var compareResult: number = StringUtils.compare(this.model.institution, another.model.institution);
                    if (compareResult == 0) {
                        compareResult = StringUtils.compare(this.model.degree, another.model.degree);
                    }

                    return compareResult;
                }
            } else {
                if (monthStarted > anotherMonthStarted) {
                    if (StringUtils.isEmpty(this.model.degree) && !StringUtils.isEmpty(another.model.degree)) {
                        return 1;
                    }
                    return -1;
                } else {
                    if (!StringUtils.isEmpty(this.model.degree) && StringUtils.isEmpty(another.model.degree)) {
                        return -1;
                    }
                    return 1;
                }
            }
            return 0;
        }
    }

}