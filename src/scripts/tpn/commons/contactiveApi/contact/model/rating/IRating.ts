"use strict";

module tpn.commons.contactiveApi.contact.model.rating {

    export interface IRating {

        rating: number;
        ratingImgUrl: string;
        reviewCount: number;
        latestReviews: IReview[];

    }

    export class Rating {

        private model: IRating;

        constructor(model: IRating) {
            this.model = model;
        }

        public equals(another: Rating): boolean {
            if (this.model == another.model) {
                return true;
            }

            return false;
        }

        public compareTo(another: Rating): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model.rating < another.model.rating) {
                return -1;
            } else if (this.model.rating > another.model.rating) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}