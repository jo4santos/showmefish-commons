"use strict";

module tpn.commons.contactiveApi.contact.model.rating {

    export interface IReview {

        date: number;
        rating: number;
        text: string;

    }

}