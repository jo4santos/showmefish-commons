"use strict";

module tpn.commons.contactiveApi.contact.model {

    import IPinnableModel = tpn.commons.contactiveApi.contact.model.common.IPinnableModel;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export interface IContactPicture extends IPinnableModel {

        smallUrl: string;
        largeUrl: string;
        retrieved: number;

    }

    export class ContactPicture {

        private model: IContactPicture;

        constructor(model: IContactPicture) {
            this.model = model;
        }

        public equals(another: ContactPicture): boolean {
            if (this.model == another.model) {
                return true;
            }

            if (this.model == null || another.model == null) {
                return false;
            }

            return StringUtils.equals(this.model.smallUrl, another.model.smallUrl);
        }

        public compareTo(another: ContactPicture): number {
            if (this.equals(another)) {
                return 0;
            }

            if (this.model == null) {
                return -1;
            }

            if (another.model == null) {
                return 1;
            }

            if (this.model.retrieved < another.model.retrieved) {
                return -1;
            } else if (this.model.retrieved > another.model.retrieved) {
                return 1;
            }

            return StringUtils.compare(this.model.smallUrl, this.model.smallUrl);
        }
    }

}