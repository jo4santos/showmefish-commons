"use strict";

module tpn.commons.contactiveApi.contact.model.common {

    export interface IPinnableModel {

        pinTime: number;

    }

}