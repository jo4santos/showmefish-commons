"use strict";

module tpn.commons.contactiveApi.contact.model.common {

    export interface IOriginIdentifiableModel {

        originFieldId: string;
        originFieldName: string;
        originFieldUrl: string;

    }
}