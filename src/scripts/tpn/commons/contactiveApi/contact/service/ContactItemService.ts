"use strict";

module tpn.commons.contactiveApi.contact.service {

    import ContactItemApi = tpn.commons.contactiveApi.contact.api.ContactItemApi;
    import IContactItem = tpn.commons.contactiveApi.contact.model.IContactItem;
    import IContactGroup = tpn.commons.contactiveApi.contact.model.IContactGroup;
    import IContactItemDatasource = tpn.commons.contactiveApi.contact.datasource.IContactItemDatasource;
    import IItemDatasource = tpn.commons.contactiveApi.item.datasource.IItemDatasource;
    import IItemMap = tpn.commons.contactiveApi.item.model.IItemMap;
    import IPromise = ng.IPromise;
    import ItemApi = tpn.commons.contactiveApi.item.api.ItemApi;
    import ItemService = tpn.commons.contactiveApi.item.service.ItemService;

    export class ContactItemService extends ItemService<IContactItem> {

        public static $inject = ["$log", "$q", "tcContactiveApiContactItemApi", "contactItemDatasource"];

        protected api: ContactItemApi;
        protected datasource: IContactItemDatasource;

        public constructor($log:ng.ILogService,
                           $q:ng.IQService,
                           ContactItemApi:ContactItemApi,
                           contactItemDatasource:IContactItemDatasource) {
            super($log, $q, ContactItemApi, contactItemDatasource);
        }

        /**
         * Gets a map of contacts filtered by name from a given datasource (nedb for example), in which the key
         * is the <field>
         *
         * @params datasource
         * @params field
         * @params searchText
         * @returns {IPromise<IItemMap>}
         */

        public getItemsGroupedName(field:string, searchText:string):IPromise<IItemMap<IContactItem>> {
            return this.datasource.getAllGroupedName(field, searchText);
        }

        /**
         * @param groupId
         * @returns {ng.IPromise<IItem[]>}
         */
        public getByGroupId(groupId: string): IPromise<IContactItem[]> {
            return this.datasource.getByGroupId(groupId);
        }

        public getMergedContact(groupId: string): ng.IPromise<any> {
            return this.api.getMergedContact(groupId);
        }

        /**
         * @param groupId
         * @returns {ng.IPromise<IContactItem[]>}
         */
        public refreshByGroupId(groupId: string): IPromise<IContactItem[]> {
            var dfrd:ng.IDeferred<IContactItem[]> = this.$q.defer<IContactItem[]>();

            this.api.refreshItemGroup(groupId)
                .then((contactGroup: IContactGroup)=>{
                    if (contactGroup) {
                        this.storeAndNotify(contactGroup.contacts)
                            .then((storedContacts:IContactItem[])=>{
                                dfrd.resolve(contactGroup.contacts);
                            }).catch((err:any)=>{
                                dfrd.reject(err);
                            });
                    } else {
                        dfrd.resolve(null);
                    }
                })
                .catch((err:any)=> {
                    dfrd.reject(err);
                });

            return dfrd.promise;
        }

        /**
         * @param groupId
         * @param itemIds
         * @returns {ng.IPromise<IContactItem[]>}
         */
        public mergeForce(groupId: string, itemIds: string[]): IPromise<IContactItem[]> {
            var dfrd:ng.IDeferred<IContactItem[]> = this.$q.defer<IContactItem[]>();

            this.api.mergeForce(groupId, itemIds)
                .then((contactItems: IContactItem[])=>{
                    this.storeAndNotify(contactItems)
                        .then((storedContacts:IContactItem[])=>{
                            dfrd.resolve(contactItems);
                        }).catch((err:any)=>{
                            dfrd.reject(err);
                        });
                })
                .catch((err:any)=> {
                    dfrd.reject(err);
                });

            return dfrd.promise;
        }

        /**
         * @param groupId
         * @param itemIds
         * @returns {ng.IPromise<IContactItem[]>}
         */
        public unmergeForce(groupId: string, itemIds: string[]): IPromise<IContactItem[]> {
            var dfrd:ng.IDeferred<IContactItem[]> = this.$q.defer<IContactItem[]>();

            this.api.unmergeForce(groupId, itemIds)
                .then((contactItems: IContactItem[])=>{
                    this.storeAndNotify(contactItems)
                        .then((storedContacts:IContactItem[])=>{
                            dfrd.resolve(contactItems);
                        }).catch((err:any)=>{
                            dfrd.reject(err);
                        });
                })
                .catch((err:any)=> {
                    dfrd.reject(err);
                });

            return dfrd.promise;
        }

    }
}