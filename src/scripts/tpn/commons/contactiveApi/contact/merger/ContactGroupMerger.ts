"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactItem         = tpn.commons.contactiveApi.contact.model.IContactItem;
    import IContactGroup        = tpn.commons.contactiveApi.contact.model.IContactGroup;
    import IMergedContactGroup  = tpn.commons.contactiveApi.contact.model.IMergedContactGroup;
    import IContactItemOrigin   = tpn.commons.contactiveApi.contact.model.IContactItemOrigin;

    import IContactName         = tpn.commons.contactiveApi.contact.model.IContactName;
    import IAddress             = tpn.commons.contactiveApi.contact.model.IAddress;
    import IContactPicture      = tpn.commons.contactiveApi.contact.model.IContactPicture;
    import IContactEmail        = tpn.commons.contactiveApi.contact.model.IContactEmail;
    import IContactPhone        = tpn.commons.contactiveApi.contact.model.IContactPhone;
    import IWork                = tpn.commons.contactiveApi.contact.model.work.IWork;
    import IEducation           = tpn.commons.contactiveApi.contact.model.IEducation;
    import IContactStatus       = tpn.commons.contactiveApi.contact.model.IContactStatus;
    import IInstantMessaging    = tpn.commons.contactiveApi.contact.model.IInstantMessaging;


    export class ContactGroupMerger {

        private sources: any;

        constructor(sources: any) {
            this.sources = sources;
        }

        public merge(contactGroup: IContactGroup): IMergedContactGroup {
            var mergedContactGroup: IMergedContactGroup = {
                origins:        this.extractContactItemOrigins(contactGroup.contacts),

                groupId:        contactGroup.groupId,
                name:           this.extractFirst     <IContactName>        (contactGroup.contacts, "name",         new ContactNameFieldComparator()),
                contactType:    this.extractFirst     <string>              (contactGroup.contacts, "contactType",  new StringFieldComparator()),
                picture:        this.extractCollection<IContactPicture>     (contactGroup.contacts, "picture",      new ContactPictureFieldComparator()),
                url:            this.extractCollection<string>              (contactGroup.contacts, "url",          new StringFieldComparator()),
                address:        this.extractCollection<IAddress>            (contactGroup.contacts, "address",      new AddressFieldComparator()),
                statusUpdate:   this.extractCollection<IContactStatus>      (contactGroup.contacts, "statusUpdate", new ContactStatusFieldComparator()),
                work:           this.extractCollection<IWork>               (contactGroup.contacts, "work",         new WorkFieldComparator()),
                education:      this.extractCollection<IEducation>          (contactGroup.contacts, "education",    new EducationFieldComparator()),
                email:          this.extractCollection<IContactEmail>       (contactGroup.contacts, "email",        new ContactEmailFieldComparator()),
                phone:          this.extractCollection<IContactPhone>       (contactGroup.contacts, "phone",        new ContactPhoneFieldComparator()),
                nickname:       this.extractCollection<string>              (contactGroup.contacts, "nickname",     new StringFieldComparator()),
                about:          this.extractCollection<string>              (contactGroup.contacts, "about",        new StringFieldComparator()),
                chat:           this.extractCollection<IInstantMessaging>   (contactGroup.contacts, "chat",         new InstantMessagingFieldComparator())
            };

            return mergedContactGroup;
        }

        private extractContactItemOrigins(contactItems: IContactItem[]): IContactItemOrigin[] {
            var contactItemOrigins: IContactItemOrigin[] = [];

            contactItems.forEach((contactItem: IContactItem)=>{
                var contactItemOrigin: IContactItemOrigin = {
                    originName: contactItem.originName,
                    originItemId: contactItem.originItemId,
                    originAccount: contactItem.originAccount
                };
                contactItemOrigins.push(contactItemOrigin);
            });

            return contactItemOrigins;
        }

        private extractFirst<T>(contactItems: IContactItem[], fieldName: string, fieldComparator?: IContactItemFieldComparator<T>): T {
            var elements: T[] = this.extractCollection(contactItems, fieldName, fieldComparator);

            if (elements.length > 0) {
                return elements[0];
            } else {
                return null;
            }
        }

        private extractCollection<T>(contactItems: IContactItem[], fieldName: string, fieldComparator?: IContactItemFieldComparator<T>): T[] {
            var contactItemFields: IContactItemField<T>[] = [];

            contactItems.forEach((contactItem: IContactItem)=>{
                var weight: number = this.sources["default"].weight[fieldName];

                if (this.sources[contactItem.originName] && this.sources[contactItem.originName].weight[fieldName]) {
                    weight = this.sources[contactItem.originName].weight[fieldName];
                }

                if (contactItem[fieldName]) {
                    if (Array.isArray(contactItem[fieldName])) {
                        contactItem[fieldName].forEach((element: T)=>{
                            this.addToCollection(contactItemFields, {weight: weight, value: element}, fieldComparator);
                        });
                    } else {
                        this.addToCollection(contactItemFields, {weight: weight, value: contactItem[fieldName]}, fieldComparator);
                    }
                }
            });

            if (contactItemFields.length > 0) {
                if (fieldComparator && fieldComparator.compare) {
                    contactItemFields.sort((o1: IContactItemField<T>, o2: IContactItemField<T>)=>{
                        return fieldComparator.compare(o1.value, o1.weight, o2.value, o2.weight);
                    });
                }
                return contactItemFields.map((contactItemField)=>{
                    return contactItemField.value;
                }).reverse();
            } else {
                return [];
            }
        }

        private addToCollection<T>(elements: IContactItemField<T>[], element: IContactItemField<T>, fieldComparator?: IContactItemFieldComparator<T>) {
            var isValid: boolean = true;
            var isRepeated: boolean = false;

                if (fieldComparator && fieldComparator.isValid) {
                    isValid = fieldComparator.isValid(element.value);
                }

                if (fieldComparator && fieldComparator.equals) {
                    isRepeated = elements.some((existingElement: IContactItemField<T>)=>{
                        return fieldComparator.equals(existingElement.value, element.value);
                    });
                }

                if (isValid && !isRepeated) {
                    elements.push(element);
                }
        }

    }

}