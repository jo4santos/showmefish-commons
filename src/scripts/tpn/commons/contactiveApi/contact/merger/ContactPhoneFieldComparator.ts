"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactPhone = tpn.commons.contactiveApi.contact.model.IContactPhone;
    import ContactPhone = tpn.commons.contactiveApi.contact.model.ContactPhone;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class ContactPhoneFieldComparator implements IContactItemFieldComparator<IContactPhone> {

        isValid(o: IContactPhone): boolean {
            return !StringUtils.isEmpty(o.original);
        }

        equals(o1: IContactPhone, o2: IContactPhone): boolean {
            var o1Phone: ContactPhone = new ContactPhone(o1);
            var o2Phone: ContactPhone = new ContactPhone(o2);

            return o1Phone.equals(o2Phone);
        }

        compare(o1: IContactPhone, o1Weight: number, o2: IContactPhone, o2Weight: number): number {
            if (o1.pinTime != o2.pinTime) {
                if (o1.pinTime == null) {
                    return -1;
                }
                if (o2.pinTime == null) {
                    return 1;
                }

                if (o1.pinTime < o2.pinTime) {
                    return -1;
                } else {
                    return 1;
                }
            }

            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Phone: ContactPhone = new ContactPhone(o1);
            var o2Phone: ContactPhone = new ContactPhone(o2);

            return o1Phone.compareTo(o2Phone);
        }

    }

}
