"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IInstantMessaging = tpn.commons.contactiveApi.contact.model.IInstantMessaging;
    import InstantMessaging = tpn.commons.contactiveApi.contact.model.InstantMessaging;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class InstantMessagingFieldComparator implements IContactItemFieldComparator<IInstantMessaging> {

        isValid(o: IInstantMessaging): boolean {
            return !StringUtils.areEmpty(o.originName, o.originImId);
        }

        equals(o1: IInstantMessaging, o2: IInstantMessaging): boolean {
            var o1InstantMessaging: InstantMessaging = new InstantMessaging(o1);
            var o2InstantMessaging: InstantMessaging = new InstantMessaging(o2);

            return o1InstantMessaging.equals(o2InstantMessaging);
        }

        compare(o1: IInstantMessaging, o1Weight: number, o2: IInstantMessaging, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1InstantMessaging: InstantMessaging = new InstantMessaging(o1);
            var o2InstantMessaging: InstantMessaging = new InstantMessaging(o2);

            return o1InstantMessaging.compareTo(o2InstantMessaging);
        }

    }

}
