"use strict";

module tpn.commons.contactiveApi.contact.merger {

    export interface IContactItemFieldComparator<T> {

        isValid?(o: T): boolean;

        equals?(o1: T, o2: T): boolean;

        compare?(o1: T, o1Weight: number, o2: T, o2Weight: number): number;

    }

}
