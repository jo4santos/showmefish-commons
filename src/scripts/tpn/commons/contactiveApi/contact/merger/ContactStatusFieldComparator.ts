"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactStatus = tpn.commons.contactiveApi.contact.model.IContactStatus;
    import ContactStatus = tpn.commons.contactiveApi.contact.model.ContactStatus;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class ContactStatusFieldComparator implements IContactItemFieldComparator<IContactStatus> {

        isValid(o: IContactStatus): boolean {
            return !StringUtils.isEmpty(o.message) && o.time != null;
        }

        equals(o1: IContactStatus, o2: IContactStatus): boolean {
            var o1Status: ContactStatus = new ContactStatus(o1);
            var o2Status: ContactStatus = new ContactStatus(o2);

            return o1Status.equals(o2Status);
        }

        compare(o1: IContactStatus, o1Weight: number, o2: IContactStatus, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Status: ContactStatus = new ContactStatus(o1);
            var o2Status: ContactStatus = new ContactStatus(o2);

            return o1Status.compareTo(o2Status);
        }

    }

}
