"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class StringFieldComparator implements IContactItemFieldComparator<string> {

        isValid(o: string): boolean {
            return !StringUtils.areEmpty(o);
        }

        equals(o1: string, o2: string): boolean {
            return StringUtils.equals(o1, o2);
        }

        compare(o1: string, o1Weight: number, o2: string, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            return StringUtils.compare(o1, o2);
        }

    }

}
