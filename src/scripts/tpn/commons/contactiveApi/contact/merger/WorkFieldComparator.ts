"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IWork = tpn.commons.contactiveApi.contact.model.work.IWork;
    import Work = tpn.commons.contactiveApi.contact.model.work.Work;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class WorkFieldComparator implements IContactItemFieldComparator<IWork> {

        isValid(o: IWork): boolean {
            return !StringUtils.areEmpty(o.company, o.position);
        }

        equals(o1: IWork, o2: IWork): boolean {
            var o1Work: Work = new Work(o1);
            var o2Work: Work = new Work(o2);

            return o1Work.equals(o2Work);
        }

        compare(o1: IWork, o1Weight: number, o2: IWork, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Work: Work = new Work(o1);
            var o2Work: Work = new Work(o2);

            return o1Work.compareTo(o2Work);
        }

    }

}
