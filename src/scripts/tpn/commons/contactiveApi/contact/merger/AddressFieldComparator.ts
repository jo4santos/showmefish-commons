"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IAddress = tpn.commons.contactiveApi.contact.model.IAddress;
    import Address = tpn.commons.contactiveApi.contact.model.Address;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class AddressFieldComparator implements IContactItemFieldComparator<IAddress> {

        isValid(o: IAddress): boolean {
            return !StringUtils.areEmpty(o.address1, o.address2, o.city, o.state, o.zip, o.country, o.latitude, o.longitude);
        }

        equals(o1: IAddress, o2: IAddress): boolean {
            var o1Address: Address = new Address(o1);
            var o2Address: Address = new Address(o2);

            return o1Address.equals(o2Address);
        }

        compare(o1: IAddress, o1Weight: number, o2: IAddress, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Address: Address = new Address(o1);
            var o2Address: Address = new Address(o2);

            return o1Address.compareTo(o2Address);
        }

    }

}
