"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactEmail = tpn.commons.contactiveApi.contact.model.IContactEmail;
    import ContactEmail = tpn.commons.contactiveApi.contact.model.ContactEmail;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class ContactEmailFieldComparator implements IContactItemFieldComparator<IContactEmail> {

        isValid(o: IContactEmail): boolean {
            return !StringUtils.isEmpty(o.email);
        }

        equals(o1: IContactEmail, o2: IContactEmail): boolean {
            var o1Email: ContactEmail = new ContactEmail(o1);
            var o2Email: ContactEmail = new ContactEmail(o2);

            return o1Email.equals(o2Email);
        }

        compare(o1: IContactEmail, o1Weight: number, o2: IContactEmail, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Email: ContactEmail = new ContactEmail(o1);
            var o2Email: ContactEmail = new ContactEmail(o2);

            return o1Email.compareTo(o2Email);
        }

    }

}
