"use strict";

module tpn.commons.contactiveApi.contact.merger {

    export class IContactItemField<T> {

        weight: number;
        value: T;

    }

}