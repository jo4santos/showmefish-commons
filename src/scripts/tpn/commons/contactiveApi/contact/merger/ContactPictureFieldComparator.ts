"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactPicture = tpn.commons.contactiveApi.contact.model.IContactPicture;
    import ContactPicture = tpn.commons.contactiveApi.contact.model.ContactPicture;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class ContactPictureFieldComparator implements IContactItemFieldComparator<IContactPicture> {

        isValid(o: IContactPicture): boolean {
            return !StringUtils.isEmpty(o.smallUrl);
        }

        equals(o1: IContactPicture, o2: IContactPicture): boolean {
            var o1Picture: ContactPicture = new ContactPicture(o1);
            var o2Picture: ContactPicture = new ContactPicture(o2);

            return o1Picture.equals(o2Picture);
        }

        compare(o1: IContactPicture, o1Weight: number, o2: IContactPicture, o2Weight: number): number {
            if (o1.pinTime != o2.pinTime) {
                if (o1.pinTime == null) {
                    return -1;
                }
                if (o2.pinTime == null) {
                    return 1;
                }

                if (o1.pinTime < o2.pinTime) {
                    return -1;
                } else {
                    return 1;
                }
            }

            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Picture: ContactPicture = new ContactPicture(o1);
            var o2Picture: ContactPicture = new ContactPicture(o2);

            return o1Picture.compareTo(o2Picture);
        }

    }

}
