"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IContactName = tpn.commons.contactiveApi.contact.model.IContactName;
    import ContactName = tpn.commons.contactiveApi.contact.model.ContactName;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class ContactNameFieldComparator implements IContactItemFieldComparator<IContactName> {

        isValid(o: IContactName): boolean {
            return !StringUtils.areEmpty(o.firstName, o.middleName, o.lastName);
        }

        equals(o1: IContactName, o2: IContactName): boolean {
            var o1Name: ContactName = new ContactName(o1);
            var o2Name: ContactName = new ContactName(o2);

            return o1Name.equals(o2Name);
        }

        compare(o1: IContactName, o1Weight: number, o2: IContactName, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Name: ContactName = new ContactName(o1);
            var o2Name: ContactName = new ContactName(o2);

            return o1Name.compareTo(o2Name);
        }

    }

}
