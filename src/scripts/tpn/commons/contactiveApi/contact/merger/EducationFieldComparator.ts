"use strict";

module tpn.commons.contactiveApi.contact.merger {

    import IEducation = tpn.commons.contactiveApi.contact.model.IEducation;
    import Education = tpn.commons.contactiveApi.contact.model.Education;

    import StringUtils = tpn.commons.shared.util.StringUtils;

    export class EducationFieldComparator implements IContactItemFieldComparator<IEducation> {

        isValid(o: IEducation): boolean {
            return !StringUtils.isEmpty(o.institution);
        }

        equals(o1: IEducation, o2: IEducation): boolean {
            var o1Education: Education = new Education(o1);
            var o2Education: Education = new Education(o2);

            return o1Education.equals(o2Education);
        }

        compare(o1: IEducation, o1Weight: number, o2: IEducation, o2Weight: number): number {
            if (o1Weight < o2Weight) {
                return 1;
            } else if (o1Weight > o2Weight) {
                return -1;
            }

            var o1Education: Education = new Education(o1);
            var o2Education: Education = new Education(o2);

            return o1Education.compareTo(o2Education);
        }

    }

}
