"use strict";

module tpn.commons.contactiveApi.test.service {

    import IPromise = ng.IPromise;
    import TestApi = tpn.commons.contactiveApi.test.api.TestApi;

    export class TestService {

        protected $q:ng.IQService;
        protected testApi:TestApi;

        public static $inject = ["$q", "tcContactiveApiTestApi"];

        public constructor($q:ng.IQService,
                           TestApi:TestApi) {
            this.$q = $q;
            this.testApi = TestApi;
        }

        /**
         * Tests connection to server
         *
         * @param options
         *
         * @returns {IPromise<any>}
         */
        public test():ng.IPromise<any> {
            return this.testApi.test();
        }
    }
}