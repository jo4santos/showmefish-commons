"use strict";

module tpn.commons.contactiveApi.item.model {

    export interface IItemMap <T extends IItem> {
        [field: string]: T[];
    }
}