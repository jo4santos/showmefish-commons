"use strict";

module tpn.commons.contactiveApi.item.model {

    export interface IItemWithOrigin extends IItem {

        originName: string;
        originAccount: string;
        originItemId: string;
        originVersion: string;
        originItemUrl: string;

    }

}