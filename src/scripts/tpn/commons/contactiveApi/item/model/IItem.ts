"use strict";

module tpn.commons.contactiveApi.item.model {

    export interface IItem {
        itemId: number;
        revision: number;
        deleted: boolean;
    }

}