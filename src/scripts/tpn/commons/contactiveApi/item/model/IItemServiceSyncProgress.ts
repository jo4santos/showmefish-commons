"use strict";

module tpn.commons.contactiveApi.item.model {

    export interface IItemServiceSyncProgress <T extends IItem> {
        total: number;
        count: number;
        items: T[];
    }

}