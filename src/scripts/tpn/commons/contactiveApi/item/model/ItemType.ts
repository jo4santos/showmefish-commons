"use strict";

module tpn.commons.contactiveApi.item.model {

    export class ItemType
    {
        private endpoint: string;
        private type: string;

        constructor(endpoint:string, type:string){
            this.endpoint = endpoint;
            this.type = type;
        }

        getEndpoint():string  {
            return this.endpoint;
        }

        getType():string  {
            return this.type;
        }

        // values
        static CONTACT = new ItemType("contacts","contact");
        static EVENT = new ItemType("events","event");
    }

}