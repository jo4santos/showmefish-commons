"use strict";

module tpn.commons.contactiveApi.item.datasource {

    import IItem = tpn.commons.contactiveApi.item.model.IItem;
    import IItemDatasourceListener  = tpn.commons.contactiveApi.item.listener.IItemDatasourceListener;
    import IItemMap = tpn.commons.contactiveApi.item.model.IItemMap;

    export interface IItemDatasource<T extends IItem> {
        registerListener(callback:IItemDatasourceListener<T>);
        deregisterListener(callback:IItemDatasourceListener<T>);

        store(items:T[]):ng.IPromise<T[]>;
        getHeadRevision(): ng.IPromise<number>;
        getAll(): ng.IPromise<T[]>;
        getAllGrouped(field:string): ng.IPromise<IItemMap<T>>;
        getByItemId(itemId: number): ng.IPromise<T[]>;
    }

}