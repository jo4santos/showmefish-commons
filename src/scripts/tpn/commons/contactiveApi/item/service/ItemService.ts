"use strict";

module tpn.commons.contactiveApi.item.service {

    import IPromise             = ng.IPromise;

    import ListenerManager      = tpn.commons.shared.listener.ListenerManager;

    import IItem                    = tpn.commons.contactiveApi.item.model.IItem;
    import IItemMap                 = tpn.commons.contactiveApi.item.model.IItemMap;
    import IItemDatasource          = tpn.commons.contactiveApi.item.datasource.IItemDatasource;
    import IItemServiceListener     = tpn.commons.contactiveApi.item.listener.IItemServiceListener;
    import IItemDatasourceListener  = tpn.commons.contactiveApi.item.listener.IItemDatasourceListener;
    import IItemQueryResponse       = tpn.commons.contactiveApi.item.api.IItemQueryResponse;
    import IItemQueryOptions        = tpn.commons.contactiveApi.item.api.IItemQueryOptions;
    import IItemServiceSyncProgress = tpn.commons.contactiveApi.item.model.IItemServiceSyncProgress;
    import ItemApi                  = tpn.commons.contactiveApi.item.api.ItemApi;

    export class ItemService <T extends IItem> implements IItemDatasourceListener<T> {

        protected $log:ng.ILogService;
        protected $q:ng.IQService;
        protected api:ItemApi<T>;
        protected datasource:IItemDatasource<T>;
        protected listeners:ListenerManager<IItemServiceListener<T>>;

        protected isSyncing:boolean;
        protected needsSyncing:boolean;
        protected syncProgress:IItemServiceSyncProgress<T>;

        public constructor($log:ng.ILogService,
                           $q:ng.IQService,
                           ItemApi:ItemApi<T>,
                           datasource:IItemDatasource<T>) {
            this.$log = $log;
            this.$q = $q;
            this.api = ItemApi;
            this.datasource = datasource;

            this.listeners = new ListenerManager<IItemServiceListener<T>>($log);

            this.datasource.registerListener(this);
        }

        /**
         * Get Contacts from backend starting on the latest revision user has on his database. Then update the datasource
         *
         * @returns {IPromise<void>}
         */
        public sync():IPromise<void> {
            var dfrd:ng.IDeferred<void> = this.$q.defer<void>();
            var options:IItemQueryOptions = {};

            if(this.isSyncing) {
                this.$log.info("Sync is already in progress, will resync after it's complete");
                this.needsSyncing = true;
                return null;
            }

            this.isSyncing = true;
            this.needsSyncing = false;

            this.datasource.getHeadRevision().then((headRevision:number)=> {
                options.minRevision = headRevision;
                this.getFromServer(options)
                    .then(
                    // On finalized
                    () => {
                        dfrd.resolve();
                    },
                    // On error
                    (err: any) => {
                        dfrd.reject(err);
                    },
                    // On notify
                    (partialResponse:IItemServiceSyncProgress<T>)=> {
                        this.syncProgress = partialResponse;

                        this.listeners.forEach((listener) => {
                            if(listener.onSyncProgress) {
                                listener.onSyncProgress(partialResponse);
                            }
                        });
                    })
                    .finally(()=>{
                        this.isSyncing = false;
                        if(this.needsSyncing) {
                            this.sync();
                        }
                    })
            });

            return dfrd.promise;
        }

        /**
         * Get contacts from user private storage using usersApi starting from minRevision
         * Since it fetches data in steps/parts, it notifies the caller using promise.notify with the progress
         *
         * @param options
         *
         * @returns {IPromise<T[]>}
         */
        public getFromServer(options?:IItemQueryOptions):IPromise<IItemServiceSyncProgress<T>> {
            var dfrd:ng.IDeferred<IItemServiceSyncProgress<T>> = this.$q.defer<IItemServiceSyncProgress<T>>();
            var dfrdPartial:ng.IDeferred<IItemServiceSyncProgress<T>> = this.$q.defer<IItemServiceSyncProgress<T>>();
            var items:T[] = [];

            options = options || {};
            options.minRevision = options.minRevision || 0;
            options.limit = options.limit || 100;
            options.offset = options.offset || 0;

            dfrdPartial.promise
                .then(()=> {
                    // All data was received, return all received items
                    var response: IItemServiceSyncProgress<T> = <IItemServiceSyncProgress<T>> {};
                    response.total = items.length;
                    response.count = items.length;
                    response.items = items;

                    dfrd.resolve(response);
                },
                (err:any)=> {
                    // ToDo: Handle error !
                    dfrd.reject(err);
                },
                (partialResponse:IItemServiceSyncProgress<T>)=> {
                    this.storeAndNotify(partialResponse.items)
                        .then((storedItems:T[])=>{

                        }).catch((err:any)=>{
                            dfrd.reject(err);
                        });

                    // Another partial was received, store new items and notify the caller with the progress
                    items = items.concat(partialResponse.items);

                    var syncProgress: IItemServiceSyncProgress<T> = <IItemServiceSyncProgress<T>>{};

                    syncProgress.total = partialResponse.total;
                    syncProgress.count = items.length;
                    syncProgress.items = items;

                    dfrd.notify(syncProgress)
                });

            this.getFromServerPartial(options, dfrdPartial);

            return dfrd.promise;
        }

        /**
         * Retrieves all contacts recursively until no data is received from the server
         *
         * @param options
         * @param dfrdPartial
         *
         * @returns {IPromise<T[]>}
         */
        private getFromServerPartial(options:IItemQueryOptions, dfrdPartial:ng.IDeferred<IItemServiceSyncProgress<T>>):IPromise<IItemServiceSyncProgress<T>> {
            this.api.get(options)
                .then((response:IItemQueryResponse<T>)=> {
                    var partialResponse: IItemServiceSyncProgress<T> = <IItemServiceSyncProgress<T>> {};
                    partialResponse.total = response.storage.totalItems;
                    partialResponse.count = response.items.length;
                    partialResponse.items = response.items;
                    dfrdPartial.notify(partialResponse);

                    if (response.items.length == options.limit) {
                        options.offset += options.limit;
                        this.getFromServerPartial(options, dfrdPartial);
                    }
                    else {
                        dfrdPartial.resolve();
                    }
                })
                .catch((err:any)=> {
                    dfrdPartial.reject(err);
                });
            return dfrdPartial.promise;
        }

        /**
         * Gets a map of contacts from a given datasource (nedb for example), in which the key
         * is the <field>
         *
         * @params field
         * @returns {IPromise<IItemMap>}
         */
        public getItemsGrouped(field:string):IPromise<IItemMap<T>> {
            return this.datasource.getAllGrouped(field);
        }

        /**
         * @param itemIds
         * @returns {ng.IPromise<T[]>}
         */
        public mergeNormal(itemIds: string[]): IPromise<T[]> {
            var dfrd:ng.IDeferred<T[]> = this.$q.defer<T[]>();

            this.api.mergeNormal(itemIds)
                .then((items: T[])=>{
                    this.storeAndNotify(items)
                        .then((storedItems:T[])=>{
                            dfrd.resolve(items);
                        }).catch((err:any)=>{
                            dfrd.reject(err);
                        });
                })
                .catch((err:any)=> {
                    dfrd.reject(err);
                });

            return dfrd.promise;
        }

        protected storeAndNotify(items: T[]): IPromise<T[]> {
            var dfrd:ng.IDeferred<T[]> = this.$q.defer<T[]>();

            if (items) {
                this.datasource.store(items)
                    .then((storedItems:T[])=> {
                        this.listeners.forEach((listener) => {
                            if (listener.onItemChange) {
                                listener.onItemChange(storedItems);
                            }
                        });

                        dfrd.resolve(storedItems);
                    })
                    .catch((err:any)=> {
                        dfrd.reject(err);
                    });
            } else {
                dfrd.resolve(null);
            }

            return dfrd.promise;
        }

        public getIsSyncing() {
            return this.isSyncing;
        }

        /**
         * Register listener to be notified when db is changed
         *
         * @params callback
         */
        public registerListener(callback:IItemServiceListener<T>) {
            this.listeners.addListener(callback);
        }

        /**
         * Deregister listener
         *
         * @params callback
         */
        public deregisterListener(callback:IItemServiceListener<T>) {
            this.listeners.removeListener(callback);
        }
    }
}