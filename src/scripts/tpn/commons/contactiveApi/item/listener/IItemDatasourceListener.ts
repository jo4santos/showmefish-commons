"use strict";

module tpn.commons.contactiveApi.item.listener {

    import IItem = tpn.commons.contactiveApi.item.model.IItem;

    export interface IItemDatasourceListener<T extends IItem> {
        onItemChange?(items: T[]): void;
    }

}