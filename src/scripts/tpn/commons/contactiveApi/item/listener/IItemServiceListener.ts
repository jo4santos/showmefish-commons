"use strict";

module tpn.commons.contactiveApi.item.listener {

    import IItem = tpn.commons.contactiveApi.item.model.IItem;
    import IItemServiceSyncProgress = tpn.commons.contactiveApi.item.model.IItemServiceSyncProgress;

    export interface IItemServiceListener<T extends IItem> {
        onItemChange?(items: T[]): void;
        onSyncProgress?(syncProgress: IItemServiceSyncProgress<T>): void;
    }

}