"use strict";

module tpn.commons.contactiveApi.item.api {

    import IItem = tpn.commons.contactiveApi.item.model.IItem;
    import IStorage = tpn.commons.contactiveApi.storage.model.IStorage;

    export interface IItemQueryResponse<T extends IItem> {
        includeDeleted: boolean;
        includeLocked: boolean;
        items: T[];
        limit: number;
        maxCreated: number;
        maxModified: number;
        maxRevision: number;
        maxUploaded: number;
        minCreated: number;
        minModified: number;
        minRevision: number;
        minUploaded: number;
        offset: number;
        onlyUploaded: boolean;
        sortField: string;
        sortOrder: string;
        sources: number[];
        storage: IStorage;
    }

}