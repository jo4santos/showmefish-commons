"use strict";

module tpn.commons.contactiveApi.item.api {

    export interface IItemQueryOptions {
        minRevision?: number;
        limit?: number;
        offset?: number;
    }

}