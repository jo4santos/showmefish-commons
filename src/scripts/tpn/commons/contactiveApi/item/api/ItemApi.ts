"use strict";

module tpn.commons.contactiveApi.item.api {

    import ApiClient            = tpn.commons.contactiveApi.api.ApiClient;
    import IItem                = tpn.commons.contactiveApi.item.model.IItem;
    import ItemType             = tpn.commons.contactiveApi.item.model.ItemType;
    import IItemQueryResponse   = tpn.commons.contactiveApi.item.api.IItemQueryResponse;
    import IItemQueryOptions    = tpn.commons.contactiveApi.item.api.IItemQueryOptions;

    import UrlEncodedUtils      = tpn.commons.shared.util.UrlEncodedUtils;

    /**
     * API for /users/me endpoint
     */

    export class ItemApi<T extends IItem> {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;
        protected itemType: ItemType;

        public constructor($q:ng.IQService,
                           apiClient:ApiClient,
                           itemType:ItemType) {
            this.$q = $q;
            this.apiClient = apiClient;
            this.itemType = itemType;
        }

        /**
         * Gets contacts for current user
         *
         * Endpoint: GET /users/me/contacts
         *
         * @params options
         * @returns {IPromise<IItemQueryResponse>}
         */
        public get(options?:IItemQueryOptions):ng.IPromise<IItemQueryResponse<T>> {

            if (options) {
                options.minRevision = options.minRevision || 0;
                options.limit = options.limit || -1;
                options.offset = options.offset || -1;
            }

            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .get(options)
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }

        /**
         * Tries to merge items on the backend
         *
         * Endpoint: POST /users/{userId}/{itemType}s/merge/normal
         *
         * @params itemIds
         * @returns {IPromise<T[]>}
         */
        public mergeNormal(itemIds: string[]):ng.IPromise<T[]> {

            var postParams = {
                item: itemIds
            };

            return this.apiClient
                .get()
                .all("users/me")
                .one(this.itemType.getEndpoint())
                .all("merge/normal")
                .post(UrlEncodedUtils.toQuery(postParams), {}, ApiClient.CONTENT_TYPE_FORM_URL_ENCODED)
                .then((data:any)=> {
                    if (data) return data.plain();
                });
        }
    }
}