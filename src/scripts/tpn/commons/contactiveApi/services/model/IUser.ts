module tpn.commons.contactiveApi.services.model {

    export interface IUser {
        username:string;
        firstName:string;
        lastName:string;
        userPhone:string;
        email: string;
        userId: string;
    }

}