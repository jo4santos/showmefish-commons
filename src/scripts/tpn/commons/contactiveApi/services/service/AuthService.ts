"use strict";

module tpn.commons.contactiveApi.services.service {

    import IConfigProvider                  = tpn.commons.contactiveApi.config.IConfigProvider;
    import ItemType                         = tpn.commons.contactiveApi.item.model.ItemType;
    import IAuthExchangeTpnTokenOptions     = tpn.commons.contactiveApi.services.api.IAuthExchangeTpnTokenOptions;
    import IAuthExchangeTpnTokenResponse    = tpn.commons.contactiveApi.services.api.IAuthExchangeTpnTokenResponse;
    import AuthApi                          = tpn.commons.contactiveApi.services.api.AuthApi;

    export class AuthService {

        protected authApi:AuthApi;
        protected configProvider:IConfigProvider;

        public static $inject = ["tcContactiveApiServicesAuthApi", "contactiveApiConfig"];

        public constructor(authApi:AuthApi,
                           configProvider:IConfigProvider) {
            this.authApi = authApi;
            this.configProvider = configProvider;
        }

        /**
         * Exchanges provider token by contactive accessToken
         *
         * @param accessToken
         * @param itemType
         *
         * @returns {IPromise<IAuthExchangeTpnTokenResponse>}
         */
        public exchangeTpnToken(accessToken:string, itemType: ItemType):ng.IPromise<IAuthExchangeTpnTokenResponse> {

            var options: IAuthExchangeTpnTokenOptions = <IAuthExchangeTpnTokenOptions>{};
            options.accessToken = accessToken;
            options.itemType = itemType.getType();
            options.userDevice = this.configProvider.getUserDevice();
            options.createIfMissing = "false";

            return this.authApi.exchangeTpnToken(options).then((response:IAuthExchangeTpnTokenResponse) => {
                return response;
            });
        }
    }
}