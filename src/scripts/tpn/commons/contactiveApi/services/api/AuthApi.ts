"use strict";

module tpn.commons.contactiveApi.services.api {

    import UrlEncodedUtils  = tpn.commons.shared.util.UrlEncodedUtils;
    import IUserDevice      = tpn.commons.contactiveApi.shared.model.IUserDevice;
    import ItemType         = tpn.commons.contactiveApi.item.model.ItemType;
    import ApiClient        = tpn.commons.contactiveApi.api.ApiClient;

    /**
     * API for /services/<service>/auth endpoint
     */

    export class AuthApi {

        protected apiClient:ApiClient;

        public static $inject = ["tcContactiveApiClient"];

        public constructor(apiClient:ApiClient) {
            this.apiClient = apiClient;
        }

        /**
         * Exchanges token given by provider (ex: warden) for a contactive accessToken
         *
         * @param options Token to exchange, info about device
         * @returns {IPromise<IAuthExchangeTpnTokenResponse>}
         */
        public exchangeTpnToken(options: IAuthExchangeTpnTokenOptions):ng.IPromise<IAuthExchangeTpnTokenResponse> {

            var options: IAuthExchangeTpnTokenOptions = options || <IAuthExchangeTpnTokenOptions>{};
            options.createIfMissing = options.createIfMissing || "false";
            options.userDevice = options.userDevice || <IUserDevice>{};

            /*
             We need this because all the parameters of the userDevice must be in the first level of the object
              */

            var params: any = {};
            params.accessToken = options.accessToken;
            params.createIfMissing = options.createIfMissing;
            params.itemType = options.itemType;
            angular.extend(params,options.userDevice);

            return this.apiClient
                .get()
                .one("services/tpn/auth/exchange")
                .post("token",UrlEncodedUtils.toQuery(params))
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }
    }
}