"use strict";

module tpn.commons.contactiveApi.services.api {

    import IUserDevice = tpn.commons.contactiveApi.shared.model.IUserDevice;

    export interface IAuthExchangeTpnTokenOptions {

        accessToken: string;
        createIfMissing?: string;
        itemType: string;
        userDevice?: IUserDevice;

    }

}