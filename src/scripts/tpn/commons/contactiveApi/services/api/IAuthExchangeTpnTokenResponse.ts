"use strict";

module tpn.commons.contactiveApi.services.api {

    import IUser = tpn.commons.contactiveApi.services.model.IUser;

    export interface IAuthExchangeTpnTokenResponse {
        accessToken: string;
        user: IUser;
    }
}