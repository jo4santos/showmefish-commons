"use strict";

module tpn.commons.contactiveApi.config {

    import IUserDevice = tpn.commons.contactiveApi.shared.model.IUserDevice;

    export interface IConfigProvider extends ng.IServiceProvider {
        getAccessToken():string;
        getAppToken():string;
        getApiEndpoint():string;
        getSourcesUri():string;
        getUserDevice():IUserDevice;
        getAuthEndpoint():string;
    }
}
