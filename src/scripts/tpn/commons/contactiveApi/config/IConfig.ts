"use strict";

module tpn.commons.contactiveApi.config {
    export interface IConfig {
        accessToken: string;
        apiEndpoint: string;
        appToken: string;
        sourcesUri: string;
        authEndpoint: string;
    }
}
