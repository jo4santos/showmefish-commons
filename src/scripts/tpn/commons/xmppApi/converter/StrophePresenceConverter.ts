module tpn.commons.xmppApi.converter {

    "use strict";

    import IPresence = tpn.commons.xmppApi.model.IPresence;
    import PresenceType = tpn.commons.xmppApi.model.PresenceType;

    /**
     * Converts presences from Strophe format to/from IMessage format
     */
    export class StrophePresenceConverter {

        fromStrophe(presenceStanza: Element): IPresence {

            var $presence = $(presenceStanza);

            var jid = $presence.attr('from');
            var bareJidFrom = Strophe.getBareJidFromJid(jid);
            var resourceJidFrom = Strophe.getResourceFromJid(jid);

            var presence = <IPresence> {
                from: bareJidFrom,
                resource: resourceJidFrom,
                type: $presence.attr('type'),
                show: $presence.children('show').text(),
                status: $presence.children("status").text(),
                priority: Number($presence.children("priority").text())
            };

            return presence;
        }

        toStrophe(presence: IPresence): Strophe.Builder {

            var $stanza: Strophe.Builder = null;

            // If the presence has a type set, no other attribute it sent
            if (presence.type) {
                $stanza = $pres({'type': presence.type});
            }
            // Otherwise the user is online, we will set the detailed presence attributes
            else {
                $stanza = $pres();

                if (presence.show != null) {
                    $stanza.c('show').t(presence.show).up();
                }

                if (presence.status != null) {
                    $stanza.c('status').t(presence.status).up();
                }

                if (presence.priority != null) {
                    $stanza.c('priority').t(presence.priority.toString()).up();
                }
            }

            return $stanza;
        }
    }
}
