module tpn.commons.xmppApi.converter {

    "use strict";

    import IVcard = tpn.commons.xmppApi.model.IVcard;

    /**
     * Converts vcards from Strophe format to/from IVcard format
     */
    export class StropheVcardConverter {

        fromStrophe(vcardStanza: Element): IVcard {

            var $stanza = $(vcardStanza);
            var $vcard = $stanza.find('vCard');

            var vcard = <IVcard> {
                fullname: $vcard.find('FN').text(),
                image: $vcard.find('BINVAL').text(),
                imageType: $vcard.find('TYPE').text(),
                url: $vcard.find('URL').text()
            };

            return vcard;
        }
    }
}
