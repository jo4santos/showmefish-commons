module tpn.commons.xmppApi.converter {

    "use strict";

    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import MessageType = tpn.commons.shared.messaging.model.MessageType;

    /**
     * Converts messages from Strophe format to/from IMessage format
     */
    export class StropheMessageConverter {

        toStrophe(message: IMessage): Strophe.Builder {

            var $stanza = $msg({
                id: message.id,
                from: message.from,
                to: message.to,
                type: message.type == MessageType.IS_WRITING ? MessageType.CHAT : message.type
            });

            if (message.body != null) {
                $stanza.c('body').t(message.body).up();
            }

            if (message.chatState != null) {
                $stanza.c(message.chatState, { xmlns: Strophe.NS.CHATSTATES }).up();
            }

            return $stanza;
        }

        fromStrophe(messageStanza: Element): IMessage {

            var $stanza = $(messageStanza);

            var message:IMessage = {
                id: $stanza.attr('id'),
                received: true,
                from: Strophe.getBareJidFromJid($stanza.attr('from')),
                to: Strophe.getBareJidFromJid($stanza.attr('to')),
                type: $stanza.attr('type'),
                body: $stanza.children('body').text(),
                xmppThreadId: $stanza.children('thread').text(),
                conversationId: undefined,
                conversationUcId: undefined
            };

            // Server sends an empty message when user is writing
            if (!message.body) {
                message.type = MessageType.IS_WRITING;
            }

            // Set conversation jid
            if (message.type == MessageType.GROUP_CHAT) {
                message.conversationId = message.to;
            } else {
                message.conversationId = message.from;
            }

            // Set time
            message.timeSent = new Date();

            // Get chat state
            var $chatState = $stanza.children("[xmlns='"+Strophe.NS.CHATSTATES+"']");
            if ($chatState.length) {
                message.chatState = $chatState[0].tagName;
            }

            return message;
        }
    }

}