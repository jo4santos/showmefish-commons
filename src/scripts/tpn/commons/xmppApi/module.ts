module tpn.commons.xmppApi {

    "use strict";

    /**
     * Register tcXmppApi module
     */
    function registerAngularModule() {
        angular
            .module("tcXmppApi", [])

            .service("tcStropheMessageConverter", converter.StropheMessageConverter)
            .service("tcStrophePresenceConverter", converter.StrophePresenceConverter)
            .service("tcStropheVcardConverter", converter.StropheVcardConverter)
            .service("tcXmppApi", api.XmppApi)
            .service("tcXmppService", service.XmppService)
    }

    angular.element(document).ready(registerAngularModule);
}

