"use strict";

module tpn.commons.xmppApi.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class XmppServiceException extends Exception {

        constructor(cause?: any) {
            super("", null, cause);
            this.name = "ExternalException";
        }
    }

}