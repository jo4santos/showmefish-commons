"use strict";

module tpn.commons.xmppApi.config {
    export interface IConfig {
        boshServerUrl?: string;
        username?: string;
        password?: string;
    }
}
