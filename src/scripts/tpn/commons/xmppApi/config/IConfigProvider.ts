"use strict";

module tpn.commons.xmppApi.config {

    export interface IConfigProvider extends ng.IServiceProvider {
        getXmppBoshServiceUrl():string;
        setXmppBoshServiceUrl(value:string);
    }
}
