"use strict";

module tpn.commons.xmppApi.service {

    // tpn.commons.shared.exception
    import AlreadyConnectedException = tpn.commons.shared.exception.AlreadyConnectedException;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;

    // tpn.commons.shared.listener
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;

    // tpn.commons.shared.messaging
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import MessageType = tpn.commons.shared.messaging.model.MessageType;
    import IMessageDatasource = tpn.commons.shared.messaging.datasource.IMessageDatasource;

    // tpn.commons.xmppApi.listener
    import IXmppConnectionListener = tpn.commons.xmppApi.listener.IXmppConnectionListener;
    import IXmppPresenceListener = tpn.commons.xmppApi.listener.IXmppPresenceListener;
    import IXmppMessageListener = tpn.commons.xmppApi.listener.IXmppMessageListener;

    // tpn.commons.xmppApi.api
    import XmppApi = tpn.commons.xmppApi.api.XmppApi;

    // tpn.commons.xmppApi.exception
    import XmppServiceException = tpn.commons.xmppApi.exception.XmppServiceException;

    // tpn.commons.xmppApi.model
    import IContact = tpn.commons.xmppApi.model.IContact;
    import IPresence = tpn.commons.xmppApi.model.IPresence;
    import IVcard = tpn.commons.xmppApi.model.IVcard;

    /**
     * Provides all XMPP features
     * Stores received message on message datasource
     */
    export class XmppService {

        protected $log: ng.ILogService;
        protected $q: ng.IQService;
        protected configProvider: config.IConfigProvider;
        protected messageDatasource: IMessageDatasource;

        protected xmppApi: XmppApi;

        static $inject = [
            "$log",
            "$q",
            "tcXmppApi",
            "xmppApiConfig",
            "messageDatasource"
        ];
        public constructor(
            $log: ng.ILogService,
            $q: ng.IQService,
            xmppApi: XmppApi,
            xmppApiConfigProvider: config.IConfigProvider,
            messageDatasource: IMessageDatasource
        ) {
            this.$log = $log;
            this.$q = $q;
            this.xmppApi = xmppApi;
            this.configProvider = xmppApiConfigProvider;
            this.messageDatasource = messageDatasource;

            this.init();
        }

        protected init() {
            this.onXmppMessageReceived = this.onXmppMessageReceived.bind(this);
            return this.xmppApi.addMessageListener(this.onXmppMessageReceived);
        }

        //
        // Connect / Disconnect API
        //

        /**
         * Register a listener to listen to connection status changes
         * @param listener
         * @returns XmppApi
         */
        public addConnectionListener(listener: IXmppConnectionListener) {
            return this.xmppApi.addConnectionListener(listener);
        }

        /**
         * Client should remove registered connection listener if it's no longer used
         * @param listener
         * @returns boolean False if not found / removed
         */
        public removeConnectionListener(listener: IXmppConnectionListener): boolean {
            return this.xmppApi.removeConnectionListener(listener);
        }

        /**
         * Connects to XMPP BOSH service
         * Triggers all connection listeners with updated connection status
         *
         * @param jid
         * @param password
         * @throws AlreadyConnectedException
         * @throws XmppServiceException
         */
        public connect(jid: string, password: string) {
            return this.xmppApi.connect(jid, password);
        }

        /**
         * Disconnect from XMPP BOSH service
         *
         * @throws NotConnectedException
         * @throws XmppServiceException
         */
        public disconnect(reason?: string) {
            return this.xmppApi.disconnect(reason);
        }

        /**
         * Returns true when client is correctly connected and available to send/receive messages
         * @returns {boolean}
         */
        public isConnected(): boolean {
            return this.xmppApi.isConnected();
        }

        /**
         * Returns user jid
         * @returns {string}
         */
        public getUserJid(): string {
            return this.xmppApi.getUserJid();
        }

        //
        // Contacts API
        //

        /**
         * Listener will receive contacts as soon as they are returned by the server
         * Triggers all contactsListeners with updated contacts
         *
         * @returns {IPromise<IContact[]>}
         */
        public loadContactsList(): ng.IPromise<IContact[]> {
            return this.xmppApi.loadContactsList();
        }

        /**
         * Returns vcard associated to user (jid)
         *
         * @param jid If empty, will get vcard for own user
         * @returns {IPromise<IVcard>}
         */
        public getUserVCard(jid?: string): ng.IPromise<IVcard> {
            return this.xmppApi.getUserVCard(jid);
        }


        /**
         * Updates own avatar
         * @param image
         * @param imageType
         * @returns {IPromise<IContact[]>}
         */
        public setUserAvatar(image: string, imageType: string = "image/png"): ng.IPromise<void> {
            return this.xmppApi.setUserAvatar(image, imageType);
        }

        //
        // Presences API
        //

        /**
         * Register a listener to listen to a specific contact presence change (defined by jid)
         * @param jid
         * @param listener
         */
        public addJidPresenceListener(jid: string, listener: IXmppPresenceListener) {
            return this.xmppApi.addJidPresenceListener(jid, listener);
        }

        /**
         * Client should remove registered presence listeners for specific contacts if no longer used
         * @param jid
         * @param listener
         * @returns boolean False if not found / removed
         */
        public removeJidPresenceListener(jid: string, listener: IXmppPresenceListener): boolean {
            return this.xmppApi.removeJidPresenceListener(jid, listener);
        }

        /**
         * Sets user presence
         * @param presence
         */
        public setUserPresence(presence: IPresence) {
            return this.xmppApi.setUserPresence(presence);
        }

        //
        // Messaging API
        //

        public addMessageListener(listener: IXmppMessageListener) {
            return this.xmppApi.addMessageListener(listener);
        }

        public removeMessageListener(listener: IXmppMessageListener): boolean {
            return this.xmppApi.removeMessageListener(listener);
        }

        /**
         * Sends message and sets chat state to ACTIVE
         * Message will be filled with defaults
         *
         * @param message
         * @see XmppApi.fillMessageWithDefaults
         */
        public sendMessage(message: IMessage) {
            this.xmppApi.sendMessage(message);
        }

        //
        // IXmppMessageListener
        //

        /**
         * When a content messaage is received, save it on datastore
         * @param message
         */
        protected onXmppMessageReceived(message: IMessage) {

            // Content message
            if (message.type != MessageType.IS_WRITING) {
                message.body = tpn.commons.shared.util.StringUtils.escapeHtml(message.body);
                this.storeMessageOnDatasource(message);
            }
        }

        /**
         * Saves new messages on datasource
         * New conversation will not be created if not exist. MessagingService should manage it
         * @param message
         */
        protected storeMessageOnDatasource(message: IMessage) {

            this.messageDatasource.updateMessages([message], true)
                .then(() => {
                    this.$log.info("[XmppService] Message for", message.conversationId ,"stored in datasource:", message.id);
                })
                .catch((e) => {
                    // XXX: Some kind of alert should be shown to the user in these cases
                    this.$log.error("[XmppService] Unexpected error while trying to save received message from XMPP into storage. Message will not be shown", message, e);
                });

        }
    }
}
