"use strict";

module tpn.commons.xmppApi.listener {

    export interface IXmppPresenceListener {
        (presence: model.IPresence): void;
    }

}