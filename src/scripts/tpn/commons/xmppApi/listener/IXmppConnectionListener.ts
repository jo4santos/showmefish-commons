"use strict";

module tpn.commons.xmppApi.listener {

    export interface IXmppConnectionListener {
        (statusCode: number, statusName: string, reason?: string): void;
    }

}