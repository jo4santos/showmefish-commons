"use strict";

module tpn.commons.xmppApi.listener {

    import IMessage = tpn.commons.shared.messaging.model.IMessage;

    export interface IXmppMessageListener {
        (message: IMessage): void;
    }

}