"use strict";

module tpn.commons.xmppApi.model {

    /**
     * @see http://xmpp.org/extensions/xep-0029.html
     */
    export class Jid {

        node: string;
        domain: string;
        resource: string;

        constructor(fullJid: string) {
            this.init(fullJid);
        }

        protected init(fullJid: string) {

            if (!fullJid) return;

            var parts, domainResource;

            parts = fullJid.split("@");
            if (parts.length == 1) {
                domainResource = parts[0];
            } else {
                this.node = parts[0];
                domainResource = parts[1];
            }

            parts = domainResource.split("/");
            this.domain = parts[0];
            if (parts.length > 1) {
                this.resource = parts[1];
            }
        }

        get full() {
            return this.node + "@" + this.domain + "/" + this.resource;
        }

        get bare() {
            return this.node + "@" + this.domain;
        }
    }

}