"use strict";

module tpn.commons.xmppApi.model {

    /**
     * @see http://xmpp.org/rfcs/rfc3921.html#presence
     */
    export interface IPresence {

        /**
         * Presence from this contact (jid)
         */
        from?: string;

        /**
         * Associated resource
         */
        resource?: string;

        /**
         * Presence Type (unavailable, ...)
         *
         * @see PresenceType
         */
        type?: string;

        /**
         * Presence Show (online, dnd, away, xa)
         *
         * @see PresenceShow
         */
        show?: string;

        /**
         * Presence message
         */
        status?: string;

        /**
         * Presence message lang
         */
        lang?: string

        /**
         * Sets presence priority
         * Defaults to 0
         */
        priority?: number;
    }

}