"use strict";

module tpn.commons.xmppApi.model {

    /**
     * XEP-0085 Chat states
     * @see http://xmpp.org/extensions/xep-0085.html
     */
    export class ChatState {
        static ACTIVE = "active";
        static INACTIVE = "inactive";
        static COMPOSING = 'composing';
        static PAUSED = 'paused';
        static GONE = 'gone';
    }

}