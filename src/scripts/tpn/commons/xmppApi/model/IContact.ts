"use strict";

module tpn.commons.xmppApi.model {

    export interface IContact {

        jid: string;
        name: string;
        groups: string[];
        resources: {[id: string]: IPresence};
        subscription: string;

    }

}