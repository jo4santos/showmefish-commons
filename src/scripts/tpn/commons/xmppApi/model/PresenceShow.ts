"use strict";

module tpn.commons.xmppApi.model {

    export class PresenceShow {
        static CHAT = "chat";
        static DND = "dnd";
        static AWAY = "away";
        static XA = "xa";
    }

}