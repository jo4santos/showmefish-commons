"use strict";

module tpn.commons.xmppApi.model {

    export class ConnectionStatus {
        static ATTACHED = 8;
        static AUTHENTICATING = 3;
        static AUTHFAIL = 4;
        static CONNECTED = 5;
        static CONNECTING = 1;
        static CONNFAIL = 2;
        static DISCONNECTED = 6;
        static DISCONNECTING = 7;
        static ERROR = 0;
        static REDIRECT = 9;

        static getNameFor(code: number): string {
            return _.findKey(this, function (v) {
                return v == code;
            }) || "UNKNOWN";
        }
    }

}