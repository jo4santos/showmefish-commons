"use strict";

module tpn.commons.xmppApi.model {

    export interface IVcard {
        fullname?: string;
        url?: string;
        imageType?: string;
        image?: string;
    }

}