"use strict";

module tpn.commons.xmppApi.model {

    export class PresenceType {
        static UNAVAILABLE = "unavailable";
        static PROBE = "probe";
        static ERROR = "error";
        static UNSUBSCRIBE = "unsubscribe";
        static UNSUBSCRIBED = "unsubscribed";
        static SUBSCRIBE = "subscribe";
        static SUBSCRIBED = "subscribed";
    }

}