"use strict";

module tpn.commons.xmppApi.api {

    // tpn.commons.shared.exception
    import AlreadyConnectedException = tpn.commons.shared.exception.AlreadyConnectedException;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;

    // tpn.commons.shared.listener
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;
    import IndexedListenerManager = tpn.commons.shared.listener.IndexedListenerManager;
    import IXmppConnectionListener = tpn.commons.xmppApi.listener.IXmppConnectionListener;
    import IXmppPresenceListener = tpn.commons.xmppApi.listener.IXmppPresenceListener;
    import IXmppMessageListener = tpn.commons.xmppApi.listener.IXmppMessageListener;

    // tpn.commons.shared.util
    import GUID = tpn.commons.shared.util.GUID;

    // tpn.commons.xmppApi.exception
    import XmppServiceException = tpn.commons.xmppApi.exception.XmppServiceException;

    // tpn.commons.xmppApi.model
    import IContact = tpn.commons.xmppApi.model.IContact;
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import IPresence = tpn.commons.xmppApi.model.IPresence;
    import IVcard = tpn.commons.xmppApi.model.IVcard;
    import PresenceType = tpn.commons.xmppApi.model.PresenceType;
    import MessageTypes = tpn.commons.shared.messaging.model.MessageType;
    import ChatStates = tpn.commons.xmppApi.model.ChatState;
    import ConnectionStatus = tpn.commons.xmppApi.model.ConnectionStatus;

    // tpn.commons.xmppApi.converter
    import StropheMessageConverter = tpn.commons.xmppApi.converter.StropheMessageConverter;
    import StrophePresenceConverter = tpn.commons.xmppApi.converter.StrophePresenceConverter;
    import StropheVcardConverter = tpn.commons.xmppApi.converter.StropheVcardConverter;

    /**
     * Strophe abstraction API
     */
    export class XmppApi {

        protected $log:ng.ILogService;
        protected $timeout:ng.ITimeoutService;
        protected $q:ng.IQService;
        protected configProvider:config.IConfigProvider;
        protected stropheMessageConverter:StropheMessageConverter;
        protected strophePresenceConverter:StrophePresenceConverter;
        protected stropheVcardConverter:StropheVcardConverter;

        protected connection:Strophe.Connection;

        protected connectionListeners:ListenerManager<IXmppConnectionListener>;
        protected messageListeners:ListenerManager<IXmppMessageListener>;
        protected jidPresenceListeners:IndexedListenerManager<IXmppPresenceListener>;

        // Used for connection retries
        protected jid: string;
        protected password: string;
        protected disconnectionRequested: boolean;
        protected connectionRetries: number = 0;
        protected lastUserPresence: IPresence;

        // Presences indexes
        protected lastPresenceIndex:{[jid: string]: IPresence} = {};
        protected resourcePresenceIndex:{[jid: string]: {[resource: string]: IPresence}} = {};

        static $inject = ["$log", "$timeout", "$q", "xmppApiConfig", "tcStropheMessageConverter", "tcStrophePresenceConverter", "tcStropheVcardConverter"];

        public constructor($log:ng.ILogService,
                           $timeout:ng.ITimeoutService,
                           $q:ng.IQService,
                           xmppApiConfigProvider:config.IConfigProvider,
                           stropheMessageConverter:StropheMessageConverter,
                           strophePresenceConverter:StrophePresenceConverter,
                           stropheVcardConverter:StropheVcardConverter) {
            this.$log = $log;
            this.$timeout = $timeout;
            this.$q = $q;
            this.configProvider = xmppApiConfigProvider;
            this.stropheMessageConverter = stropheMessageConverter;
            this.strophePresenceConverter = strophePresenceConverter;
            this.stropheVcardConverter = stropheVcardConverter;

            this.init();
        }

        protected init() {
            this.overrideStropheLog();
            this.bindCallbacksToThis();
            this.initConnection();
            this.initListenersManagers();
        }

        //
        // Connect / Disconnect API
        //

        /**
         * Register a listener to listen to connection status changes
         * @param listener
         */
        public addConnectionListener(listener:IXmppConnectionListener) {
            this.connectionListeners.addListener(listener);
        }

        /**
         * Client should remove registered connection listener if it's no longer used
         * @param listener
         * @returns boolean False if not found / removed
         */
        public removeConnectionListener(listener:IXmppConnectionListener):boolean {
            return this.connectionListeners.removeListener(listener);
        }

        /**
         * Connects to XMPP BOSH service
         * Triggers all connection listeners with updated connection status
         *
         * @param jid
         * @param password
         * @throws AlreadyConnectedException
         * @throws XmppServiceException
         */
        public connect(jid:string, password:string) {

            if (this.connection.connected) {
                throw new AlreadyConnectedException();
            }

            this.jid = jid;
            this.password = password;
            this.disconnectionRequested = false;

            try {
                this.connection.service = this.configProvider.getXmppBoshServiceUrl();
                this.connection.connect(jid, password, this.connectionCallback.bind(this));
            } catch (error) {
                throw new XmppServiceException(error);
            }
        }

        protected reconnect() {
            var retryTime = this.connectionRetries * 500;
            if (retryTime > 10000) {
                retryTime = 10000;
            }

            this.initConnection();
            this.$timeout(() => {
                this.connect(this.jid, this.password);
            }, retryTime);
        }

        /**
         * Disconnect from XMPP BOSH service
         *
         * @throws XmppServiceException
         * @returns XmppApi
         */
        public disconnect(reason?:string) {

            if (!this.connection.connected) {
                return;
            }

            this.jid = undefined;
            this.password = undefined;
            this.disconnectionRequested = true;

            try {
                this.connection.disconnect(reason);
            } catch (error) {
                throw new XmppServiceException(error);
            }
        }

        /**
         * Returns true when client is correctly connected and available to send/receive messages
         * @returns {boolean}
         */
        public isConnected():boolean {
            return this.connection.connected;
        }

        /**
         * Returns user jid
         * @returns {string}
         */
        public getUserJid():string {
            return Strophe.getBareJidFromJid(this.connection.jid);
        }

        //
        // Contacts API
        //

        /**
         * Listener will receive contacts as soon as they are returned by the server
         * Triggers all contactsListeners with updated contacts
         *
         * @returns {IPromise<IContact[]>}
         */
        public loadContactsList():ng.IPromise<IContact[]> {
            throw new Error("Not implemented, removed roster plugin");
        }

        /**
         * Returns vcard associated to user (jid)
         *
         * @param jid If empty, will get vcard for own user
         * @returns {IPromise<IVcard>}
         */
        public getUserVCard(jid?:string):ng.IPromise<IVcard> {

            var deferred = this.$q.defer<IVcard>();

            this.connection.vcard.get(
                (stanza) => {
                    var vcard = this.stropheVcardConverter.fromStrophe(stanza);
                    deferred.resolve(vcard);
                },
                jid,
                (error) => {
                    deferred.reject(new XmppServiceException(error))
                }
            );

            return deferred.promise;
        }


        /**
         * Updates own avatar
         * @param image
         * @param imageType
         * @returns {IPromise<IContact[]>}
         */
        public setUserAvatar(image:string, imageType:string = "image/png"):ng.IPromise<void> {

            var deferred = this.$q.defer<void>();

            var iq = $iq({
                type: "set",
                from: this.connection.jid,
                id: GUID.generate(),
                to: Strophe.getBareJidFromJid(this.connection.jid)
            })
                .c("vCard", {xmlns: "vcard-temp"})
                .c("PHOTO")
                .c("TYPE", imageType).up()
                .c("BINVAL", image);

            this.connection.sendIQ(iq,
                (result) => {
                    deferred.resolve();
                },
                (error) => {
                    deferred.reject(new XmppServiceException(error));
                }
            );

            return deferred.promise;
        }

        //
        // Presences API
        //

        /**
         * Register a listener to listen to a specific contact presence change (defined by jid)
         * @param jid
         * @param listener
         * @returns XmppApi
         */
        public addJidPresenceListener(jid:string, listener:IXmppPresenceListener) {
            this.jidPresenceListeners.addListener(jid, listener);
        }

        /**
         * Client should remove registered presence listeners for specific contacts if no longer used
         * @param jid
         * @param listener
         * @returns boolean False if not found / removed
         */
        public removeJidPresenceListener(jid:string, listener:IXmppPresenceListener):boolean {
            return this.jidPresenceListeners.removeListener(jid, listener);
        }

        /**
         * Sets user presence
         * @param presence
         */
        public setUserPresence(presence:IPresence) {

            var $spresence = this.strophePresenceConverter.toStrophe(presence);
            this.connection.send($spresence);
            this.lastUserPresence = presence;
        }

        //
        // Messaging API
        //

        public addMessageListener(listener:IXmppMessageListener) {
            this.messageListeners.addListener(listener);
        }

        public removeMessageListener(listener:IXmppMessageListener):boolean {
            return this.messageListeners.removeListener(listener);
        }

        /**
         * Sends message and sets chat state to ACTIVE
         * Message will be filled with defaults
         *
         * FIXME: Should throw an exception if there's no available connection
         * FIXME: isConnected should not be used by services
         *
         * @param message
         * @see XmppApi.fillMessageWithDefaults
         */
        public sendMessage(message:IMessage) {

            this.fillMessageWithDefaults(message);
            var smsg = this.stropheMessageConverter.toStrophe(message);

            this.connection.send(smsg);
        }

        //
        // Protected
        //

        //
        // Connection stuff
        //

        protected initConnection() {
            this.connection = new Strophe.Connection("");
        }

        //
        // Callbacks registration
        //

        protected initListenersManagers() {
            this.connectionListeners = new ListenerManager<IXmppConnectionListener>(this.$log);

            this.jidPresenceListeners = new IndexedListenerManager<IXmppPresenceListener>(
                this.$log,
                {
                    onAddedListener: this.notifyListenerInitialPresence
                }
            );

            this.messageListeners = new ListenerManager<IXmppMessageListener>(this.$log);
        }

        protected bindCallbacksToThis() {
            this.connectionCallback = this.connectionCallback.bind(this);
            this.presenceCallback = this.presenceCallback.bind(this);
            this.messageCallback = this.messageCallback.bind(this);
            this.registerPresenceCallback = this.registerPresenceCallback.bind(this);
            this.deregisterPresenceCallback = this.deregisterPresenceCallback.bind(this);
            this.registerMessageCallback = this.registerMessageCallback.bind(this);
            this.deregisterMessageCallback = this.deregisterMessageCallback.bind(this);
            this.notifyListenerInitialPresence = this.notifyListenerInitialPresence.bind(this);
        }

        protected registerPresenceCallback() {
            this.connection.addHandler(this.presenceCallback, null, 'presence', null);
        }

        protected deregisterPresenceCallback() {
            this.connection.deleteHandler(this.presenceCallback);
        }

        protected registerMessageCallback() {
            this.connection.addHandler(this.messageCallback, null, 'message', 'chat');
        }

        protected deregisterMessageCallback() {
            if (this.connection) {
                this.connection.deleteHandler(this.messageCallback);
            }
        }

        protected notifyListenerInitialPresence(jid:string, listener:IXmppPresenceListener) {
            var lastPresence = this.lastPresenceIndex[jid];
            if (lastPresence) {
                var args = [lastPresence];
                listener.apply(null, args);
            }
        }

        //
        // Callbacks
        //

        protected connectionCallback(statusCode:number, reason:string) {

            var statusName = ConnectionStatus.getNameFor(statusCode);
            this.$log.debug("[XmppApi] Connection Status", arguments, statusName);
            this.notifyConnectionListeners(statusCode, statusName, reason);

            switch (statusCode) {
                case ConnectionStatus.CONNECTED:
                    this.registerMessageCallback();
                    this.registerPresenceCallback();
                    if (this.lastUserPresence) {
                        this.setUserPresence(this.lastUserPresence);
                    }
                    this.connectionRetries = 0;
                    break;
                case ConnectionStatus.DISCONNECTED:
                    this.deregisterMessageCallback();
                    this.deregisterPresenceCallback();
                    if (!this.disconnectionRequested) {
                        this.$log.warn("[XmppApi] Connection was lost, trying to reconnect ...");
                        this.connectionRetries ++;
                        this.reconnect();
                    }
                    break;
            }

            return true;
        }

        protected presenceCallback(presenceStanza:Element) {

            var presence = this.strophePresenceConverter.fromStrophe(presenceStanza);
            this.updatePresenceIndexes(presence);

            return true;
        }

        protected messageCallback(messageStanza:Element) {

            var message = this.stropheMessageConverter.fromStrophe(messageStanza);
            this.$log.debug("[XmppApi] Message received", arguments, message);
            this.notifyMessageListeners(message);

            return true;
        }

        //
        // Notify listeners
        //

        protected notifyPresenceListeners(presence: IPresence) {

            var args;
            this.jidPresenceListeners.forEach(presence.from, (listener:IXmppPresenceListener) => {
                try {
                    args = [presence];
                    listener.apply(null, args);
                } catch (err) {
                    this.$log.error("[XmppApi] Error while running presence listener", listener, "with arguments", args, ". Ignoring and going to next listener: ", err);
                }
            }, this);
        }

        protected notifyMessageListeners(message: IMessage) {

            var args;
            this.messageListeners.forEach((listener:IXmppMessageListener) => {
                try {
                    args = [message];
                    listener.apply(null, args);
                } catch (err) {
                    this.$log.error("[XmppApi] Error while running message listener", listener, "with arguments", args, ". Ignoring and going to next listener: ", err);
                }
            }, this);
        }

        protected notifyConnectionListeners(statusCode, statusName, reason) {

            var args;
            this.connectionListeners.forEach((listener:IXmppConnectionListener) => {
                try {
                    args = [statusCode, statusName, reason];
                    listener.apply(null, args);
                } catch (err) {
                    this.$log.error("[XmppApi] Error while running connection listener", listener, "with arguments", args, ". Ignoring and going to next listener: ", err);
                }
            }, this);
        }

        //
        // Misc
        //

        /**
         * Return last selected presence for associated jid
         * @param jid
         * @returns {IPresence}
         */
        protected getLastPresence(jid: string): IPresence {
            return this.lastPresenceIndex[jid];
        }

        /**
         * Update presence indexes
         * @param newPresence
         */
        protected updatePresenceIndexes(newPresence: IPresence) {

            var jid: string = newPresence.from;
            var resource: string = newPresence.resource;
            var oldLastPresence: IPresence = this.lastPresenceIndex[jid];
            var newLastPresence: IPresence;

            // Init resource hash if null
            if (!this.resourcePresenceIndex[jid]) {
                this.resourcePresenceIndex[jid] = {};
            }

            // Update resource priority
            this.resourcePresenceIndex[jid][resource] = newPresence;
            //this.$log.debug("[XmppApi] User resource presence updated ", newPresence);

            //
            // Update last presence
            //
            var resourcePresences = this.resourcePresenceIndex[jid];

            // Update lastPresenceIndex with the most priority presence
            var result: IPresence|Number = _.max(resourcePresences, (presence: IPresence) => presence.priority);
            if (result && result != Number.NEGATIVE_INFINITY) {
                this.lastPresenceIndex[jid] = newLastPresence = <IPresence> result;
            } else {
                this.lastPresenceIndex[jid] = newLastPresence = undefined;
            }

            //
            // Notify listeners if lastPresence changed
            //

            if (oldLastPresence != newLastPresence) {
                this.notifyPresenceListeners(newLastPresence);
                //this.$log.debug("[XmppApi] User last presence updated", newLastPresence);
            }
        }

        protected overrideStropheLog() {
            var prefix = "[Strophe]";
            Strophe.log = (level, msg) => {
                switch (level) {
                    case Strophe.LogLevel.FATAL:
                        this.$log.error(prefix, msg);
                        break;
                    case Strophe.LogLevel.ERROR:
                        this.$log.error(prefix, msg);
                        break;
                    case Strophe.LogLevel.WARN:
                        this.$log.warn(prefix, msg);
                        break;
                    /*
                    case Strophe.LogLevel.INFO:
                        this.$log.debug(prefix, msg);
                        break;
                    case Strophe.LogLevel.DEBUG:
                        this.$log.debug(prefix, msg);
                        break;
                    default:
                        this.$log.debug(prefix, msg);
                        */
                }
            };
        }

        /**
         * Sets default values to message
         * @param message
         * @see IMessage
         */
        protected fillMessageWithDefaults(message:IMessage) {
            if (message.id == undefined) {
                message.id = GUID.generate();
            }

            if (message.from == undefined) {
                message.from = Strophe.getBareJidFromJid(this.connection.jid);
            }

            if (message.type == undefined) {
                message.type = MessageTypes.NORMAL;
            }

            if (message.conversationId == undefined) {
                message.conversationId = message.to;
            }

            if (message.timeSent == undefined) {
                message.timeSent = new Date();
            }

            if (message.chatState == undefined) {
                message.chatState = ChatStates.ACTIVE;
            }
        }
    }
}

