module tpn.commons.unifiedContact {

    "use strict";

    /**
     * Register tcUnifiedContact module
     */
    function registerAngularModule() {

        var angularApp = angular.module("tcUnifiedContact", [
            "tcXmppApi",
            "tcContactiveApi"
        ]);

        // Should be provided by client module for tpn.commons.unifiedContact.UnifiedContactService:
        // "unifiedContactDatasource"

        // Unified Contact services
        angularApp.service('tcUnifiedContactService', UnifiedContactService);
    }

    angular.element(document).ready(registerAngularModule);
}

