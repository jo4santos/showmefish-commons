"use strict";

module tpn.commons.unifiedContact.listener {

    import IUnifiedContact = tpn.commons.unifiedContact.model.IUnifiedContact;

    export interface IUnifiedContactListener {
        onUpdatedContact?(unifiedContact: IUnifiedContact): void;
    }

}