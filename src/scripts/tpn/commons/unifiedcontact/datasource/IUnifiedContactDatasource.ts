"use strict";

module tpn.commons.unifiedContact.datasource {

    import IDatasourceUnifiedContactListener    = tpn.commons.unifiedContact.listener.IDatasourceUnifiedContactListener;
    import IUnifiedContact                      = tpn.commons.unifiedContact.model.IUnifiedContact;
    import IUnifiedContactQueryOptions          = tpn.commons.unifiedContact.model.IUnifiedContactQueryOptions;

    /**
     * Saves unified contacts in NeDB storage, indexed by itemId
     */
    export interface IUnifiedContactDatasource /* extends IDatasource<IDatasourceUnifiedContactListener> */ {
        getUnifiedContacts(query?:IUnifiedContactQueryOptions): ng.IPromise<IUnifiedContact[]>;
        saveUnifiedContact(unifiedContact:IUnifiedContact):ng.IPromise<IUnifiedContact>;
        clear(): ng.IPromise<void>;

        registerListener(callback:IDatasourceUnifiedContactListener);
        deregisterListener(callback:IDatasourceUnifiedContactListener);
    }
}
