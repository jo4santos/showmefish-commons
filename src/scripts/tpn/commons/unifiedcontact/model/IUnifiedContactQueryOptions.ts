"use strict";

module tpn.commons.unifiedContact.model {

    export interface IUnifiedContactQueryOptions {

        groupId?: string;
        jid?: string|string[];

        searchText?: string;
        name?: boolean;
        phone?: boolean;

        limit?: number;
        offset?: number;

        sortBy?: IQuerySort;
    }

    export interface IQuerySort {
        property: string;
        desc?: boolean;
    }

}