"use strict";

module tpn.commons.unifiedContact.model {

    // tpn.commons.shared
    import IMessage = tpn.commons.shared.messaging.model.IMessage;

    // tpn.commons.contactiveApi
    import IContactItem         = tpn.commons.contactiveApi.contact.model.IContactItem;
    import IMergedContactGroup  = tpn.commons.contactiveApi.contact.model.IMergedContactGroup;

    // tpn.commons.xmppApi
    import IPresence = tpn.commons.xmppApi.model.IPresence;

    export interface IUnifiedContact extends IMergedContactGroup {
        presence: IPresence;
        presenceType: UnifiedContactPresenceType;
        hasChatCapabilities: boolean;
    }

}
