"use strict";

module tpn.commons.unifiedContact.model {

    export class UnifiedContactPresenceType {
        static ONLINE = "online";
        static CHAT = "chat";
        static DND = "dnd";
        static AWAY = "away";
        static XA = "xa";
        static OFFLINE = "offline";
    }

}
