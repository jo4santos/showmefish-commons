"use strict";

module tpn.commons.unifiedContact {

    // tpn.commons.shared
    import Exception                = tpn.commons.shared.exception.Exception;
    import ListenerManager          = tpn.commons.shared.listener.ListenerManager;

    // tpn.commons.xmppApi
    import XmppService              = tpn.commons.xmppApi.service.XmppService;
    import IXmppPresenceListener    = tpn.commons.xmppApi.listener.IXmppPresenceListener;
    import IPresence                = tpn.commons.xmppApi.model.IPresence;
    import PresenceType             = tpn.commons.xmppApi.model.PresenceType;
    import PresenceShow             = tpn.commons.xmppApi.model.PresenceShow;

    // tpn.commons.contactiveApi
    import IContactiveConfigProvider = tpn.commons.contactiveApi.config.IConfigProvider;
    import IContactItem              = tpn.commons.contactiveApi.contact.model.IContactItem;
    import IContactPhone             = tpn.commons.contactiveApi.contact.model.IContactPhone;
    import IItemMap                  = tpn.commons.contactiveApi.item.model.IItemMap;
    import IItemServiceListener   = tpn.commons.contactiveApi.item.listener.IItemServiceListener;
    import ContactItemService        = tpn.commons.contactiveApi.contact.service.ContactItemService;

    // tpn.commons.unifiedContact
    import IUnifiedContact                      = tpn.commons.unifiedContact.model.IUnifiedContact;
    import UnifiedContactPresenceType           = tpn.commons.unifiedContact.model.UnifiedContactPresenceType;
    import IUnifiedContactQueryOptions          = tpn.commons.unifiedContact.model.IUnifiedContactQueryOptions;
    import IDatasourceUnifiedContactListener    = tpn.commons.unifiedContact.listener.IDatasourceUnifiedContactListener;
    import IUnifiedContactListener              = tpn.commons.unifiedContact.listener.IUnifiedContactListener;

    // tpn.commons.contactiveApi.contact.merger
    import IMergedContactGroup  = tpn.commons.contactiveApi.contact.model.IMergedContactGroup;
    import ContactGroupMerger   = tpn.commons.contactiveApi.contact.merger.ContactGroupMerger;

    /**
     * Get contact items from datasource
     * Get sources.json from backend
     * Merge contact items into unified contact based on sources
     * Store unified contact in datasource
     * Set listeners for xmpp presence updates
     */
    export class UnifiedContactService implements IItemServiceListener<IContactItem> {

        protected $http:ng.IHttpService;
        protected $log:ng.ILogService;
        protected $q:ng.IQService;

        protected xmppService:XmppService;
        protected contactItemService:ContactItemService;
        protected contactiveConfigProvider:IContactiveConfigProvider;
        protected unifiedContactDatasource:datasource.IUnifiedContactDatasource;

        // Listeners

        protected unifiedContactListeners:ListenerManager<IUnifiedContactListener>;

        protected sourcesObj: any;
        protected merging: boolean = false;
        protected contactsToMerge: string[] = [];

        static $inject = [
            "$http",
            "$log",
            "$q",
            "tcXmppService",
            "tcContactiveApiContactItemService",
            "contactiveApiConfig",
            "unifiedContactDatasource"
        ];

        constructor($http:ng.IHttpService,
                    $log:ng.ILogService,
                    $q:ng.IQService,
                    xmppService:XmppService,
                    contactiveApiContactItemService:ContactItemService,
                    contactiveConfigProvider:IContactiveConfigProvider,
                    unifiedContactDatasource:datasource.IUnifiedContactDatasource) {
            this.$http = $http;
            this.$log = $log;
            this.$q = $q;
            this.xmppService = xmppService;
            this.contactItemService = contactiveApiContactItemService;
            this.contactiveConfigProvider = contactiveConfigProvider;
            this.unifiedContactDatasource = unifiedContactDatasource;

            this.init();
        }

        protected init() {
            this.unifiedContactListeners = new ListenerManager<IUnifiedContactListener>(this.$log);

            // Todo: What if app is offline when started and regains connection? Merging won't work anymore ...
            this.$http.get(this.contactiveConfigProvider.getSourcesUri())
                .success((data)=> {
                    this.sourcesObj = data;
                    this.$log.debug("UnifiedContactService got sources file for merging",this.sourcesObj);
                })
                .error((error)=>{
                    this.$log.error("Error downloading sources file for mergind", error);
                });

            this.initCallbacks();
            this.registerListeners();
        }

        //
        // Init
        //

        protected initCallbacks() {
            this.$log.info("UnifiedContactService initCallbacks called");
            this.onItemChange = this.onItemChange.bind(this);
        }

        protected registerListeners() {
            this.$log.info("UnifiedContactService registerListeners called");
            this.contactItemService.registerListener(this);
        }

        protected deregisterListeners() {
            this.$log.info("UnifiedContactService deregisterListeners called");
            this.contactItemService.deregisterListener(this);
        }

        //
        // Methods
        //

        public isSyncing():boolean {
            return this.contactItemService.getIsSyncing();
        }

        public getUnifiedContacts(query?:IUnifiedContactQueryOptions):ng.IPromise<IUnifiedContact[]> {
            return this.unifiedContactDatasource.getUnifiedContacts(query);
        }

        public getUnifiedContact(groupId:string):ng.IPromise<IUnifiedContact> {
            var query:IUnifiedContactQueryOptions = <IUnifiedContactQueryOptions>{};
            query.groupId = groupId;
            return this.unifiedContactDatasource.getUnifiedContacts(query).then((unifiedContacts:IUnifiedContact[])=> {
                return unifiedContacts[0];
            });
        }


        /**
         * Processes the contactsToMerge array
         * Gets all items for a groupId
         * Unifies them into a single UnifiedContact
         * Stores unified contact in datasource
         *
         * @returns {IPromise<TResult>}
         */
        protected mergeContacts() {
            if(this.merging || this.contactsToMerge.length == 0) {
                return;
            }
            var groupId: string = this.contactsToMerge.pop();
            //this.$log.info("Merging",groupId,this.contactsToMerge.length,"left")
            this.merging = true;
            this.contactItemService.getByGroupId(groupId)
                .then((items:IContactItem[]) => {
                    if (items.length == 0 || !items[0].groupId) {
                        return null;
                    }

                    var merger: ContactGroupMerger = new ContactGroupMerger(this.sourcesObj.sources);
                    var unifiedContact: IUnifiedContact = <IUnifiedContact>merger.merge({groupId: groupId, contacts: items});

                    if(unifiedContact.chat && unifiedContact.chat.length > 0) {
                        unifiedContact.hasChatCapabilities = true;
                    }
                    else {
                        unifiedContact.hasChatCapabilities = false;
                    }

                    unifiedContact.phone.forEach((phone:IContactPhone)=> {

                        // Create parameter with country code and normalized phone number to provide search
                        phone.e164Format = "";
                        if (phone.countryCode && phone.normalized.indexOf(phone.countryCode.toString()) !== 0) {
                            phone.e164Format += "+" + phone.countryCode;
                        }
                        if (phone.normalized) {
                            phone.e164Format += "" + phone.normalized;
                        }
                    })

                    if(unifiedContact.name) {
                        var names: string [] = [];
                        if(unifiedContact.name.firstName) {
                            names.push(unifiedContact.name.firstName);
                        }
                        if(unifiedContact.name.middleName) {
                            names.push(unifiedContact.name.middleName);
                        }
                        if(unifiedContact.name.lastName) {
                            names.push(unifiedContact.name.lastName);
                        }
                        unifiedContact.name.normalized = names.join(" ");
                        unifiedContact.name.normalized = tpn.commons.shared.util.StringUtils.normalize(angular.copy(unifiedContact.name.normalized));
                    }

                    return this.unifiedContactDatasource.saveUnifiedContact(unifiedContact)
                        .then((savedUnifiedContact:IUnifiedContact)=> {
                            this.unifiedContactListeners.forEach((listener)=> {
                                if (listener.onUpdatedContact) {
                                    listener.onUpdatedContact(savedUnifiedContact);
                                }
                            })
                        });
                })
                .finally(() => {
                    // Merge new contact here
                    this.merging = false;
                    this.mergeContacts();
                })
        }

        public getPresenceType(presence: IPresence): UnifiedContactPresenceType {
            var unifiedContactPresenceType: UnifiedContactPresenceType = null;

            if(presence.show == PresenceShow.AWAY) {
                unifiedContactPresenceType = UnifiedContactPresenceType.AWAY;
            }
            else if(presence.show == PresenceShow.DND) {
                unifiedContactPresenceType = UnifiedContactPresenceType.DND;
            }
            else if(presence.show == PresenceShow.CHAT) {
                unifiedContactPresenceType = UnifiedContactPresenceType.CHAT;
            }
            else if(presence.show == PresenceShow.XA) {
                unifiedContactPresenceType = UnifiedContactPresenceType.XA;
            }
            else if(presence.type == PresenceType.UNAVAILABLE) {
                unifiedContactPresenceType = UnifiedContactPresenceType.OFFLINE;
            }
            else if(!presence.show && !presence.type) {
                unifiedContactPresenceType = UnifiedContactPresenceType.ONLINE;
            }

            return unifiedContactPresenceType;
        }

        isMerging(): boolean {
            return this.merging;
        }

        getNumberContactsToMerge(): number {
            return this.contactsToMerge.length;
        }

        //
        // Callbacks
        //

        /**
         * Called when items change in contactive datasource
         * @param items
         */
        public onItemChange(items:IContactItem[]) {
            items.forEach((item:IContactItem)=> {
                if (item.groupId) {
                    // Add groupId to queue so mergeContact gets to it when it finishes previous merge
                    if(this.contactsToMerge.indexOf(item.groupId) == -1) {
                        this.contactsToMerge.push(item.groupId);
                        this.mergeContacts();
                    }
                }
            });
        }

        //
        // Listeners registration
        //

        public registerUnifiedContactListener(listener:IUnifiedContactListener) {
            this.unifiedContactListeners.addListener(listener);
        }

        public deregisterUnifiedContactListener(listener:IUnifiedContactListener) {
            this.unifiedContactListeners.removeListener(listener);
        }

        public registerJidPresenceHandler(jid:string, listener:IXmppPresenceListener) {
            this.xmppService.addJidPresenceListener(jid, listener);
        }

        public deregisterJidPresenceHandler(jid:string, listener:IXmppPresenceListener) {
            this.xmppService.removeJidPresenceListener(jid, listener);
        }

    }
}