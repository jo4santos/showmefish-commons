module tpn.commons.ucApi.messageSync.converter {

    "use strict";

    // tpn.commons.shared
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import MessageType = tpn.commons.shared.messaging.model.MessageType;

    // tpn.commons.ucApi
    import IConfigProvider = tpn.commons.ucApi.shared.config.IConfigProvider;
    import IMessagesContainer = tpn.commons.ucApi.messageSync.model.response.IMessagesContainer;
    import IUcMessage = tpn.commons.ucApi.messageSync.model.response.IUcMessage;
    import IUcConversationUpdate = tpn.commons.ucApi.messageSync.model.response.IUcConversationUpdate;

    /**
     * Converts messages from UC API format to/from IMessage format
     */
    export class UcMessageConverter {

        protected $log: ng.ILogService;
        protected configProvider: IConfigProvider;

        static $inject = ["$log", "ucApiConfig"];
        constructor(
            $log: ng.ILogService,
            configProvider: IConfigProvider
        ) {
            this.$log = $log;
            this.configProvider = configProvider;
        }

        fromUc(container: IMessagesContainer): IMessage[] {

            if (!container) {
                return null;
            }

            var msgs = <IMessage[]> [];

            container.messages.forEach((uc) => {
                var msg = this.fromUcMessage(uc);
                msg.conversationUcId = container.convId;
                msgs.push(msg);
            });

            return msgs;
        }

        fromUcMessage(item: IUcMessage|IUcConversationUpdate): IMessage {

            if (!item) {
                return null;
            }

            if (!item.to || item.to.length > 1) {
                this.$log.warn("This message has more than one destination", item);
            }

            var from = Strophe.getBareJidFromJid(item.from);

            var to;
            if (item.to.length == 1) {
                to = Strophe.getBareJidFromJid(item.to[0]);
            } else if (item.to.length > 1) {
                to = Strophe.getBareJidFromJid(item.to[0]);
                this.$log.warn("More than 1 destinations on received message. Assuming first", item);
            } else {
                this.$log.warn("No destinations found on received message", item);
            }

            var received = from != this.configProvider.getUserJid();
            var conversationId = Strophe.getBareJidFromJid(received ? from : to);

            var msg = <IMessage> {
                id: item.xmppId,
                ucId: item.msgId,
                isRead: item.isRead,
                received: received,
                from: from,
                to: to,
                conversationId: conversationId,
                conversationUcId: item.convId,
                xmppThreadId: item.xmppThreadId,
                subject: undefined,
                type: MessageType.CHAT,
                timeSent: new Date(item.timeSent),
                chatState: undefined,
                body: item.message,
                lang: undefined
            };

            return msg;
        }
    }

}