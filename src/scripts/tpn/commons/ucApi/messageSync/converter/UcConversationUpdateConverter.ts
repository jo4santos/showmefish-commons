module tpn.commons.ucApi.messageSync.converter {

    "use strict";

    // tpn.commons.shared
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import IConversationUpdate = tpn.commons.shared.messaging.model.IConversationUpdate;

    // tpn.commons.ucApi
    import IUcConversationUpdate = tpn.commons.ucApi.messageSync.model.response.IUcConversationUpdate;

    /**
     * Converts conversation updates from UC API to/from IConversationUpdate format
     */
    export class UcConversationUpdateConverter {

        protected $log: ng.ILogService;
        protected ucMessageConverter: UcMessageConverter;

        static $inject = [
            "$log",
            "tcUcMessageConverter"
        ];
        constructor(
            $log: ng.ILogService,
            ucMessageConverter: UcMessageConverter
        ) {
            this.$log = $log;
            this.ucMessageConverter = ucMessageConverter;
        }

        fromUcArray(list: IUcConversationUpdate[]): IConversationUpdate[] {

            if (!list) {
                return null;
            }

            var statuses = <IConversationUpdate[]> [];

            list.forEach((uc) => {
                statuses.push(this.fromUc(uc));
            });

            return statuses;
        }

        fromUc(item: IUcConversationUpdate): IConversationUpdate {

            if (!item) {
                return null;
            }

            var lastMessage: IMessage = this.ucMessageConverter.fromUcMessage(item);
            lastMessage.id = item.lastMsgId;

            var status = <IConversationUpdate> {
                conversationId: lastMessage.conversationId,
                conversationUcId: item.convId,
                unreadCount: item.unreadCount,
                deleted: item.deleted,
                lastMessage: lastMessage
            };

            return status;
        }
    }

}