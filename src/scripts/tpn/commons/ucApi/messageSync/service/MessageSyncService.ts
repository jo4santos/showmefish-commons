"use strict";

module tpn.commons.ucApi.messageSync.service {

    // tpn.commons.shared.*
    import IllegalStateException = tpn.commons.shared.exception.IllegalStateException;
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;
    import Exception = tpn.commons.shared.exception.Exception;
    import AlreadyConnectedException = tpn.commons.shared.exception.AlreadyConnectedException;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;
    import ErrorConnectingException = tpn.commons.shared.exception.ErrorConnectingException;

    // tpn.commons.shared.messaging
    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IConversationUpdate = tpn.commons.shared.messaging.model.IConversationUpdate;
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import IConversationDatasource = tpn.commons.shared.messaging.datasource.IConversationDatasource;
    import IMessageDatasource = tpn.commons.shared.messaging.datasource.IMessageDatasource;
    import SortOrder = tpn.commons.shared.messaging.model.SortOrder;

    // tpn.commons.ucApi.shared
    import UcServiceException = tpn.commons.ucApi.shared.exception.UcServiceException;
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;
    import IResponse = tpn.commons.ucApi.shared.model.IResponse;
    import WebsocketStatus = tpn.commons.ucApi.shared.api.WebsocketStatus;
    import WsApiClient = tpn.commons.ucApi.shared.api.WsApiClient;
    import IConfigProvider = tpn.commons.ucApi.shared.config.IConfigProvider;

    // tpn.commons.ucApi.messageSync.model
    import IMessageSyncRequest = tpn.commons.ucApi.messageSync.model.request.IMessageSyncRequest;
    import IConversationsStatusResponse = tpn.commons.ucApi.messageSync.model.response.IConversationsUpdateResponse;
    import IMessagesResponse = tpn.commons.ucApi.messageSync.model.response.IMessagesResponse;
    import IUcMessage = tpn.commons.ucApi.messageSync.model.response.IUcMessage;

    // tpn.commons.ucApi.messageSync.listener
    import IMessageSyncListener = tpn.commons.ucApi.messageSync.listener.IMessageSyncListener;

    // tpn.commons.ucApi.messageSync.converter
    import UcMessageConverter = tpn.commons.ucApi.messageSync.converter.UcMessageConverter;
    import UcConversationStatusConverter = tpn.commons.ucApi.messageSync.converter.UcConversationUpdateConverter;

    // tpn.commons.ucApi.messageSync.api
    import MessageSyncApi = tpn.commons.ucApi.messageSync.api.MessageSyncApi;

    /**
     * MessageSync features
     */
    export class MessageSyncService {

        protected $log: ng.ILogService;
        protected $interval: ng.IIntervalService;
        protected $q: ng.IQService;
        protected configProvider: IConfigProvider;
        protected messageSyncApi: MessageSyncApi;
        protected messageDatasource: IMessageDatasource;
        protected conversationDatasource: IConversationDatasource;

        protected listeners: ListenerManager<IMessageSyncListener>;

        protected schedule: ng.IPromise<void>;

        protected lastUpdates: {[conversationUcId: string]: IConversationUpdate};

        static $inject = [
            "$log",
            "$interval",
            "$q",
            "ucApiConfig",
            "tcUcMessageSyncApi",
            "messageDatasource"
        ];
        public constructor(
            $log: ng.ILogService,
            $interval: ng.IIntervalService,
            $q: ng.IQService,
            configProvider: IConfigProvider,
            messageSyncApi: MessageSyncApi,
            messageDatasource: IMessageDatasource
        ) {
            this.$log = $log;
            this.$interval = $interval;
            this.$q = $q;
            this.configProvider = configProvider;
            this.messageSyncApi = messageSyncApi;
            this.messageDatasource = messageDatasource;

            this.init();
        }

        protected init() {
            this.lastUpdates = {};
            this.listeners = new ListenerManager<IMessageSyncListener>(this.$log);
            this.bindCallbacksToThis();
        }

        protected bindCallbacksToThis() {
            this.checkForUpdates = this.checkForUpdates.bind(this);
        }

        //
        // Sync service
        //

        /**
         * Gets updates from MessageSync API each configured UpdatePeriodMs
         * For each conversation update, triggers listener.conversationWasUpdated
         */
        startPeriodicSync() {
            if (this.isPeriodicSyncRunning()) {
                throw new IllegalStateException("Sync service already started");
            }

            this.schedule = this.$interval(this.checkForUpdates, this.configProvider.getUpdatePeriodMs());
        }

        /**
         * Stops getting updates from MessageSync API
         */
        stopPeriodicSync() {
            if (!this.isPeriodicSyncRunning()) {
                throw new IllegalStateException("Sync service already stopped");
            }

            this.$interval.cancel(this.schedule);
            this.schedule = null;
        }

        /**
         * Is periodic sync running?
         * @returns {boolean}
         */
        isPeriodicSyncRunning(): boolean {
            return this.schedule != null
        }

        //
        // MessageSyncApi
        //

        /**
         * Get new conversation updates
         * @returns {ng.IPromise<IConversationUpdate[]>}
         */
        protected getConveresationsUpdates(since?: Date, priorTo?: Date, pageSize?: number): ng.IPromise<IConversationUpdate[]> {

            var userJid = this.configProvider.getUsername();
            return this.messageSyncApi.getConversationsUpdates(userJid);
        }

        /**
         * Get all messages associated to identified conversation
         *
         * @param conversationIds
         * @param since After this date
         * @param priorTo Before this date
         * @param pageSize
         * @returns {ng.IPromise<IMessage[]>}
         */
        getMessages(conversationIds: { id: string, ucId: string }, since?: Date, priorTo?: Date, pageSize?: number): ng.IPromise<IMessage[]> {

            if(!conversationIds.id || !conversationIds.ucId) {
                throw new Exception("Conversation id and ucId must be provided");
            }

            var userJid = this.configProvider.getUsername();
            return this.messageSyncApi.getMessages(userJid, conversationIds.ucId, since, priorTo, pageSize)
                .then((messages: IMessage[]) => {
                    this.onMessagesReceived(conversationIds.id, messages, true);
                    return messages;
                })
        }

        /**
         * Mark all messages as read (before "since" date)
         *
         * @param conversationUcId
         * @param since
         * @returns {ng.IPromise<void>}
         */
        markMessagesAsRead(conversationUcId: string, since?: Date): ng.IPromise<void> {

            var userJid = this.configProvider.getUsername();
            return this.messageSyncApi.markMessagesAsRead(userJid, conversationUcId, since);
        }

        /**
         * Mark all messages as deleted (before "since" date)
         *
         * @param conversationUcId
         * @param since
         * @returns {ng.IPromise<void>}
         */
        markMessageAsDeleted(conversationUcId: string, since?: Date): ng.IPromise<void> {

            var userJid = this.configProvider.getUsername();
            return this.messageSyncApi.markMessageAsDeleted(userJid, conversationUcId, since);
        }

        /**
         * Checks for updates
         */
        checkForUpdates(): ng.IPromise<IConversationUpdate[]> {

            return this.getConveresationsUpdates()
                .then((newUpdates: IConversationUpdate[]) => {

                    var promises: ng.IPromise<void>[] = [];

                    newUpdates.forEach((newUpdate) => {

                        var oldUpdate: IConversationUpdate = this.lastUpdates[newUpdate.conversationUcId];

                        // Check if there are changes
                        if (oldUpdate) {

                            // If is different
                            if (oldUpdate.lastMessage.timeSent.getTime() != newUpdate.lastMessage.timeSent.getTime())
                            {
                                promises.push(this.onConversationUpdate(newUpdate, this.configProvider.getLastUpdateTime()));
                            }

                            // else ... there's nothing to update
                        }

                        // New!
                        else {
                            promises.push(this.onConversationUpdate(newUpdate, this.configProvider.getLastUpdateTime()));
                        }

                    });

                    this.lastUpdates = _.indexBy(newUpdates, 'conversationUcId');
                    this.configProvider.setLastUpdateTime(this.getUpdatesMaxTimeSent(newUpdates));

                    return this.$q.all(promises);
                });
        }

        //
        // Updates
        //

        /**
         * When a ConversationUpdate was received, check if messages were deleted or if
         * are new messages available on server
         *
         * @param update
         * @param from
         */
        protected onConversationUpdate(update: IConversationUpdate, from?: Date): ng.IPromise<void> {

            this.$log.info("[MessageSyncService] Received ConversationUpdate for", update.conversationId);

            // Conversations messages were deleted
            if (update.deleted) {
                this.onMessagesDeleted(update.conversationId, update.lastMessage.timeSent);
            }

            // New messages arrived
            else {

                this.getMostRecentDatasourceMessage(update.conversationId)
                .then((mostRecentDatasourceMessage: IMessage) => {

                        var from: Date;
                        var maxPages = Number.POSITIVE_INFINITY;

                        // 1st. If some message on datastore, the from date is the min of those:
                        //      - TimeSent of last message on Datastore
                        //      - Last update (if any message on datastore)
                        if (mostRecentDatasourceMessage) {
                            from = _.min([mostRecentDatasourceMessage.timeSent, this.configProvider.getLastUpdateTime()]);
                        }

                        // 2nd. If no message on datastore:
                        //      - Save only one page of messages (10 messages)
                        //      - from = minHistoryDate
                        else {
                            from = this.getMinHistoryDate();
                            maxPages = 1;
                        }

                        return this.getAllMessagesBetween(update, from, undefined, maxPages)
                            .then(() => {
                                this.$log.info("[MessageSyncService] Received all conversation", update.conversationId, "messages from", from);
                                return null;
                            });
                    })


            }

            return this.$q.when();
        }

        /**
         * Get all messages since between [from, to[
         * Keeps getting messages chunks (defined MESSAGES_PAGESIZE) recursively, until there are no more retrieved messages
         * For each chunk, triggers an update event (triggerUpdate)
         *
         * @param update
         * @param from
         * @param to
         * @param maxPages
         * @returns {IPromise<TResult>}
         */
        protected getAllMessagesBetween(update: IConversationUpdate, from: Date, to?: Date, maxPages: number = Number.POSITIVE_INFINITY): ng.IPromise<IMessage[]> {

            var conversationIds = {
                id: update.conversationId,
                ucId: update.conversationUcId,
            };

            return this.getMessages(conversationIds, from, to, this.configProvider.getMessagesPageSize())
                .then((messages: IMessage[]) => {
                    this.$log.debug("[MessageSyncService] Received conversation", update.conversationId, "chunk messages between", [from, to]);

                    var promise: ng.IPromise<IMessage[]>;
                    var remainingMaxPages = maxPages - 1;

                    // If not complete, continue requesting ...
                    if (messages.length == this.configProvider.getMessagesPageSize() && remainingMaxPages > 0) {
                        var minTimeSent = this.getMessagesMinTimeSent(messages);
                        promise = <ng.IPromise<IMessage[]>> this.getAllMessagesBetween(update, from, minTimeSent, remainingMaxPages);
                        this.onMessagesReceived(update.conversationId, messages);
                    }

                    else {
                        this.onMessagesReceived(update.conversationId, messages, true);
                    }

                    return promise;
                });
        }

        //
        // Storage
        //

        /**
         * When MessageSync messages received, updates DB messages
         *
         * @param conversationId
         * @param messages
         * @param lastChunk
         */
        protected onMessagesReceived(conversationId: string, messages: IMessage[], lastChunk?: boolean) {

            if (!messages || !messages.length) {
                return;
            }

            // If message is in message sync server, we assume it will be delivered to the end-user.
            // @see http://jira.tpn.thinkingphones.net/browse/DAPP-98
            messages.forEach((message: IMessage)=>{
                message.isDelivered = true;
            });

            var mostRecentMessage = this.getMostRecentMessage(messages);
            var mostRecentReadMessage = this.getMostRecentReadMessage(messages);

            return this.getMostRecentDatasourceReadMessage(conversationId)
                .then((mostRecentDatasourceReadMessage:IMessage) => {

                    // Update local DB with each new message
                    return this.messageDatasource.updateMessages(messages, true)

                        // Mark messages as read if we've more recent read messages
                        .then(() => {
                            if (!mostRecentDatasourceReadMessage || mostRecentDatasourceReadMessage.timeSent < mostRecentReadMessage.timeSent) {
                                return this.messageDatasource.markMessagesAdRead(conversationId, mostRecentReadMessage.timeSent);
                            }
                        })

                        // Trigger messages received
                        .then(() => {
                            this.triggerMessagesReceived(conversationId, messages, lastChunk);
                        })

                        .catch((error: Exception) => {
                            this.$log.error("[MessagingSyncService] Unexpected error updating mesasges on datastore. Not storing those messages", error, messages);
                            throw error;
                        });
                })

        }

        /**
         * When MessageSync messages deleted, deletes DB messages before deleteBefore
         *
         * @param conversationId
         * @param messages
         * @param lastChunk
         */
        protected onMessagesDeleted(conversationId: string, deleteBefore: Date) {

            this.messageDatasource.removeMessages({
                conversationId: conversationId,
                to: deleteBefore
            })
                .then(() => {
                    this.triggerMessagesDeleted(conversationId, deleteBefore);
                })
                .catch((error) => {
                    this.$log.error("[MessageSyncService] Unexpected error while removing messages from MessageSync into storage. Message will not be deleted", conversationId, deleteBefore, error);
                    throw error;
                });

        }

        //
        // Triggers
        //

        /**
         * Triggers listener.onSyncNewMessages
         *
         *
         * @param conversationId
         * @param messages
         * @param lastChunk
         */
        protected triggerMessagesReceived(conversationId: string, messages: IMessage[], lastChunk: boolean = false) {

            this.$log.info("[MessageSyncService] Trigger messages received for", conversationId);

            this.listeners.forEach((listener: IMessageSyncListener) => {
                if (listener.onSyncNewMessages) {
                    listener.onSyncNewMessages(conversationId, messages, lastChunk);
                }
            })
        }

        /**
         * Triggers listener.triggerMessagesDeleted
         * @param conversationId
         * @param deleteBefore
         */
        protected triggerMessagesDeleted(conversationId: string, deleteBefore: Date) {

            this.$log.info("[MessageSyncService] Trigger messages deleted for", conversationId);

            this.listeners.forEach((listener: IMessageSyncListener) => {
                if (listener.onSyncDeletedMessages) {
                    listener.onSyncDeletedMessages(conversationId, deleteBefore);
                }
            })

        }

        //
        // Listeners
        //

        /**
         * Register a listener to listen to connection status changes
         * @param listener
         */
        public registerListener(listener: IMessageSyncListener) {
            this.listeners.addListener(listener);
        }

        /**
         * Client should remove registered connection listener if it's no longer used
         * @param listener
         * @returns boolean False if not found / removed
         */
        public unregisterListener(listener: IMessageSyncListener): boolean {
            return this.listeners.removeListener(listener);
        }

        //
        // Misc
        //

        /**
         * Returns the minimum date to preserve historic messages
         * When a conversation is updated, the returned messages are allways after this date
         *
         * @returns {Date}
         */
        protected getMinHistoryDate(): Date {
            var date = new Date();
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            date.setMilliseconds(0);
            date.setDate(date.getDate() - (this.configProvider.getHistoryDays() || 90));
            return date;
        }

        /**
         * Returns the max timeSent date in listed updates
         * Used to find the lastUpdateTime
         *
         * @param updates
         * @returns {Date}
         */
        protected getUpdatesMaxTimeSent(updates: IConversationUpdate[]) {
            var update = _.max(updates, (update) => {
                return update.lastMessage.timeSent;
            });
            return update.lastMessage.timeSent;
        }

        /**
         * Returns the min timeSent date in listed messages
         * Used to check if we received all messages or if we need to keep asking messages to the server
         *
         * @param messages
         * @returns {Date}
         */
        protected getMessagesMinTimeSent(messages: IMessage[]) {
            var message = _.min(messages, (message) => {
                return message.timeSent;
            });
            return message.timeSent;
        }

        /**
         * Get the most recent message from list
         *
         * @param messages
         * @returns {Date}
         */
        protected getMostRecentMessage(messages: IMessage[]): IMessage {
            if (!messages) return null;

            var message: IMessage = _.max(messages, (message: IMessage) => {
                return message.timeSent;
            });

            return message;
        }

        /**
         * Get the most recent read message from list
         *
         * @param messages
         * @returns {Date}
         */
        protected getMostRecentReadMessage(messages: IMessage[]): IMessage {
            if (!messages) return null;

            var message: IMessage = _.max(messages, (message: IMessage) => {
                return message.isRead ? message.timeSent : undefined;
            });

            return message;
        }

        /**
         * Get the most recent read message from datasource
         * @param conversationId
         * @returns {ng.IPromise<IMessage[]>}
         */
        protected getMostRecentDatasourceReadMessage(conversationId: string): ng.IPromise<IMessage> {
            return this.messageDatasource.getMessage({
                conversationId: conversationId,
                isRead: true,
                sort: {
                    timeSent: SortOrder.DESC
                },
                limit: 1
            });
        }
        /**
         * FIXME: Not working!!!
         * Get the most recent read message from datasource
         * @param conversationId
         * @returns {ng.IPromise<IMessage[]>}
         */
        protected getMostRecentDatasourceMessage(conversationId: string): ng.IPromise<IMessage> {
            return this.messageDatasource.getMessage({
                conversationId: conversationId,
                sort: {
                    timeSent: SortOrder.DESC
                },
                limit: 1
            });
        }

    }


}
