"use strict";

module tpn.commons.ucApi.messageSync.model.request {

    import IRequest = tpn.commons.ucApi.shared.model.IRequest;

    export interface IMessageSyncRequest extends IRequest {
        conversation: IMessageSyncRequestDetail;
    }
}
