"use strict";

module tpn.commons.ucApi.messageSync.model.request {

    export interface IMessageSyncRequestDetail {
        userId: string;
        convId?: string;
        priorTo?: number;
        pageSize?: number;
        since?: number;
    }
}
