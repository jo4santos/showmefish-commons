"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    import IResponse = tpn.commons.ucApi.shared.model.IResponse;

    export interface IMessagesResponse extends IResponse {
        conversation: IResponseResultContainer<IMessagesContainer>;
    }
}
