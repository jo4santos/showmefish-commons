"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    export interface IUcMessage {

        /**
         * Message identifier
         * Example: "e6804250-4688-11e5-a04c-a3c6e0cdbb9a"
         */
        msgId: string;

        /**
         * Message XMPP identifier
         * Example: "143999893565013461866"
         */
        xmppId: string;

        /**
         * Message was read?
         */
        isRead: boolean;

        /**
         * Complete jid from contact where was sent
         * Example: "fsantos@im.thinkingphones.com/tpn-comm"
         */
        from: string;

        /**
         * Unix timestamp
         * Example: 1439998950126
         */
        timeSent: number;

        /**
         * Conversation id
         */
        convId: string;

        /**
         * XMPP thread identifier
         * Example: "MQwHx6"
         */
        xmppThreadId: string;

        /**
         * Sent to contacts
         * Example: ["fcosta@im.thinkingphones.com/dapp"]
         */
        to: string[];

        /**
         * "IM"
         */
        resource: string;

        /**
         * Body message
         */
        message: string;
    }
}
