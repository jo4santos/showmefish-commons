"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    export interface IConversationsUpdateContainer {
        unread_count: number;
        last_messages: IUcConversationUpdate[];
    }
}
