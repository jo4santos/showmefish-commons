"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    export interface IMessagesContainer {
        convId: string;
        messages: IUcMessage[];
    }
}
