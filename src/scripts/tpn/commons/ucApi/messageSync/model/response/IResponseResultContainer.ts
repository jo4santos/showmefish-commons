"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    import IRecordList = tpn.commons.ucApi.shared.model.IRecordList;

    export interface IResponseResultContainer<T> {
        convId: string;
        userId: string;
        result: IRecordList<T>;
    }
}

