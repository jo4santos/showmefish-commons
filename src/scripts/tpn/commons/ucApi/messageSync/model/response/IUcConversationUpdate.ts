"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    import IUcMessage = tpn.commons.ucApi.messageSync.model.response.IUcMessage;

    export interface IUcConversationUpdate extends IUcMessage {
        unreadCount: number;
        lastMsgId: string;
        deleted: boolean;
    }
}
