"use strict";

module tpn.commons.ucApi.messageSync.model.response {

    export interface IConversationOpResult {
        convId: string;
        result: string; /* OK */
    }

}
