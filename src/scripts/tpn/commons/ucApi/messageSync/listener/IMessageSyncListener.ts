"use strict";

module tpn.commons.ucApi.messageSync.listener {

    // tpn.commons.shared
    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IConversationUpdate = tpn.commons.shared.messaging.model.IConversationUpdate;
    import IMessage = tpn.commons.shared.messaging.model.IMessage;

    // tpn.commons.contactiveApi
    import IContactItem = tpn.commons.contactiveApi.contact.model.IContactItem;

    export interface IMessageSyncListener {
        onSyncNewMessages?(conversationId: string, messages: IMessage[], lastChunk?: boolean): void;
        onSyncDeletedMessages?(conversationId: string, deleteBefore: Date): void;
    }

}