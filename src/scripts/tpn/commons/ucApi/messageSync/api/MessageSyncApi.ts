"use strict";

module tpn.commons.ucApi.messageSync.api {

    // tpn.commons.shared
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;
    import AlreadyConnectedException = tpn.commons.shared.exception.AlreadyConnectedException;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;
    import ErrorConnectingException = tpn.commons.shared.exception.ErrorConnectingException;
    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IConversationUpdate = tpn.commons.shared.messaging.model.IConversationUpdate;
    import IMessage = tpn.commons.shared.messaging.model.IMessage;

    // tpn.commons.ucApi.shared
    import UcServiceException = tpn.commons.ucApi.shared.exception.UcServiceException;
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;
    import IResponse = tpn.commons.ucApi.shared.model.IResponse;
    import ApiResponses = tpn.commons.ucApi.shared.model.ApiResponses;
    import WebsocketStatus = tpn.commons.ucApi.shared.api.WebsocketStatus;
    import WsApiClient = tpn.commons.ucApi.shared.api.WsApiClient;

    // tpn.commons.ucApi.messageSync
    import IMessageSyncRequest = tpn.commons.ucApi.messageSync.model.request.IMessageSyncRequest;

    import IConversationsUpdateResponse = tpn.commons.ucApi.messageSync.model.response.IConversationsUpdateResponse;
    import IMessagesResponse = tpn.commons.ucApi.messageSync.model.response.IMessagesResponse;
    import IUcMessage = tpn.commons.ucApi.messageSync.model.response.IUcMessage;
    import IUcConversationUpdate = tpn.commons.ucApi.messageSync.model.response.IUcConversationUpdate;
    import UcMessageConverter = tpn.commons.ucApi.messageSync.converter.UcMessageConverter;
    import UcConversationUpdateConverter = tpn.commons.ucApi.messageSync.converter.UcConversationUpdateConverter;

    /**
     * MessageSync API
     *
     * @see http://wiki.tpn.thinkingphones.net/display/UCAPI/WebBroker+Websocket+APIs
     */
    export class MessageSyncApi {

        protected $log: ng.ILogService;
        protected $q: ng.IQService;
        protected wsApiClient: WsApiClient;

        protected ucMessageConverter: UcMessageConverter;
        protected ucConversationUpdateConverter: UcConversationUpdateConverter;

        static $inject = [
            "$log",
            "$q",
            "tcUcWsApiClient",
            "tcUcMessageConverter",
            "tcUcConversationUpdateConverter",
        ];
        public constructor(
            $log: ng.ILogService,
            $q: ng.IQService,
            wsApiClient: WsApiClient,
            ucMessageConverter: UcMessageConverter,
            ucConversationUpdateConverter: UcConversationUpdateConverter
        ) {
            this.$log = $log;
            this.$q = $q;
            this.wsApiClient = wsApiClient;
            this.ucMessageConverter = ucMessageConverter;
            this.ucConversationUpdateConverter = ucConversationUpdateConverter;

            this.init();
        }

        protected init() {
        }

        //
        //
        //

        /**
         * Get new conversation updates
         *
         * @param userJid Return conversations for which user?
         * @returns {ng.IPromise<IConversationUpdate[]>}
         */
        public getConversationsUpdates(userJid: string, since?: Date, priorTo?: Date): ng.IPromise<IConversationUpdate[]> {

            var request: IMessageSyncRequest = {
                operation: UcApiOperation.RETRIEVE_CONVERSATIONS_STATUS,
                conversation: {
                    userId: userJid,
                    since: since ? since.getTime() : undefined,
                    priorTo: priorTo ? priorTo.getTime() : undefined
                }
            };

            return this.wsApiClient.sendRequest(request)
                .then((response: IConversationsUpdateResponse) => {
                    if (response.conversation.result.count) {
                        var lastMessages: IUcConversationUpdate[] = response.conversation.result.records[0].last_messages;
                        return this.ucConversationUpdateConverter.fromUcArray(lastMessages);
                    }
                });
        }

        /**
         * Get all messages associated to identified conversation
         *
         * @param userJid Return conversations for which user?
         * @param conversationUcId
         * @param since After this date
         * @param priorTo Before this date
         * @param pageSize
         * @returns {ng.IPromise<IMessage[]>}
         */
        public getMessages(userJid: string, conversationUcId: string, since?: Date, priorTo?: Date, pageSize?: number): ng.IPromise<IMessage[]> {

            var request: IMessageSyncRequest = {
                operation: UcApiOperation.RETRIEVE_CONVERSATIONS_STATUS,
                conversation: {
                    userId: userJid,
                    convId: conversationUcId,
                    since: since ? since.getTime() : undefined,
                    priorTo: priorTo ? priorTo.getTime() : undefined,
                    pageSize: pageSize
                }
            };

            return this.wsApiClient.sendRequest(request)
                .then((response: IMessagesResponse) => {
                    if (response.conversation.result.count) {
                        var messagesContainer = response.conversation.result.records[0];
                        return this.ucMessageConverter.fromUc(messagesContainer);
                    } else {
                        return [];
                    }
                });
        }


        /**
         * Mark all messages as read (before "since" date)
         *
         * @param userJid Return conversations for which user?
         * @param conversationUcId
         * @param since
         * @returns {ng.IPromise<void>}
         */
        public markMessagesAsRead(userJid: string, conversationUcId: string, since?: Date): ng.IPromise<void> {

            var request: IMessageSyncRequest = {
                operation: UcApiOperation.MARK_MESSAGE_READ,
                conversation: {
                    userId: userJid,
                    convId: conversationUcId,
                    since: since ? since.getTime() : undefined
                }
            };

            return this.wsApiClient.sendRequest(request)
                .then((response: IResponse) => {
                    return null;
                });
        }

        /**
         * Mark all messages as deleted (before "since" date)
         *
         * @param userJid Return conversations for which user?
         * @param conversationUcId
         * @param since
         * @returns {ng.IPromise<void>}
         */
        public markMessageAsDeleted(userJid: string, conversationUcId: string, since?: Date): ng.IPromise<void> {

            var request: IMessageSyncRequest = {
                operation: UcApiOperation.MARK_MESSAGE_DELETED,
                conversation: {
                    userId: userJid,
                    convId: conversationUcId,
                    since: since ? since.getTime() : undefined
                }
            };

            return this.wsApiClient.sendRequest(request)
                .then((response: IResponse) => {
                    return null;
                });
        }

    }


}
