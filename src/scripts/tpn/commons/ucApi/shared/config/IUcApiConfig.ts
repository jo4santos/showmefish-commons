"use strict";

module tpn.dapp.conversation.config {

    /**
     * Application device configuration
     */
    export interface IUcApiConfig {
        restApiEndpoint: string;
        wsApiEndpoint: string;
    }

}
