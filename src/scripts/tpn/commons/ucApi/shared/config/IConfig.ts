"use strict";

module tpn.commons.ucApi.shared.config {
    export interface IConfig {
        updatePeriodMs: number;
        messagesPageSize: number;
        historyDays: number;
    }
}
