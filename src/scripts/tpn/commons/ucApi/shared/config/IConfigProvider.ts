"use strict";

module tpn.commons.ucApi.shared.config {

    export interface IConfigProvider extends ng.IServiceProvider {
        getWsApiEndpoint(): string;
        getRestApiEndpoint(): string;
        getAccessToken(): string;
        getUserJid(): string;
        getUsername(): string;
        getUpdatePeriodMs(): number; // 4000
        getMessagesPageSize(): number; // 10
        getHistoryDays(): number; // 1
        getLastUpdateTime(): Date;
        setLastUpdateTime(date: Date);
    }
}
