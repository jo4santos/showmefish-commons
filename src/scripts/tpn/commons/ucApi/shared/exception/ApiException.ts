"use strict";

module tpn.commons.ucApi.shared.api {

    // tpn.commons.shared
    import Exception = tpn.commons.shared.exception.Exception;

    // tpn.commons.ucApi
    import ApiResponses = tpn.commons.ucApi.shared.model.ApiResponses;

    export class ApiException extends Exception {

        apiMessage:any;

        constructor(status?:any, apiMessage?:string) {
            super(ApiResponses.getMessageFor(status) || apiMessage, status);
            this.name = "UcApiException";
            this.apiMessage = apiMessage;
        }
    }
}
