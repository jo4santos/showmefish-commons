"use strict";

module tpn.commons.ucApi.shared.api {

    // tpn.commons.shared
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;
    import Exception = tpn.commons.shared.exception.Exception;
    import AlreadyConnectedException = tpn.commons.shared.exception.AlreadyConnectedException;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;
    import ErrorConnectingException = tpn.commons.shared.exception.ErrorConnectingException;
    import ErrorDisconnectingException = tpn.commons.shared.exception.ErrorDisconnectingException;
    import BusyException = tpn.commons.shared.exception.BusyException;
    import UnexpectedInputException = tpn.commons.shared.exception.UnexpectedInputException;

    // tpn.commons.ucApi.shared
    import UcServiceException = tpn.commons.ucApi.shared.exception.UcServiceException;
    import ApiResponses = tpn.commons.ucApi.shared.model.ApiResponses;
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;
    import IResponse = tpn.commons.ucApi.shared.model.IResponse;
    import IErrorResponse = tpn.commons.ucApi.shared.model.IErrorResponse;
    import WebsocketStatus = tpn.commons.ucApi.shared.api.WebsocketStatus;
    import IConfigProvider = tpn.commons.ucApi.shared.config.IConfigProvider;
    import DisconnectCode = tpn.commons.ucApi.shared.model.DisconnectCode;

    /**
     * Websockets API client that returns promises for each request
     */
    export class WsApiClient {

        protected $log: ng.ILogService;
        protected $q: ng.IQService;
        protected $timeout: ng.ITimeoutService;

        protected configProvider: IConfigProvider;
        protected ws: WebSocket;

        protected connectRequest: ng.IDeferred<void>;
        protected disconnectRequest: ng.IDeferred<void>;
        protected pendingRequests: {[requestId: number]: ng.IDeferred<any>};

        protected lastRequestId = 0;

        static $inject = [
            "$log",
            "$q",
            "$timeout",
            "ucApiConfig"
        ];
        public constructor(
            $log: ng.ILogService,
            $q: ng.IQService,
            $timeout: ng.ITimeoutService,
            configProvider: IConfigProvider
        ) {
            this.$log = $log;
            this.$q = $q;
            this.$timeout = $timeout;

            this.configProvider = configProvider;

            this.init();
        }

        protected init() {
            this.pendingRequests = {};
        }

        public isConnected() {
            return this.ws ? this.ws.readyState == WebsocketStatus.OPEN : false;
        }

        public connect(): ng.IPromise<void> {
            this.$log.debug("Connect");

            if (this.connectRequest) {
                return this.$q.reject(new BusyException("Already connecting"));
            }

            if (this.ws && this.ws.readyState != WebsocketStatus.CONNECTING && this.ws.readyState != WebsocketStatus.CLOSED) {
                return this.$q.reject(new AlreadyConnectedException());
            }

            this.connectRequest = this.$q.defer<void>();

            this.ws = new WebSocket(this.getUrl());
            this.ws.onopen = this.onOpen.bind(this);
            this.ws.onmessage = this.onMessage.bind(this);
            this.ws.onclose = this.onClose.bind(this);

            return this.connectRequest.promise;
        }

        public disconnect(code?: number, reason?: string): ng.IPromise<void> {
            this.$log.debug("Disconnect", arguments);

            var code: number  = code || DisconnectCode.GENERAL_CODE;

            // Already disconnecting?
            if (this.disconnectRequest) {
                return this.$q.reject(new BusyException("Already disconnecting"));
            }

            // Connecting?
            if (this.ws && this.ws.readyState == WebsocketStatus.CONNECTING) {
                return this.$q.reject(new NotConnectedException());
            }

            // Already closed?
            if (this.ws && this.ws.readyState == WebsocketStatus.CLOSED) {
                return this.$q.when();
            }


            try {
                var disconnectRequest = this.$q.defer<void>();
                this.ws.close(code, reason);
            } catch (e) {
                return this.$q.reject(new Exception("Error closing websocket", undefined, e));
            }

            this.disconnectRequest = disconnectRequest;
            return disconnectRequest.promise;
        }

        public sendRequest(request: IRequest): ng.IPromise<IResponse> {
            //this.$log.debug("Send request", request);

            if (!this.isConnected()) {
                return this.$q.reject(new NotConnectedException());
            }

            if (request.id == null) {
                request.id = this.getNextRequestId();
            }

            var str = JSON.stringify(request);
            this.ws.send(str);

            return this.pushPendingRequest<IResponse>(request);
        }

        //
        // Callbacks
        //

        protected onOpen(ev: Event) {
            this.$log.debug('Connection was open.', ev);

            // After connection open, a message with 401 status could be received immediatlly, indicating that there
            // was an authentication error.
            // We need to wait a little to check if there's no 401 message, before calling connectRequest.resolve();

            this.$timeout(() => {
                // If it was not rejected, resolve it
                if (this.connectRequest) {
                    this.connectRequest.resolve();
                }
            }, 200);
        }

        protected onClose(ev: CloseEvent) {
            this.$log.debug('Connection was closed.', ev);

            if (this.connectRequest) {
                this.connectRequest.reject(new ErrorConnectingException("Connection was closed: " + ev.reason, ev.code));
                this.connectRequest = null;
            }

            if (this.disconnectRequest) {
                this.disconnectRequest.resolve();
                this.disconnectRequest = null;
            }
        }

        protected onError(ev: ErrorEvent) {
            this.$log.debug('Connection error', ev);

            if (this.connectRequest) {
                this.connectRequest.reject(new ErrorConnectingException("Connection error", undefined, ev.error));
                this.connectRequest = null;
            }

            if (this.disconnectRequest) {
                this.disconnectRequest.reject(new ErrorDisconnectingException(null, ev.error));
                this.disconnectRequest = null;
            }
        }

        protected onMessage(ev: MessageEvent) {

            var response;
            try {
                response = <IResponse|IErrorResponse> JSON.parse(ev.data);
            } catch (error) {
                throw new UnexpectedInputException("Error while trying to parse JSON message: " + ev.data, error)
            }

            //this.$log.debug("Received message", response);

            var pending: ng.IDeferred<any>;
            if (response.id && typeof response.status === "object") {
                pending = this.retrievePendingRequest(response.id);  // this.pendingRequests[response.id]
                if (!pending) {
                    this.$log.error("There's no pending request associated with received response", response);
                    return;
                }

            }
            else {
                if (this.disconnectRequest) {
                    pending = this.disconnectRequest;
                    this.disconnectRequest = null;
                }
                else if (this.connectRequest) {
                    pending = this.connectRequest;
                    this.connectRequest = null;
                }
            }

            if (pending) {
                if (typeof response.status === "number") {

                    if (response.status == ApiResponses.OK_CODE) {
                        pending.resolve(response);
                    }

                    else {
                        var exception = new UcServiceException(response.status, response.reason);
                        pending.reject(exception);
                    }
                }

                else {
                    if (response.status.code == ApiResponses.OK_CODE) {
                        pending.resolve(response);
                    }

                    else {
                        var reason = response.status.reason.join(", ");
                        var exception = new UcServiceException(response.status.code, reason);
                        pending.reject(exception);
                    }
                }
            }
        }

        //
        // Requests
        //

        protected getNextRequestId() {
            return ++ this.lastRequestId;
        }

        protected pushPendingRequest<T>(request: IRequest, callback?: Function): ng.IPromise<T> {
            var deferred = this.$q.defer<T>();
            this.pendingRequests[request.id] = deferred;
            var promise = deferred.promise;
            if (callback) {
                promise = promise.then(<any> callback);
            }
            return promise;
        }

        protected retrievePendingRequest(requestId): ng.IDeferred<any> {

            var deferred = this.pendingRequests[requestId];

            if (deferred) {
                this.pendingRequests[requestId] = null;
            }

            return deferred;
        }

        //
        // Misc
        //

        protected getUrl() {
            return this.configProvider.getWsApiEndpoint() + "?token=" + this.configProvider.getAccessToken() ;
        }
    }
}
