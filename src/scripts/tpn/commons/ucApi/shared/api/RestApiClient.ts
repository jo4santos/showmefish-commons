"use strict";

module tpn.commons.ucApi.shared.api {

    // restangular
    import IResponse    = restangular.IResponse;
    import IService     = restangular.IService;
    import IProvider    = restangular.IProvider;

    // tpn.commons.citadelApi
    import IConfigProvider = tpn.commons.ucApi.shared.config.IConfigProvider;

    export class RestApiClient {

        public static CONTENT_TYPE_TEXT_PLAIN = 'text/plain';
        public static CONTENT_TYPE_JSON = 'application/json';

        protected $log:ng.ILogService;

        protected restangular:IService;
        protected configProvider:IConfigProvider;

        public static $inject = ["$log", "Restangular", "ucApiConfig"];

        public constructor($log:ng.ILogService,
                           restangular:IService,
                           configProvider:IConfigProvider) {
            this.$log = $log;
            this.restangular = restangular;
            this.configProvider = configProvider;
        }

        //
        // Public API
        //

        public get() {
            return this.restangular.withConfig(this.config.bind(this, {}));
        }

        public getWithToken(providedToken: string) {
            return this.restangular.withConfig(this.config.bind(this, {
                token: providedToken
            }));
        }

        public getWithCredentials(username: string, password: string) {
            return this.restangular.withConfig(this.config.bind(this, {
                username: username,
                password: password
            }));
        }

        //
        // Misc
        //

        private config(options: any, provider:IProvider) {

            options = options || {};

            var headers: any = {
                'Content-Type': RestApiClient.CONTENT_TYPE_TEXT_PLAIN
            };

            if (options.username && options.password) {
                headers.username = options.username;
                headers.password = options.password;
            } else {
                headers.Authorization = 'Bearer ' + (options.token || this.configProvider.getAccessToken());
            }

            provider.setBaseUrl(this.configProvider.getRestApiEndpoint());
            provider.setDefaultHeaders(headers);
            provider.setResponseInterceptor(this.responseInterceptor.bind(this));
            provider.setErrorInterceptor(this.errorInterceptor.bind(this));

            // Move .options function to .restangularOptions in returned objects
            // @see https://github.com/mgonto/restangular/issues/266
            provider["restangularFields"].options = 'restangularOptions';
        }

        private responseInterceptor(data:IApiResponseData|string, operation:string, what:string, url:string, response:IResponse, deferred:ng.IDeferred<any>) {
            if (response.status != 200) {
                this.errorInterceptor(response, deferred, data);
            } else {
                if (typeof data === "string") {
                    return data;
                }
                else {
                    if (data.status != 0) {
                        this.errorInterceptor(response, deferred, data);
                    } else {
                        return data.data;
                    }
                }
            }
        }

        private errorInterceptor(response:IResponse, deferred:ng.IDeferred<any>, data?:IApiResponseData|string) {
            this.$log.error("Server error", response, data);

            var status:number = response.status;
            var msg:string = undefined;
            if (typeof data === "object") {
                if (data.status) {
                    status = data.status;
                }
                if (data.msg) {
                    msg = data.msg;
                }
            }

            var exception = new ApiException(status, msg);

            if (response.status == 401 || (typeof data === "object" && data.status == 401)) {
                this.$log.warn("SERVER_UNAUTHORIZED response");
            }

            deferred.reject(exception);
        }
    }

    interface IApiResponseData {
        msg: string;
        status: number;
        data: any;
    }

}