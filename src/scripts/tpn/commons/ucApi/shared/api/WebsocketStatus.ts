"use strict";

module tpn.commons.ucApi.shared.api {

    /**
     * @see https://developer.mozilla.org/pt-PT/docs/Web/API/WebSocket#Constants
     */
    export class WebsocketStatus {

        /**
         * connection not yet established
         */
        static CONNECTING = 0;

        /**
         * conncetion established
         */
        static OPEN = 1;

        /**
         * in closing handshake
         */
        static CLOSING = 2;

        /**
         * connection closed or could not open
         */
        static CLOSED = 3;
    }
}
