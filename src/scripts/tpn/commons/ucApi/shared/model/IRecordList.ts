"use strict";

module tpn.commons.ucApi.shared.model {

    export interface IRecordList<T> {
        count: number;
         records: T[];
    }
}
