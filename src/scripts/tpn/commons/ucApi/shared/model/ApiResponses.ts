"use strict";

module tpn.commons.ucApi.shared.model {

    export class ApiResponses {

        static CONNECTION_REFUSED_CODE = 0;
        static UNSPECIFIED_ERROR_CODE = 2;
        static OK_CODE = 200;
        static NO_CONTENT_CODE = 204;
        static NOT_AUTHORIZED_CODE = 401;
        static ALREADY_USED_CODE = 403;
        static NOT_FOUND_CODE = 404;

        static messages = {
            0: "Connection refused.",
            2: "Unspecified server error.",
            200: "OK",
            204: "No content returned.",
            401: "Not authorized.",
            403: "Token already used.",
            404: "Not found."
        };

        static getMessageFor(code:number) {
            return ApiResponses.messages[code];
        }
    }
}