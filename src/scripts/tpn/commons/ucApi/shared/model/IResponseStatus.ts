"use strict";

module tpn.commons.ucApi.shared.model {

    export interface IResponseStatus {
        code: number;
        reason: string[];
    }
}
