"use strict";

module tpn.commons.ucApi.shared.model {

    export class UcApiOperation {
        static RETRIEVE_CONVERSATIONS_STATUS = "retrieve";
        static MARK_MESSAGE_READ = "markread";
        static MARK_MESSAGE_DELETED = "markdeleted";
        static INITIATE_CALL = "initiate";
    }

}
