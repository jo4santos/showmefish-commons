"use strict";

module tpn.commons.ucApi.shared.model {

    export interface IErrorResponse {
        status: number;
        reason: string;
    }
}
