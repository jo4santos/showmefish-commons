"use strict";

module tpn.commons.ucApi.shared.model {

    export interface IResponse {
        id?: number;
        operation?: string;
        status: IResponseStatus;
    }
}
