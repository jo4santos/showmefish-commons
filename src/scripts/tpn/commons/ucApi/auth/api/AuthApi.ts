"use strict";

module tpn.commons.ucApi.auth.api {

    import RestApiClient = tpn.commons.ucApi.shared.api.RestApiClient;

    /**
     * API for /authenticate endpoint
     */

    export class AuthApi {

        protected $q:ng.IQService;
        protected apiClient:RestApiClient;

        public static $inject = ["$q", "tcUcRestApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:RestApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Exchanges token from warden (i.e.) for a UC token
         *
         * @param externalToken Warden token to be used to authenticate user
         * @returns {IPromise<string>}
         */
        public exchangeToken(externalToken: string): ng.IPromise<string> {

            return this.apiClient.getWithToken(externalToken)
                .one("authenticate")
                .withHttpConfig({
                    transformResponse: (data) => data
                })
                .post("")
                .then((internalToken:string)=> {
                    return internalToken;
                })
        }

        /**
         * Exhanges user credentials for a UC token
         *
         * @param username
         * @param password
         * @returns {IPromise<string>}
         */
        public exchangeCredentials(username: string, password: string): ng.IPromise<string> {

            return this.apiClient.getWithCredentials(username, password)
                .one("authenticate")
                .withHttpConfig({
                    transformResponse: (data) => data
                })
                .post("")
                .then((internalToken:string)=> {
                    return internalToken;
                });
        }
    }
}