"use strict";

module tpn.commons.ucApi.auth.service {

    import AuthApi = tpn.commons.ucApi.auth.api.AuthApi;

    /**
     * API for UC authenticate endpoint
     */
    export class AuthService {

        protected $q:ng.IQService;
        protected authApi: AuthApi;

        public static $inject = ["$q", "tcUcAuthApi"];

        public constructor($q:ng.IQService,
                           authApi:AuthApi) {
            this.$q = $q;
            this.authApi = authApi;
        }

        /**
         * Exchanges token from warden (i.e.) for a UC token
         *
         * @param externalToken Warden token to be used to authenticate user
         * @returns {IPromise<string>}
         */
        public exchangeToken(externalToken: string):ng.IPromise<string> {
            return this.authApi.exchangeToken(externalToken);
        }

        public exchangeCredentials(username: string, password: string):ng.IPromise<string> {
            return this.authApi.exchangeCredentials(username, password);
        }
    }
}