"use strict";

module tpn.commons.ucApi {

    /**
     * Register tcXmppApi module
     */
    function registerAngularModule() {
        angular
            .module("tcUcApi", [])

            // ucApiConfig (IUcApiConfigProvider) should be provided by client module

            // shared
            .service("tcUcWsApiClient", shared.api.WsApiClient)
            .service("tcUcRestApiClient", shared.api.RestApiClient)

            // auth
            .service("tcUcAuthApi", auth.api.AuthApi)
            .service("tcUcAuthService", auth.service.AuthService)

            // messageSync
            .service("tcUcMessageConverter", messageSync.converter.UcMessageConverter)
            .service("tcUcConversationUpdateConverter", messageSync.converter.UcConversationUpdateConverter)
            .service("tcUcMessageSyncApi", messageSync.api.MessageSyncApi)
            .service("tcUcMessageSyncService", messageSync.service.MessageSyncService)

            // call
            .service("tcUcCallApi", call.api.UcCallApi)
            .service("tcUcCallService", call.service.UcCallService);
    }

    angular.element(document).ready(registerAngularModule);
}

