"use strict";

module tpn.commons.ucApi.call.listener {

    export interface ICallListener {
        onCallInitiated?(number: string): void;
    }

}