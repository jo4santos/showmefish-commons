"use strict";

module tpn.commons.ucApi.call.api {

    // tpn.commons.ucApi.shared
    import UcServiceException = tpn.commons.ucApi.shared.exception.UcServiceException;
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;
    import IResponse = tpn.commons.ucApi.shared.model.IResponse;
    import ApiResponses = tpn.commons.ucApi.shared.model.ApiResponses;
    import WebsocketStatus = tpn.commons.ucApi.shared.api.WebsocketStatus;
    import WsApiClient = tpn.commons.ucApi.shared.api.WsApiClient;

    // tpn.commons.ucApi.call
    import ICallRequest = tpn.commons.ucApi.call.model.ICallRequest;

    /**
     * UC Call API
     *
     * @see http://wiki.tpn.thinkingphones.net/display/UCAPI/WebBroker+Websocket+APIs
     */
    export class UcCallApi {

        protected $log: ng.ILogService;
        protected $q: ng.IQService;
        protected wsApiClient: WsApiClient;

        static $inject = [
            "$log",
            "$q",
            "tcUcWsApiClient"
        ];
        public constructor(
            $log: ng.ILogService,
            $q: ng.IQService,
            wsApiClient: WsApiClient
        ) {
            this.$log = $log;
            this.$q = $q;
            this.wsApiClient = wsApiClient;

            this.init();
        }

        protected init() {
        }


        /**
         * Initiate a call
         *
         * @param userJid Return conversations for which user?
         * @param number Number to call
         * @returns {ng.IPromise<void>}
         * @see http://wiki.tpn.thinkingphones.net/display/UCAPI/WebBroker+Websocket+APIs#WebBrokerWebsocketAPIs-Initiate
         */
        initiateCall(userJid: string, number: string): ng.IPromise<void>  {

            var request = <ICallRequest> {
                operation: UcApiOperation.INITIATE_CALL,
                call: {
                    userId: userJid,
                    callControlSettings: {
                        extension: number
                    }
                }
            };

            return this.wsApiClient.sendRequest(request)
                .then((response: IResponse) => {
                    return;
                });
        }

    }


}