"use strict";

module tpn.commons.ucApi.call.model {

    export interface ICallNumber {
        extension: string;
    }
}