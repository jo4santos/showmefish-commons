"use strict";

module tpn.commons.ucApi.call.model {

    export interface ICallOptions {
        userId: string;
        callControlSettings: ICallNumber;
    }
}