"use strict";

module tpn.commons.ucApi.call.model {

    // tpn.commons.ucApi.shared
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;

    export interface ICallRequest extends IRequest {
        call: ICallOptions;
    }
}