"use strict";

module tpn.commons.ucApi.call.service {

    // tpn.commons.shared
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;

    // tpn.commons.ucApi.shared
    import UcServiceException = tpn.commons.ucApi.shared.exception.UcServiceException;
    import UcApiOperation = tpn.commons.ucApi.shared.model.UcApiOperation;
    import IRequest = tpn.commons.ucApi.shared.model.IRequest;
    import IResponse = tpn.commons.ucApi.shared.model.IResponse;
    import ApiResponses = tpn.commons.ucApi.shared.model.ApiResponses;
    import WebsocketStatus = tpn.commons.ucApi.shared.api.WebsocketStatus;
    import WsApiClient = tpn.commons.ucApi.shared.api.WsApiClient;

    // tpn.commons.ucApi.shared
    import IConfigProvider = tpn.commons.ucApi.shared.config.IConfigProvider;

    // tpn.commons.ucApi.call
    import UcCallApi = tpn.commons.ucApi.call.api.UcCallApi;
    import ICallListener = tpn.commons.ucApi.call.listener.ICallListener;

    /**
     * UC Call API
     *
     * @see http://wiki.tpn.thinkingphones.net/display/UCAPI/WebBroker+Websocket+APIs
     */
    export class UcCallService {

        protected $log: ng.ILogService;
        protected configProvider: IConfigProvider;
        protected ucCallApi: UcCallApi;

        // Listeners

        protected listeners: ListenerManager<ICallListener>;

        static $inject = [
            "$log",
            "ucApiConfig",
            "tcUcCallApi"
        ];
        public constructor(
            $log: ng.ILogService,
            configProvider: IConfigProvider,
            ucCallApi: UcCallApi
        ) {
            this.$log = $log;
            this.configProvider = configProvider;
            this.ucCallApi = ucCallApi;

            this.init();
        }

        protected init() {
            this.listeners = new ListenerManager<ICallListener>(this.$log);
        }

        /**
         * Initiate a call
         *
         * @param number Number to call
         * @returns {ng.IPromise<void>}
         * @see http://wiki.tpn.thinkingphones.net/display/UCAPI/WebBroker+Websocket+APIs#WebBrokerWebsocketAPIs-Initiate
         */
        initiateCall(number: string): ng.IPromise<void>  {
            var userJid = this.configProvider.getUsername();
            return this.ucCallApi.initiateCall(userJid, number).then(()=>{
                this.listeners.forEach((listener) => {
                    if (listener.onCallInitiated) {
                        listener.onCallInitiated(number);
                    }
                });
            });
        }

        //
        // Listeners
        //

        /**
         * Register a listener to listen to connection status changes
         * @param listener
         */
        public registerListener(listener: ICallListener) {
            this.listeners.addListener(listener);
        }

        /**
         * Client should remove registered connection listener if it's no longer used
         * @param listener
         * @returns boolean False if not found / removed
         */
        public unregisterListener(listener: ICallListener): boolean {
            return this.listeners.removeListener(listener);
        }

    }


}