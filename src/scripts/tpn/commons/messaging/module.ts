module tpn.commons.messaging {

    "use strict";

    /**
     * Register tpnDappMessaging module
     */
    function registerAngularModule() {

        var angularApp = angular.module("tcMessaging", [
            "tcXmppApi",
            "tcUcApi",
            "tcUnifiedContact"
        ]);

        // Should be provided by client module for tpn.commons.messaging.MessagingService:
        // "conversationDatasource"
        // "messageDatasource"

        // Messaging services
        angularApp.service('tcMessagingService', service.MessagingService);
    }

    angular.element(document).ready(registerAngularModule);
}

