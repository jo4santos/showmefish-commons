"use strict";

module tpn.commons.messaging.config {

    /**
     * Application device configuration
     */
    export interface IMessagingServiceConfigProvider {

        getMessagesPageSize(): number;
        getMessageMaxSize(): number;
        $get(): IMessagingServiceConfigProvider;
    }

}
