"use strict";

module tpn.commons.messaging.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class MessageEmptyException extends Exception {

        constructor(message:string = "Message is empty") {
            super(message, 202);
            this.name = "MessageEmptyException";
        }
    }

}