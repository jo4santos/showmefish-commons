"use strict";

module tpn.commons.messaging.exception {

    import Exception = tpn.commons.shared.exception.Exception;

    export class MessageMaxSizeException extends Exception {

        constructor(message:string = "Maximum message size hit") {
            super(message, 201);
            this.name = "MessageMaxSizeException";
        }
    }

}