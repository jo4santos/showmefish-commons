"use strict";

module tpn.commons.messaging.listener {

    // tpn.commons.shared
    import Exception = tpn.commons.shared.exception.Exception;
    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IMessage = tpn.commons.shared.messaging.model.IMessage;

    // tpn.commons.contactiveApi
    import IContactItem = tpn.commons.contactiveApi.contact.model.IContactItem;

    export interface IConversationListener {

        //
        // Conversation
        //

        /**
         * Called when a conversation was loaded
         * @param conversation
         */
        onLoadedConversation?(conversation: IConversation): void;

        /**
         * Called when the active conversation changed
         * @param conversation
         */
        onActiveConversation?(conversation: IConversation, previouslyActive?: IConversation): void;

        /**
         * Called when there was some change on conversation (will not be called for messages updates)
         * @param conversation
         */
        onUpdatedConversation?(conversation: IConversation): void;

        /**
         * Called when some conversation is removed
         * @param removedConversation
         */
        onRemovedConversation?(removedConversation: IConversation): void;

        //
        // Messages
        //

        onSentMessageError?(error: Exception, message: IMessage, conversation: IConversation): void;

        /**
         * Called on every update to messages array (sent, received, mark as read, ...)
         * @param messages
         * @param conversation
         */
        onChangedMessages?(messages: IMessage[], conversation: IConversation): void;
        onChangedMessagesError?(error: Exception, conversation: IConversation): void;

        /**
         * Called when some user is writting a message
         * @param message
         * @param conversation
         */
        onReceivedMessageWriting?(message: IMessage, conversation: IConversation): void;
    }

}