"use strict";

module tpn.commons.messaging.service {

    // tpn.commons.shared.*
    import Exception = tpn.commons.shared.exception.Exception;
    import IllegalStateException = tpn.commons.shared.exception.IllegalStateException;
    import UnexpectedInputException = tpn.commons.shared.exception.UnexpectedInputException;
    import ListenerManager = tpn.commons.shared.listener.ListenerManager;
    import NotConnectedException = tpn.commons.shared.exception.NotConnectedException;

    // tpn.commons.shared.messaging
    import IMessage = tpn.commons.shared.messaging.model.IMessage;
    import MessageType = tpn.commons.shared.messaging.model.MessageType;
    import IConversationUpdate = tpn.commons.shared.messaging.model.IConversationUpdate;
    import IConversation = tpn.commons.shared.messaging.model.IConversation;
    import IConversationDatasource = tpn.commons.shared.messaging.datasource.IConversationDatasource;
    import IMessageDatasource = tpn.commons.shared.messaging.datasource.IMessageDatasource;
    import IDatasourceConversationListener = tpn.commons.shared.messaging.listener.IDatasourceConversationListener;
    import IDatasourceMessageListener = tpn.commons.shared.messaging.listener.IDatasourceMessageListener;
    import IMessagesChanged = tpn.commons.shared.messaging.listener.IMessagesChanged;
    import IConversationQueryOptions = tpn.commons.shared.messaging.model.IConversationQueryOptions;
    import IMessageQueryOptions = tpn.commons.shared.messaging.model.IMessageQueryOptions;
    import IPeriodQueryOptions = tpn.commons.shared.messaging.model.IPeriodQueryOptions;

    // tpn.commons.xmppApi
    import IXmppMessageListener = tpn.commons.xmppApi.listener.IXmppMessageListener;
    import XmppService = tpn.commons.xmppApi.service.XmppService;
    import IPresence = tpn.commons.xmppApi.model.IPresence;

    // tpn.commons.messaging
    import IConversationListener = tpn.commons.messaging.listener.IConversationListener;
    import MessageMaxSizeException = tpn.commons.messaging.exception.MessageMaxSizeException;
    import MessageEmptyException = tpn.commons.messaging.exception.MessageEmptyException;
    import IMessagingServiceConfigProvider = tpn.commons.messaging.config.IMessagingServiceConfigProvider;

    // tpn.commons.unifiedContact
    import UnifiedContactService = tpn.commons.unifiedContact.UnifiedContactService;
    import IUnifiedContact = tpn.commons.unifiedContact.model.IUnifiedContact;
    import IUnifiedContactListener = tpn.commons.unifiedContact.listener.IUnifiedContactListener;

    // tpn.commons.ucApi.messageSync
    import MessageSyncService = tpn.commons.ucApi.messageSync.service.MessageSyncService;

    // tpn.commons.ucApi
    import IMessageSyncListener = tpn.commons.ucApi.messageSync.listener.IMessageSyncListener;

    /**
     * Allows controllers to register listeners to receive Messages/Conversations updates
     *
     * Keeps loaded conversations objects that should be used directly by controllers
     * Stores conversations and messages on local DB
     * Receives updates from MessageSyncService and updates DB and conversation objects
     * Receives messages from XmppService and updates DB and conversation objects
     */
    export class MessagingService implements IDatasourceConversationListener, IDatasourceMessageListener, IUnifiedContactListener {

        protected $log: ng.ILogService;
        protected $q: ng.IQService;
        protected $timeout: ng.ITimeoutService;

        protected messagingServiceConfigProvider: IMessagingServiceConfigProvider;

        protected xmppService: XmppService;
        protected unifiedContactService: UnifiedContactService;
        protected messageSyncService: MessageSyncService;
        protected conversationDatasource: IConversationDatasource;
        protected messageDatasource: IMessageDatasource;

        // Listeners

        protected listeners: ListenerManager<IConversationListener>;

        // Loaded conversations (with messages, participants, ...)

        protected activeConversation: IConversation;
        protected loadedConversations: IConversation[] = [];
        protected loadedContacts: IUnifiedContact[] = [];

        protected missingContactsJids: string[] = [];

        protected isLoading: boolean = false;
        protected isLoadingNeeded: boolean = false;

        // Used to prevent marking messages as read if user is not with the app open
        protected isWindowFocused: boolean = true;

        static $inject = [
            "$log",
            "$q",
            "$timeout",
            "messagingServiceConfigProvider",
            "tcXmppService",
            "tcUnifiedContactService",
            "tcUcMessageSyncService",
            "conversationDatasource",
            "messageDatasource"
        ];
        constructor(
            $log: ng.ILogService,
            $q: ng.IQService,
            $timeout: ng.ITimeoutService,
            messagingServiceConfigProvider: IMessagingServiceConfigProvider,
            xmppService: XmppService,
            unifiedContactService: UnifiedContactService,
            messageSyncService: MessageSyncService,
            conversationDatasource: IConversationDatasource,
            messageDatasource: IMessageDatasource
        ) {
            this.$log = $log;
            this.$q = $q;
            this.$timeout = $timeout;
            this.messagingServiceConfigProvider = messagingServiceConfigProvider;
            this.xmppService = xmppService;
            this.unifiedContactService = unifiedContactService;
            this.messageSyncService = messageSyncService;
            this.conversationDatasource = conversationDatasource;
            this.messageDatasource = messageDatasource;

            this.init();
        }

        protected init() {
            this.listeners = new ListenerManager<IConversationListener>(this.$log);
            this.initCallbacks();
            this.registerListeners();

            // Listen to window events to know when to show notifs
            // TODO Fuze - When to show notifications on message received
            /*var win = nwGui.Window.get();
            win.on("focus", ()=>{
                this.isWindowFocused = true;
                if(this.activeConversation && this.activeConversation.messages) {
                    this.markMessagesAsRead(this.activeConversation.messages);
                }
            });
            win.on("blur", ()=>{
                this.isWindowFocused = false;
            });*/
        }

        protected initCallbacks() {
            this.onXmppMessageReceived = this.onXmppMessageReceived.bind(this);
            this.onNewConversation = this.onNewConversation.bind(this);
            this.onUpdatedConversation = this.onUpdatedConversation.bind(this);
            this.onRemovedConversation = this.onRemovedConversation.bind(this);
            this.onChangedMessages = this.onChangedMessages.bind(this);
            this.onRemovedMessages = this.onRemovedMessages.bind(this);
            this.presenceUpdateHandler = this.presenceUpdateHandler.bind(this);
            this.onUpdatedContact = this.onUpdatedContact.bind(this);
        }

        protected registerListeners() {
            this.xmppService.addMessageListener(this.onXmppMessageReceived);
            this.conversationDatasource.registerListener(this);
            this.messageDatasource.registerListener(this);
            this.unifiedContactService.registerUnifiedContactListener(this);
        }

        /**
         * Load all conversations from DB
         *
         * @returns {ng.IPromise<void>}
         */
        loadConversations(): ng.IPromise<void> {

            // We need to make sure we don't overload or overlap the messaging service with load requests
            if(this.isLoading) {
                this.isLoadingNeeded = true;
                return this.$q.all([]).then(()=>null);
            }
            else {
                this.isLoading = true;
                this.isLoadingNeeded = false;

                var loadedConversations: IConversation[];
                return this.conversationDatasource.getConversations()
                    .then((conversations: IConversation[]) => {
                        loadedConversations = conversations;

                        var promises: ng.IPromise<void>[] = [];
                        loadedConversations.forEach((conversation: IConversation) => {
                            promises.push(this.loadConversation(conversation, false));
                        });

                        return this.$q.all(promises).then(() => {
                            this.isLoading = false;
                            if(this.isLoadingNeeded) {
                                this.loadConversations();
                            }
                        });
                    });
            }
        }

        /**
         * Load conversation transient information and add it to loadedConversations
         * On success, triggers listener.onLoadedConversation
         *
         * @param conversation
         * @param callMessageSync Call MessageSync if no messages on local DB
         * @returns {ng.IPromise<void>}
         */
        protected loadConversation(conversation: IConversation, callMessageSync: boolean = true): ng.IPromise<void> {

            conversation.messages = conversation.messages || [];
            return this.$q.all([
                this.loadMessages(conversation, callMessageSync),
                this.loadParticipants(conversation)
            ])
                .then(() => {

                    this.addLoadedContacts(conversation.participants);

                    var conversationFound = _.find(this.loadedConversations, {
                        "id": conversation.id
                    });

                    if(conversationFound) {
                        var conversationIndex = _.indexOf(this.loadedConversations, conversationFound);
                        this.loadedConversations.splice(conversationIndex, 1, conversation);
                    }
                    else {
                        this.loadedConversations.push(conversation);
                    }

                    this.listeners.forEach((listener) => {
                        if (listener.onLoadedConversation) {
                            listener.onLoadedConversation(conversation);
                        }
                    });
                });
        }

        protected addLoadedContacts(participants: IUnifiedContact[]) {
            participants.forEach((participant: IUnifiedContact)=>{
                // Check if participant already exists
                var contactFound = _.find(this.loadedContacts, {
                    "groupId": participant.groupId
                })
                if(!contactFound) {
                    this.loadedContacts.push(participant);
                }
            });
        }

        /**
         * Load participants to identified conversation
         *
         * @param conversation
         * @returns {ng.IPromise<IUnifiedContact[]>}
         */
        protected loadParticipants(conversation: IConversation = this.activeConversation): ng.IPromise<IUnifiedContact[]> {

            this.missingContactsJids = _.uniq(this.missingContactsJids.concat(conversation.participantsJids));

            return this.unifiedContactService.getUnifiedContacts({
                jid: conversation.participantsJids
            })
                .then((contacts: IUnifiedContact[]) => {

                    contacts.forEach((contact)=>{
                        contact.chat.forEach((chat)=>{
                            var index = _.indexOf(this.missingContactsJids, chat.originImId);
                            if(index != -1) {
                                this.missingContactsJids.splice(index, 1);
                            }
                        })
                    })

                    conversation.participants = contacts;
                    return conversation.participants;
                });
        }

        //
        // Getters
        //

        /**
         * Returns the active conversation
         *
         * @returns {IConversation}
         */
        getActiveConversation() { return this.activeConversation; }

        /**
         * Returns the loaded conversations
         *
         * @returns {IConversation[]}
         */
        getLoadedConversations() { return this.loadedConversations; }

        /**
         * Returns the list of contacts envolved in recent conversations
         *
         */
        getLoadedContacts() { return this.loadedContacts; }

        //
        // Conversations management
        //

        /**
         * Resumes or creates a conversation with provided information
         *
         * @param conversationId
         * @param participantsJids
         * @returns {ng.IPromise<IConversation>}
         */
        startConversation(conversationId: string, participantsJids?: string[]): ng.IPromise<IConversation> {

            // Check if it's already the active conversation
            if (this.activeConversation && this.activeConversation.id == conversationId) {
                return this.$q.when(this.activeConversation);
            }

            return this.getOrCreateConversation(<IConversation>{
                id: conversationId,
                participantsJids: participantsJids
            })
                .then((conversation) => {
                    this.setActiveConversation(conversation);
                    return conversation;
                });
        }

        //
        // Messages
        //

        /**
         * Load (more) messages into conversation conversation
         *
         * If success, listener.onLoadedMessages is called
         * If error, listener.onLoadedMessagesError is called
         *
         * - Load from local DB
         * -- IF messages.size == pageSize THEN
         * --- Add messages to conversation
         * -- ELSE
         * --- Get messages from Message Sync
         *
         * @param conversation
         * @param callMessageSync Call MessageSync if no messages on local DB
         * @returns {ng.IPromise<void>}
         */
        loadMessages(conversation: IConversation = this.activeConversation, callMessageSync: boolean = true): ng.IPromise<void> {

            var offset = conversation.messages.length;

            // Get messages from local DB
            return this.messageDatasource.getMessages({
                conversationId: conversation.id,
                offset: offset,
                limit: this.messagingServiceConfigProvider.getMessagesPageSize(),
                sort: {
                    timeSent: -1
                }
            })

                .then((storedMessages: IMessage[]) => {

                    // We don't have sufficient messages on datastore, call MessageSync
                    if (storedMessages.length < this.messagingServiceConfigProvider.getMessagesPageSize()) {

                        if (callMessageSync && conversation.ucId) {

                            var olderMessage = this.getOlderMessage(conversation.messages);
                            var olderMessageTimesent = olderMessage ? olderMessage.timeSent : undefined;

                            // Get more messages from MessageSync
                            this.requestMessagesFromMessageSync(conversation, olderMessageTimesent);
                        }
                    }

                    return storedMessages;
                })

                // Append them to conversation and notify listeners
                .then((loadedMessages: IMessage[]) => {

                    // Save new messages on conversation
                    this.updateMessagesOnLoadedConversation(conversation, loadedMessages, false);

                    // Notify listeners
                    this.listeners.forEach((listener) => {
                        if (listener.onChangedMessages) {
                            listener.onChangedMessages(conversation.messages, conversation);
                        }
                    });
                })

                // Notify listeners on error
                .catch((error: Exception) => {

                    this.listeners.forEach((listener) => {
                        if (listener.onChangedMessagesError) {
                            listener.onChangedMessagesError(error, conversation);
                        }
                    });

                });
        }

        /**
         * Request more messages from MesageSync
         * Result will come from datasource, after saved
         * @param conversation
         * @param before
         */

        protected requestMessagesFromMessageSync(conversation: IConversation, before: Date): ng.IPromise<void> {

            var conversationIds = {
                id: conversation.id,
                ucId: conversation.ucId
            };

            return this.messageSyncService.getMessages(
                conversationIds,
                null,
                before,
                this.messagingServiceConfigProvider.getMessagesPageSize()
            )

                // Notify listeners on error
                .catch((error: Exception) => {
                    this.$log.error("[MessagingService] Error ocurred trying to load additional messages from Message Sync");
                    throw error;
                });
        }

        /**
         * Send message using XmppApi and adds it to MessageDatasource
         * In error, listener.onSentMessageError is called
         *
         * @param conversation
         * @returns {ng.IPromise<IMessage>}
         */
        sendMessage(conversation: IConversation = this.activeConversation): ng.IPromise<IMessage> {

            var deferred = this.$q.defer<IMessage>();

            var message = <IMessage> {
                conversationId: conversation.id,
                to: conversation.participantsJids[0],
                body: conversation.inputText
            };

            if (!this.validateOrRejectSentMessage(conversation, message, deferred)) {
                return deferred.promise;
            }

            this.xmppService.sendMessage(message);
            this.messageDatasource.insertMessages([message])
                .then(() => {
                    conversation.inputText = "";
                    deferred.resolve(message);
                })
                .catch((error: Exception) => {

                    this.listeners.forEach((listener) => {
                        if (listener.onSentMessageError) {
                            listener.onSentMessageError(error, message, conversation);
                        }
                    });

                    return error;
                });

            return deferred.promise;
        }

        /**
         * Mark all messages before (and including) provided "message" as read
         *
         * @param messages Will mark as read all provided messages and all others before those
         * @returns {ng.IPromise<void>}
         */
        markMessagesAsRead(messages: IMessage[]): ng.IPromise<void> {

            // find older message
            var message = _.max(messages, (message: IMessage) => message.timeSent);

            var conversationUcId = message.conversationUcId;
            if (!conversationUcId) {
                var conversation = this.getLoadedConversationById(message.conversationId);
                conversationUcId = conversation.ucId;
            }

            if (!conversationUcId) {
                var error = new Exception("No conversationUcId found for associated message/conversation. Couldn't mark messages as read");
                this.$log.warn("Couldn't mark messages as read", message, error);
            }

            return this.messageSyncService.markMessagesAsRead(conversationUcId, message.timeSent)
                .then(() => {
                    return this.messageDatasource.markMessagesAdRead(message.conversationId, message.timeSent);
                })
                .catch((error) => {
                    this.$log.warn("Couldn't mark messages as read", message, error);
                    throw error;
                });
        }

        /**
         * Mark as read after 2s with conversation open (XXX: Now it's immediate)
         * @param conversation
         */
        markConversationAsReadDelayed(conversation: IConversation) {
            if (conversation.messages.length) {
                this.$timeout(() => {
                    return this.markMessagesAsRead(conversation.messages);
                }, 0);
            }
        }

        /**
         * Validates message. If not valid, rejects deferred and notifies listeners
         * On error triggers listener.onSentMessageError
         *
         * @param conversation
         * @param message
         * @param deferred
         * @returns {boolean}
         */
        protected validateOrRejectSentMessage(conversation: IConversation, message: IMessage, deferred: ng.IDeferred<any>): boolean {

            var error: Exception;

            if (!this.xmppService.isConnected()) {
                error = new NotConnectedException();
                deferred.reject(error);
            }

            else if (!message.body) {
                error = new MessageEmptyException();
                deferred.reject(error);
            }

            else if (message.body.length > this.messagingServiceConfigProvider.getMessageMaxSize()) {
                error = new MessageMaxSizeException();
                deferred.reject(error);
            }

            if (error) {
                this.listeners.forEach((listener) => {
                    if (listener.onSentMessageError) {
                        listener.onSentMessageError(error, message, conversation);
                    }
                });
            }

            return error == null;
        }

        protected updateUnreadCount(conversation: IConversation) {
            this.messageDatasource.getMessagesCount({
                conversationId: conversation.id,
                isRead: false,
                fromJids: conversation.participantsJids
            })
                .then((count: number) => {
                    if (conversation.unreadCount != count) {
                        conversation.unreadCount = count;
                        // XXX: Improvement - Update only unreadCount field
                        this.conversationDatasource.updateConversation(conversation)
                            .catch((error: Exception) => {
                                this.$log.error("Unexpected error updating count on conversation", conversation.id, error)
                            })
                    }
                })
                .catch((error: Exception) => {
                    this.$log.error("Unexpected error retrieving messages unread count", conversation.id, error)
                })
        }

        //
        // Conversation management
        //

        /**
         * Get conversation by id from loadedConversations
         *
         * @param conversationId
         * @returns {IConversation}
         */
        protected getLoadedConversationById(conversationId: string): IConversation {

            for (var i=0; i<this.loadedConversations.length; i++) {
                var conversation = this.loadedConversations[i];
                if (conversation.id == conversationId) {
                    return conversation;
                }
            }

            return null;
        }

        /**
         * Create a conversation with information provided by message
         *
         * @param message
         * @returns {ng.IPromise<IConversation>}
         */
        protected getOrCreateConversationByMessage(message: IMessage): ng.IPromise<IConversation> {
            return this.getOrCreateConversation(<IConversation>{
                id: message.conversationId,
                ucId: message.conversationUcId,
                participantsJids: [message.received ? message.from : message.to],
                messages: [],
                inputText: ""
            });
        }

        /**
         * Gets conversation from loadedConversations, from DB or creates a new one and loads it
         *
         * @param reqConversation
         * @returns {ng.IPromise<IConversation>}
         */
        protected getOrCreateConversation(reqConversation: IConversation): ng.IPromise<IConversation> {

            // Check in the other conversations
            var conversation = this.getLoadedConversationById(reqConversation.id);
            if (conversation) {
                return this.$q.when(conversation);
            }

            // Get conversation from datasource
            return this.conversationDatasource.getConversation(reqConversation.id)

                // Set found conversation or create new one
                .then((dsConversation: IConversation) => {

                    // Found conversation
                    if (dsConversation) {
                        return dsConversation;
                    }

                    // Create new one
                    else {
                        reqConversation.messages = reqConversation.messages || [];
                        return this.conversationDatasource.insertConversation(reqConversation)
                            .then(() => reqConversation);
                    }
                })

                // Load conversation
                .then((conversation: IConversation) => {
                    return this.loadConversation(conversation)
                        .then(() => conversation);
                })
        }

        /**
         * Sets conversation as active
         * On success triggers listener.onActiveConversation
         *
         * @param conversation
         */
        protected setActiveConversation(conversation: IConversation) {

            // If already active, return
            if (conversation == this.activeConversation) {
                return;
            }

            // Remove presence listeners for old conversation
            this.removePresenceListenerForActiveConversation();

            // Set new active, unset last active
            var previouslyActive = this.activeConversation;
            this.activeConversation = conversation;
            conversation.active = true;
            if (previouslyActive) {
                previouslyActive.active = false;
            }

            // Add presence listeners for old conversation
            this.addPresenceListenerForActiveConversation();

            this.$log.info("Activated conversation", conversation.id);

            this.listeners.forEach((listener) => {
                if (listener.onActiveConversation) {
                    listener.onActiveConversation(conversation, previouslyActive);
                }
            });

            this.markConversationAsReadDelayed(conversation);
        }

        //
        // Conversation - Messages
        //

        /**
         * Update conversation (creates or updates) and all associated messages
         * After updating messages, will sort them and mark as read if necessary
         *
         * @param conversationId
         * @param messages
         * @returns {any}
         */
        protected updateLoadedConversationAndMessages(conversationId: string, messages: IMessage[]) {

            var conversation: IConversation;
            var referenceMessage = _.first(messages)

            // Get or create conversation
            return this.getOrCreateConversationByMessage(referenceMessage)

                // Udpate all messages on conversation
                .then((c) => {
                    conversation = c;
                    var promises: ng.IPromise<void>[] = [];

                    // Check if
                    messages.forEach((message: IMessage) => {
                        this.$log.info("[MessagingService] Updating message", message.id, "on loaded conversation", conversation.id);

                        // Creates
                        this.updateMessageOnLoadedConversation(conversation, message, false);
                    })

                    // Sort by timeSent ASC
                    conversation.messages = _.sortBy(conversation.messages, "timeSent");

                    // Mark as read and/or update unread count
                    if (conversation == this.activeConversation && this.isWindowFocused) {
                        this.markMessagesAsRead(messages);
                    } else {
                        this.updateUnreadCount(conversation);
                    }

                    // Not ucId on conversation? Update conversation
                    if (!conversation.ucId && referenceMessage.conversationUcId) {
                        conversation.ucId = referenceMessage.conversationUcId;
                        this.conversationDatasource.updateConversation(conversation);
                    }

                    // Notify listeners
                    this.notifyListenersChangedMessages(conversation, messages);
                });
        }

        /**
         * Add/update conversation messages
         * On success, triggers listener.onChangedMessages or listener.onSentMessage (if notifyListeners)
         * On error, triggers listener.onSentMessage (if notifyListeners)
         *
         * @param conversation
         * @param message
         * @param notifyListeners
         */
        protected updateMessagesOnLoadedConversation(conversation: IConversation, messages: IMessage[], notifyListeners: boolean = true) {

            messages.forEach((message: IMessage) => {
                this.updateMessageOnLoadedConversation(conversation, message, notifyListeners);
            })
        }

        /**
         * Add/update conversation message
         * On success, triggers listener.onChangedMessages or listener.onSentMessage (if notifyListeners)
         * On error, triggers listener.onSentMessage (if notifyListeners)
         *
         * @param conversation
         * @param message
         * @param notifyListeners
         */
        protected updateMessageOnLoadedConversation(conversation: IConversation, message: IMessage, notifyListeners: boolean = true) {

            var messageFound = _.find(conversation.messages, {
                "id": message.id
            });

            // Update message if was found
            if (messageFound) {
                var index = conversation.messages.indexOf(messageFound);
                conversation.messages.splice(index, 1, message);
            }

            // Else insert message
            else {
                conversation.messages.push(message);

                // Sort by timeSent ASC
                conversation.messages = _.sortBy(conversation.messages, "timeSent");
            }

            if (notifyListeners) {
                this.notifyListenersChangedMessages(conversation, [message]);
            }
        }

        /**
         * Remove all messages in [query.from, query.to[ in conversation
         * On success, triggers listener.onChangedMessages
         *
         * @param conversation
         * @param query
         */
        protected removeMessagesOnLoadedConversation(conversation: IConversation, query: IPeriodQueryOptions) {

            var removedMessages: IMessage[] = _.remove(conversation.messages, (message: IMessage) => {
                var removeIt = false;

                // Remove if in interval [from, to[
                if (query.from && query.to &&
                    query.from >= message.timeSent &&
                    query.to < message.timeSent) {
                    removeIt = true;
                }

                // Remove if in interval [from, inf.[
                else if (query.from && query.from >= message.timeSent) {
                    removeIt = true;
                }

                // Remove if in interval ]inf., to[
                else if (query.to && query.to < message.timeSent) {
                    removeIt = true;
                }

                return removeIt;
            });

            if (removedMessages.length) {
                this.listeners.forEach((listener) => {
                    if (listener.onChangedMessages) {
                        listener.onChangedMessages(removedMessages, conversation);
                    }
                });
            }
        }

        //
        // Presences
        //

        //
        // Users presences
        //

        protected addPresenceListenerForActiveConversation() {
            if (!this.activeConversation) {
                return;
            }

            this.activeConversation.participants.forEach((participant: IUnifiedContact) => {
                if (participant.chat.length) {
                    this.unifiedContactService.registerJidPresenceHandler(participant.chat[0].originImId, this.presenceUpdateHandler);
                }
            });
        }

        protected removePresenceListenerForActiveConversation() {
            if (!this.activeConversation) {
                return;
            }

            this.activeConversation.participants.forEach((participant: IUnifiedContact) => {
                if (participant.chat.length) {
                    this.unifiedContactService.deregisterJidPresenceHandler(participant.chat[0].originImId, this.presenceUpdateHandler);
                }
            });
        }

        protected presenceUpdateHandler(presence: IPresence) {
            if (!this.activeConversation) {
                return;
            }

            for (var index in this.activeConversation.participants) {
                var participant = this.activeConversation.participants[index];
                if (participant.chat.length && participant.chat[0].originImId == presence.from) {
                    participant.presence = presence;

                    // Notify listeners
                    this.listeners.forEach((listener) => {
                        if (listener.onUpdatedConversation) {
                            listener.onUpdatedConversation(this.activeConversation);
                        }
                    });

                    return;
                }
            }
        }

        //
        // IUnifiedContactListener
        //

        onUpdatedContact(unifiedContact:IUnifiedContact):void {
            if (unifiedContact.chat) {

                var found: boolean = false;
                unifiedContact.chat.forEach((chat) => {
                    if(this.missingContactsJids.indexOf(chat.originImId) != -1) {
                        found = true;
                    }
                });

                if (found) {
                    this.loadedConversations.forEach((conversation)=> {

                        var found: boolean = false;
                        unifiedContact.chat.forEach((chat) => {
                            if(conversation.participantsJids.indexOf(chat.originImId) != -1) {
                                found = true;
                            }
                        });

                        if(found) {
                            this.loadParticipants(conversation)
                                .then((participants)=> {
                                    this.addLoadedContacts(participants);
                                    // Notify listeners
                                    this.listeners.forEach((listener) => {
                                        if (listener.onLoadedConversation) {
                                            listener.onLoadedConversation(conversation);
                                        }
                                    });
                                });
                        }
                    })
                }
            }
        }

        //
        // IDatasourceConversationListener
        //

        /**
         * After DB conversation creation, loads conversation into loadedConversations
         *
         * @param newConversation
         */
        onNewConversation(newConversation: IConversation): void {
            this.$log.info("[MessagingService] New conversation from datasource", newConversation.id);

            this.loadConversation(newConversation)
                .catch((error) => {
                    this.$log.error("Couldn't load conversation", newConversation, error);
                })
        }

        /**
         * After DB conversation update, updates conversation on loadedConversations
         * On success, triggers listener.onUpdatedConversation
         *
         * @param updatedConversation
         */
        onUpdatedConversation(updatedConversation: IConversation): void {
            this.$log.info("[MessagingService] Updated conversation from datasource", updatedConversation.id);

            var conversation = this.getLoadedConversationById(updatedConversation.id);

            if (!conversation) {
                this.$log.warn("[MessagingService] Unexpected updating a not loaded conversation, loading it", conversation);
                this.loadConversation(updatedConversation)
                    .catch((error) => {
                        this.$log.error("Couldn't load conversation", updatedConversation, error);
                    })
            }

            else {
                this.applyConversationUpdate(conversation, updatedConversation);

                this.listeners.forEach((listener) => {
                    if (listener.onUpdatedConversation) {
                        listener.onUpdatedConversation(conversation);
                    }
                });
            }
        }

        /**
         * After DB conversation removed, removes conversation from loadedConversations
         * On success, triggers listener.onRemovedConversation
         *
         * @param removedConversation
         */
        onRemovedConversation(removedConversation: IConversation): void {
            this.$log.info("[MessagingService] Remmoved conversation from datasource", removedConversation.id);

            var conversation = this.getLoadedConversationById(removedConversation.id);

            if (!conversation) {
                this.$log.warn("[MessagingService] Unexpected removing a not loaded conversation.", conversation);
            }

            else {
                // Remove from loadedConversations
                var idx = this.loadedConversations.indexOf(conversation);
                if (idx >= 0) {
                    this.loadedConversations.splice(idx, 1);
                }

                // Remove as activeConversation
                if (this.activeConversation == conversation) {
                    this.activeConversation = null;
                }

                // Call listeners
                this.listeners.forEach((listener) => {
                    if (listener.onRemovedConversation) {
                        listener.onRemovedConversation(conversation);
                    }
                });
            }
        }

        //
        // IDatasourceMessageListener
        //

        /**
         * After DB message creation, loads message into loadedConversation
         *
         * @param message
         */
        onChangedMessages(messagesChanged: IMessagesChanged): void {
            this.$log.info("[MessagingService] New messages updated from datasource");

            var promises: ng.IPromise<void>[] = [];

            // Create an array with all changed messages
            var messages: IMessage[] = [];
            messages = messages.concat(messagesChanged.inserted || []);
            messages = messages.concat(messagesChanged.updated || []);

            // Update messages per conversation
            var messagesByConversation: IMessagesByConversationIndex = {};
            messages.forEach((message) => {
                if (!messagesByConversation[message.conversationId]) {
                    messagesByConversation[message.conversationId] = [];
                }
                messagesByConversation[message.conversationId].push(message);
            });
            _.forEach(messagesByConversation, (messages: IMessage[], conversationId: string) => {
                promises.push(this.updateLoadedConversationAndMessages(conversationId, messages));
            });
        }

        /**
         * After marked messages as read, mark loadedMessages also as read
         * On success, triggers listener.onChangedMessages
         *
         * @param conversationId Mark messages on this conversation
         * @param toInc Mark as read to this date, inclusivelly
         * @param numMarked Number of messages marked as read
         */
        onMarkAsRead(conversationId: string, toInc: Date, numMarked: number) {
            this.$log.info("[MessagingService] Marked messages as read on conversation", conversationId);

            var conversation = this.getLoadedConversationById(conversationId);
            if (conversation) {
                var updated = [];

                conversation.messages.forEach((message: IMessage) => {
                    if (message.timeSent <= toInc) {
                        message.isRead = true;
                        updated.push(message);
                    }
                });

                this.listeners.forEach((listener) => {
                    if (listener.onChangedMessages) {
                        listener.onChangedMessages(updated, conversation);
                    }
                });

                this.updateUnreadCount(conversation)
            }

        }

        /**
         * After DB messages removed, removes messages between query dates
         *
         * @param query
         * @param numDeleted
         */
        onRemovedMessages(query: IMessageQueryOptions, numDeleted: number): void {
            this.$log.debug("[MessagingService] Removed messages from datasource", query);

            var conversation = this.getLoadedConversationById(query.conversationId);
            if (conversation) {
                this.removeMessagesOnLoadedConversation(conversation, query);
            }
        }

        //
        // IXmppMessageListener
        //

        /**
         * On XMPP message received, updates message or triggers writting listener
         * On message type IS_WRITTING, triggers listener.onReceivedMessageWriting
         *
         * @param message
         */
        onXmppMessageReceived(message: IMessage) {

            // Is writing a message
            if (message.type == MessageType.IS_WRITING) {

                // For IS_WRITTING, check only on loaded conversations
                var conversation = this.getLoadedConversationById(message.conversationId);

                this.listeners.forEach((listener) => {
                    if (listener.onReceivedMessageWriting) {
                        listener.onReceivedMessageWriting(message, conversation);
                    }
                });

            }

        }

        //
        // Listeners
        //

        /**
         * Register listener
         * @param listener
         */
        registerListener(listener: IConversationListener) {
            this.listeners.addListener(listener);
        }

        /**
         * Deregister listener
         * @param listener
         */
        deregisterListener(listener: IConversationListener) {
            this.listeners.removeListener(listener);
        }

        protected notifyListenersChangedMessages(conversation: IConversation, messages: IMessage[]) {

            this.listeners.forEach((listener) => {

                if (listener.onChangedMessages) {
                    listener.onChangedMessages(messages, conversation);
                }

            });
        }

        //
        // Misc
        //

        /**
         * Get the older message
         *
         * @param messages
         * @returns {IMessage}
         */
        protected getOlderMessage(messages: IMessage[]): IMessage {
            if (!messages) return null;

            var message: IMessage = _.min(messages, (message: IMessage) => {
                return message.timeSent;
            });

            return message;
        }

        /**
         * Merge changes from update into local conversation
         *
         * @param conversation
         * @param updatedConversation
         */
        protected applyConversationUpdate(conversation: IConversation, updatedConversation: IConversation) {
            conversation.ucId = conversation.ucId || updatedConversation.ucId;
            conversation.threads = conversation.threads || updatedConversation.threads;
            conversation.type = conversation.type || updatedConversation.type;
            conversation.subject = conversation.subject || updatedConversation.subject;
            conversation.participantsJids = conversation.participantsJids || updatedConversation.participantsJids;
        }
    }
    
    interface IMessagesByConversationIndex {
        [conversationId: string]: IMessage[];
    }
}
