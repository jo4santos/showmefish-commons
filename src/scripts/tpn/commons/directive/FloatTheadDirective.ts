"use strict"

module tpn.commons.directive {

    /**
     * Fixed table header:
     *
     * <div class="scrollable-container">
     *   <table tc-float-thead=".scrollable-container">...</table>
     * </div>
     *
     * @see http://mkoryak.github.io/floatThead/
     */
    export var FloatTheadDirectiveFactory = FloatTheadDirective;
    export function FloatTheadDirective () {

        return {
            restrict: 'A',
            link: function($scope:ng.IScope, element, attrs)
            {
                window.requestAnimationFrame(createFloatThead);

                function createFloatThead() {

                    // Create fixed table header
                    element.floatThead({
                        scrollContainer: function ($table) {
                            var $container = $table.closest(attrs.tcFloatThead);
                            return $container;
                        }
                    });

                    // Update on ui-layout resize
                    $("body").on("uiLayoutResize", reflowFloatThead);

                    // Update on data loaded
                    $scope.$on("dataLoaded", reflowFloatThead);
                }

                function reflowFloatThead() {
                    element.floatThead("reflow");
                }
            }
        };

    }
}
