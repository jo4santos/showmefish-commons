"use strict";

module tpn.commons.directive {

    /**
     * Confirms on exit
     *
     *  <form tc-confirm-on-exit>...</form>
     */
    export var ConfirmOnExitDirectiveFactory = [ConfirmOnExitDirective];
    function ConfirmOnExitDirective() {
        return {
            restrict: 'A',
            require: 'form',
            link: function($scope, $formElement, attrs, $formController) {

                var message = "Are you sure you want to cancel this activity?\nAll the information entered will be lost";

                $(window).on('beforeunload', confirmExit);
                $(window).on('hide.bs.modal', confirmExit);
                $scope.$on('$stateChangeStart', confirmExit);

                function confirmExit(event) {
                    console.log("confirmExit", arguments);
                    if ($formController.$dirty) {
                        if (!confirm(message)) {
                            event.preventDefault();
                        }
                    }
                }
            }
        };
    }
}
