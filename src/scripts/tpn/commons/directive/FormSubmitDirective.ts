"use strict"

module tpn.commons.directive {

    /**
     * Similar to ng-submit form directive, with submit buttons loading state and form disable support
     * (using callbacks that return promises)
     *
     * While promise is not resolved:
     * # Adds an k-submitting CSS class (tc-submitting-class attribute)
     * # Disables all form inputs (tc-disable-while-submitting attribute)
     *
     * Limitations:
     * # When promise is resolved, it enables all inputs (whether or not they were disabled before submit)
     *
     * Usage:
     * <form tc-form-submit="submitReturnPromise()">...</form>
     *
     */
    export var FormSubmitDirectiveFactory = ["$parse", FormSubmitDirective];
    function FormSubmitDirective (
        $parse: ng.IParseService
    ) {
        var SUBMITTING_DEFAULT_CLASS_NAME = "tc-submitting";
        return {
            restrict: 'A',
            require: 'form',
            link: ($scope, $formElement, attrs, $formController) => {

                var submittingClass = attrs.tcSubmittingClass || SUBMITTING_DEFAULT_CLASS_NAME;
                var disableWhileSubmitting = attrs.tcDisableWhileSubmitting !== undefined ? attrs.tcDisableWhileSubmitting : true;
                var fn = $parse(attrs.tcFormSubmit);

                /**
                 * Disable all form inputs
                 * Puts submit buttons in loading state
                 */
                var disableForm = () => {
                    $formElement.find("input, select, button").attr("disabled", true);
                    $formElement.find("input[type='submit'], button[type='submit']").each((i, elem) => {
                        $(elem)["button"]("loading");
                    });
                };

                /**
                 * Enables all form inputs
                 * Resets submit buttons loading state
                 */
                var enableForm = () => {
                    // TODO: Should evaluate ng-disabled (if exists) or restore original disabled status
                    $formElement.find("input, select, button").removeAttr("disabled");
                    $formElement.find("input[type='submit'], button[type='submit']").each((i, elem) => {
                        $(elem)["button"]("reset");
                    });
                };

                /**
                 * Enables ans disables form during promise loading
                 * @param promise
                 */
                var changeFormStatus = (promise: ng.IPromise<any>) => {

                    if (promise && promise.finally)
                    {
                        if (disableWhileSubmitting)
                            disableForm();

                        $formElement.addClass(submittingClass);

                        promise.finally(() => {
                            $formElement.removeClass(submittingClass);
                            enableForm();
                        })
                    }
                };

                $formElement.bind('submit', (event: ng.IAngularEvent) => {
                    // if form is not valid cancel it.
                    if (!$formController.$valid) return false;

                    var promise = fn($scope, {$event:event});
                    console.debug("Got promise from form submit", promise);

                    changeFormStatus(promise);
                });
            }
        };
    }
}
