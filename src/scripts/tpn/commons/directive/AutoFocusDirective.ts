"use strict"

module tpn.commons.directive {

    /**
     * Autofocus elements inside dynamically loaded templates.
     *
     * Usage:
     * <input type="text" tc-auto-focus>
     *
     * @see https://gist.github.com/mlynch/dd407b93ed288d499778
     */
    export var AutoFocusDirectiveFactory = ["$timeout", AutoFocusDirective];
    function AutoFocusDirective (
        $timeout: ng.ITimeoutService
    ) {
        return {
            restrict: 'A',
            link : function($scope, $element) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        };
    }
}
