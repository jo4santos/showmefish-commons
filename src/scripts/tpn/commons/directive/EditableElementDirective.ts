"use strict";

module tpn.commons.directive {

    /**
     * Enables ng-model on content-editable elements
     *
     * <any tc-editable-element content-editable ng-model="ctrl.model"></any>
     *
     * @see http://fdietz.github.io/recipes-with-angular-js/common-user-interface-patterns/editing-text-in-place-using-html5-content-editable.html
     */
    export var EditableElementDirectiveFactory = [EditableElementDirective];
    function EditableElementDirective () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function(scope, element, attrs, ngModel) {

                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || "");
                };

                element.bind("blur keyup change", function() {
                    scope.$apply(read);
                });
            }
        }
    }
}
