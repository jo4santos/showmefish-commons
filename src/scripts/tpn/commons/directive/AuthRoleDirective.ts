"use strict"

module tpn.commons.directive {

    import AuthEvents = tpn.commons.shared.model.auth.AuthEvents;
    import IUserSession = tpn.commons.shared.model.auth.IUserSession;

    /**
     * Hide items that user hasn't access:
     *
     * <any tc-auth-role="ADMIN">Element only showed to admin</any>
     *
     */
    export var AuthRoleDirectiveFactory = ["$rootScope", "tcUserSession", AuthRoleDirective];
    function AuthRoleDirective(
        $rootScope: ng.IRootScopeService,
        userSession: IUserSession
    ) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                function refreshVisibility() {
                    if (userSession.hasUserRole(attrs.tcAuthRole)) {
                        element.show();
                    } else {
                        element.hide();
                    }
                }

                $rootScope.$on(AuthEvents.LOGGED_IN, refreshVisibility);
                $rootScope.$on(AuthEvents.LOGGED_OUT, refreshVisibility);

                refreshVisibility();
            }
        };
    }
}
