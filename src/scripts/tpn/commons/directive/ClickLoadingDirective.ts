"use strict"

module tpn.commons.directive {

    /**
     * Similar to ng-click directive, turning the element into loading status (tc-loading class and disabled)
     * (using callbacks that return promises)
     *
     * While promise is not resolved:
     * # Adds an loading CSS class (tc-loading-class attribute, defaults to tc-loading)
     * # Disables input (tc-disable-while-loading attribute, defaults to true)
     * # Changes text to loading (if tc-loading-text defined)
     *
     * Limitations:
     * # When promise is resolved, it enables the element (whether or not it were disabled before submit)
     *
     * Usage:
     * <ANY tc-click="opReturnPromise()"
     *      tc-loading-class="loading"
     *      tc-disable-while-loading="false"
     *      tc-loading-text="Changing..."
     *      >...</ANY>
     *
     */
    export var ClickLoadingDirectiveFactory = ["$parse", ClickLoadingDirective];
    function ClickLoadingDirective (
        $parse: ng.IParseService
    ) {
        var SUBMITTING_DEFAULT_CLASS_NAME = "tc-loading";
        return {
            restrict: 'A',
            link: ($scope, $element, attrs) => {

                var loadingClass = attrs.tcLoadingClass || SUBMITTING_DEFAULT_CLASS_NAME;
                var loadingText = attrs.tcLoadingText;
                var disableWhileLoading = attrs.tcDisableWhileLoading !== undefined ? attrs.tcDisableWhileLoading : true;
                var fn = $parse(attrs.tcClick);

                var savedHref;
                var savedText;

                /**
                 * Disable element
                 */
                var disableElement = () => {

                    $element.attr("disabled", true);
                    $element.addClass("disabled");

                    savedHref = $element.attr("href");
                    $element.removeAttr("href");

                    if (loadingText) {
                        savedText = $element.html();
                        $element.html(loadingText);
                    }
                };

                /**
                 * Enable element
                 */
                var enableElement = () => {
                    $element.removeAttr("disabled");
                    $element.removeClass("disabled");

                    if (savedHref) {
                        $element.attr("href", savedHref);
                    }

                    if (savedText) {
                        $element.html(savedText);
                    }
                };

                /**
                 * Enables ans disables element during promise loading
                 * @param promise
                 */
                var changeElementStatus = (promise: ng.IPromise<any>) => {

                    if (promise && promise.finally)
                    {
                        if (disableWhileLoading)
                            disableElement();

                        $element.addClass(loadingClass);

                        promise.finally(() => {
                            $element.removeClass(loadingClass);
                            enableElement();
                        })
                    }
                };

                $element.bind('click', (event: ng.IAngularEvent) => {

                    var promise = $scope.$apply(() => {
                        return fn($scope, {$event:event});
                    });
                    console.debug("Got promise from form submit", promise);

                    changeElementStatus(promise);
                });
            }
        };
    }
}
