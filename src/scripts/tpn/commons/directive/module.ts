module tpn.commons.directive {

    "use strict";

    /**
     * Register tcActiveLink module
     */
    function registerAngularModule() {
        angular
            .module("tcDirectives", ["ngAnimate"])
            .constant("tcLocationHashPrefix", "!")
            .directive("tcAuthRole", AuthRoleDirectiveFactory)
            .directive("tcAutoFocus", AutoFocusDirectiveFactory)
            .directive("tcClick", ClickLoadingDirectiveFactory)

            .directive("tcCollapseAutoClose", CollapseAutoCloseDirectiveFactory)
            .directive("tcConfirmOnExit", ConfirmOnExitDirectiveFactory)
            .directive("tcFloatThead", FloatTheadDirectiveFactory)

            .directive("tcButtonClick", ButtonClickDirectiveFactory)
            .directive("tcFormSubmit", FormSubmitDirectiveFactory)
            .directive("tcHighlightOnChange", HighlightOnChangeDirectiveFactory)
            .directive("tcLoading", LoadingDirectiveFactory)

            .directive("tcEditableElement", EditableElementDirectiveFactory)

            .directive("tdOnload", OnloadDirectiveFactory)

            .directive("tcFocusOn", FocusOnDirectiveFactory)
            .directive("tcTriggerFocusOn", TriggerFocusOnDirectiveFactory)
    }

    angular.element(document).ready(registerAngularModule);
}
