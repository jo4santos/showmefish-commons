"use strict"

module tpn.commons.directive {

    /**
     * Enables calling angular defined methods elements which trigger the load event
     * Example for this is an iframe.
     *
     * When the load event is triggered, the callback is called with the element that triggered the event.
     *
     * Usage:
     * <iframe td-onload="Controller.myCallback"/>
     *
     * Callback declaration:
     * myCallback = (element:HTMLIFrameElement) => {...}
     */
    export var OnloadDirectiveFactory = [OnloadDirective];
    function OnloadDirective () {
        return {
            restrict: "A",
            scope: {
                callback: "&tdOnload"
            },
            link: ($scope, $element, attrs) => {
                var expressionHandler = $scope.callback();
                $element.on("load", () => {
                    expressionHandler($element);
                });
            }
        };
    }
}
