"use strict"

module tpn.commons.directive {

    /**
     * Sets focus on a different element when a configurable event is met.
     *
     * The event can be a click or mouseover, etc .. And defaults to click
     * When no target is set, the directive searches for the first input below it and focuses on it
     * If a parent is set, an element is only focused if it's below that parent
     *
     * Usages:
     * <div tc-trigger-focus-on><input /></div>
     * <div tc-trigger-focus-on="'#my-input'"></div>
     * <div tc-trigger-focus-on="'.my-input'" tc-trigger-focus-on-target="'.my-input-parent'" tc-trigger-focus-on-event="'click'"></div>
     *
     */
    export var TriggerFocusOnDirectiveFactory = [TriggerFocusOnDirective];

    function TriggerFocusOnDirective() {
        return {
            restrict: "A",
            scope: {
                focusTarget: "=tcTriggerFocusOn",
                focusEvent: "=tcTriggerFocusOnEvent",
                focusParent: "=tcTriggerFocusOnParent"
            },
            link: ($scope, $element:JQuery) => {

                var focusEvent:string = $scope.focusEvent || "click";

                $element.bind(focusEvent, function () {
                    var focusTarget:JQuery = null;
                    var focusParent:JQuery = null;

                    // If there's no target defined, focus on the closest input below the element
                    if (!$scope.focusTarget) {
                        focusTarget = $element.find("input,textarea,*[contenteditable=true]").eq(0);
                    }

                    // If a parent is defined, we'll focus on the target which is contained by the parent
                    else if ($scope.focusTarget && $scope.focusParent) {
                        focusParent = $($scope.focusParent);
                        focusTarget = focusParent.find($scope.focusTarget).eq(0);
                    }

                    else {
                        focusTarget = $($scope.focusTarget);
                    }

                    focusTarget.focus();
                });
            }
        };
    }
}