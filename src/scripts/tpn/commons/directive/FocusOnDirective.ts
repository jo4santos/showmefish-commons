"use strict"

module tpn.commons.directive {

    /**
     * Sets focus on element when a condition is met. Useful for cross-application interactions
     *
     * Usage:
     * <input td-focus-on="Controller.myFlag"/>
     *
     */
    export var FocusOnDirectiveFactory = [FocusOnDirective];
    function FocusOnDirective () {
        return {
            restrict: "A",
            scope: {
                focusValue: "=tcFocusOn"
            },
            link: ($scope, $element, attrs) => {
                $scope.$watch("focusValue", (currentValue, previousValue) => {
                    if (currentValue === true && !previousValue) {
                        $element[0].focus();
                    } else if (currentValue === false && previousValue) {
                        $element[0].blur();
                    }
                })
            }
        };
    }
}