"use strict"

module tpn.commons.directive {

    /**
     * Closes collapse on ESC on some not prevented click and plain links
     * Used to auto close navbar collapse menu.
     *
     * Usage:
     * <any tc-collapse-auto-close>...</any>
     *
     * TODO: [JS] [PERFORMANCE] Should bind only when the element is opened and unbind when is closed
     */
    export var CollapseAutoCloseDirectiveFactory = [CollapseAutoCloseDirective];
    function CollapseAutoCloseDirective() {

        var targetAttribute = "data-target";
        var toggleSelector = $.fn.collapse.Constructor.DEFAULTS.trigger + "[tc-collapse-auto-close]";
        var collapsedClass = 'collapsed';

        $(document).on('keydown', closeNavCollapse.bind(this));
        $(document).on('click', closeNavCollapse.bind(this));

        function closeNavCollapse(e) {

            // Exit if is prevent and is not a plain link
            if (e.isDefaultPrevented() && !isPlainLink(e)) return;

            // If keydown event, only close on ESC key
            if (e.type == "keydown" && e.keyCode != 27) return;

            $(toggleSelector).each((index, element) => {
                var $element       = $(element);
                var relatedTarget  = $element.attr(targetAttribute);
                var $relatedTarget = $(relatedTarget);

                // It's already closed. Do nothing
                if ($element.hasClass(collapsedClass)) return;

                // Else, close it
                $relatedTarget["collapse"]('hide');
            });
        }

        function isPlainLink(e) {
            var $target:any = $(e.target);

            // Find nearest link
            var $link;
            if ($target.is("a")) {
                $link = $target;
            } else {
                $link = $target.closest("a");
            }

            // Return false if there is no link
            if (!$link.length)
                return false;

            // Return false if it is a toggle
            if ($link.attr("dropdown-toggle") !== undefined || $link.hasClass(".navbar-toggle")) {
                return false;
            }

            // Is a plain link
            return true;
        }

        return {
            restrict: 'A'
        };
    }
}
