"use strict"

module tpn.commons.directive {

    /**
     * Highlights model values changes:
     *
     * <span tc-highlight-on-change="value">{{value}}</span>
     *
     * Needs also some CSS:
     *  .tc-changed.tc-changed-add, .tc-changed.tc-changed-remove { transition: all 1s; -webkit-transition: all 1s ease-in-out; }
     *  .tc-changed.tc-changed-add { background-color: yellow; }
     *
     * @see https://groups.google.com/d/msg/angular/xZptsb-NYc4/YH35m39Eo2wJ
     * @see http://stackoverflow.com/a/20056060/198787
     */
    export var HighlightOnChangeDirectiveFactory = ["$animate", "$timeout", HighlightOnChangeDirective];
    function HighlightOnChangeDirective (
        $animate:ng.animate.IAnimateService,
        $timeout:ng.ITimeoutService
    ){
        var clazz = 'tc-changed';

        function onValueChange(element, nv, ov) {
            if (ov !== undefined) {
                $timeout(() => {
                    $animate.addClass(element, clazz).then(() => {
                        $animate.removeClass(element, clazz);
                    });
                });
            }
        }

        return {
            restrict: 'A',
            link: function ($scope:ng.IScope, element, attrs) {
                $scope.$watch(attrs.tcHighlightOnChange, onValueChange.bind(this, element));
            }
        };
    }
}
