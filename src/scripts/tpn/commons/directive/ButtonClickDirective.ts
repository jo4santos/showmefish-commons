"use strict"

module tpn.commons.directive {

    /**
     * Similar to ng-click directive, with submit buttons loading state and button disable support
     * (using callbacks that return promises)
     *
     * While promise is not resolved:
     * # Adds an tc-submitting CSS class (tc-submitting-class attribute)
     * # Disables button (tc-disable-while-submitting attribute)
     *
     * Limitations:
     * # When promise is resolved, it enables all inputs (whether or not they were disabled before submit)
     *
     * Usage:
     * <button tc-button-click="submitReturnPromise()">...</button>
     *
     */
    export var ButtonClickDirectiveFactory = ["$parse", ButtonClickDirective];
    function ButtonClickDirective (
        $parse: ng.IParseService
    ) {
        var SUBMITTING_DEFAULT_CLASS_NAME = "tc-submitting";
        return {
            restrict: 'A',
            link: ($scope, $btnElement, attrs) => {

                var submittingClass = attrs.tcSubmittingClass || SUBMITTING_DEFAULT_CLASS_NAME;
                var disableWhileSubmitting = attrs.tcDisableWhileSubmitting !== undefined ? attrs.tcDisableWhileSubmitting : true;
                var fn = $parse(attrs.tcButtonClick);

                /**
                 * Disable button and set loading state
                 */
                var disableButton = () => {
                    $btnElement.attr("disabled", true);
                    $btnElement["button"]("loading");
                };

                /**
                 * Enable button and unset loading state
                 * Resets submit buttons loading state
                 */
                var enableButton = () => {
                    // TODO: Should evaluate ng-disabled (if exists) or restore original disabled status
                    $btnElement.removeAttr("disabled");
                    $btnElement["button"]("reset");
                };

                /**
                 * Enables and disables button during promise loading
                 * @param promise
                 */
                var changeButtonStatus = (promise: ng.IPromise<any>) => {

                    if (promise && promise.finally)
                    {
                        if (disableWhileSubmitting)
                            disableButton();

                        $btnElement.addClass(submittingClass);

                        promise.finally(() => {
                            $btnElement.removeClass(submittingClass);
                            enableButton();
                        })
                    }
                };

                $btnElement.bind('click', (event: ng.IAngularEvent) => {

                    var promise = fn($scope, {$event:event});
                    console.debug("Got promise from click", promise);

                    changeButtonStatus(promise);
                });
            }
        };
    }
}
