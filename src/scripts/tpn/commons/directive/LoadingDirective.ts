"use strict"

module tpn.commons.directive {

    /**
     * Adds loading text to an element when "tc-loading" gives true and disabled the button
     *
     * Usage:
     * <any tc-loading="expression" data-loading-text="Loading...">...</any>
     *
     * @see http://plnkr.co/edit/K58w8rLqRXUk5Xg7X8d2
     */
    export var LoadingDirectiveFactory = [LoadingDirective];
    function LoadingDirective() {
        return function(scope, element, attrs) {
            return scope.$watch(function() {
                    return scope.$eval(attrs.tcLoading);
                },
                function(loading) {
                    if (loading) {
                        element.attr("disabled", true);
                        element.addClass("tc-loading");
                        return element.button("loading");
                    }
                    element.removeClass("tc-loading");
                    element.button("reset");
                    element.removeAttr("disabled");
                });
        };
    }
}
