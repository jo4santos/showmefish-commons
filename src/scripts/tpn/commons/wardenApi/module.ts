module tpn.commons.wardenApi {

    "use strict";

    /**
     * Register WardenApi module
     */
    function registerAngularModule() {
        angular
            .module("tcWardenApi", ["restangular"])

            .service("tcWardenApiClient", api.ApiClient)

            .service("tcWardenApiUsersApi", users.api.UsersApi)

            .service("tcWardenApiWardenService", service.WardenService)
    }

    angular.element(document).ready(registerAngularModule);
}

