"use strict";

module tpn.commons.wardenApi.users.api {

    import UrlEncodedUtils              = tpn.commons.shared.util.UrlEncodedUtils;

    import IUserAuthorizationPresenter  = tpn.commons.wardenApi.users.model.IUserAuthorizationPresenter;
    import ApiClient                    = tpn.commons.wardenApi.api.ApiClient;

    /**
     * API for /users/me/storage endpoint
     */

    export class UsersApi {

        protected $q:ng.IQService;
        protected apiClient:ApiClient;

        public static $inject = ["$q", "tcWardenApiClient"];

        public constructor($q:ng.IQService,
                           apiClient:ApiClient) {
            this.$q = $q;
            this.apiClient = apiClient;
        }

        /**
         * Get's the login type (basic or delegated) from warden based on userKey
         *
         * @params userKey
         * @returns {IPromise<IGetAuthOptionsResponse>}
         */
        public getAuthOptions(userKey:string):ng.IPromise<IGetAuthOptionsResponse> {
            return this.apiClient.get().all("users")
                .one(userKey)
                .one("auth")
                .one("options")
                .get()
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }

        /**
         * Gets a token for a user without redirecting
         *
         * @param optionId Identifies if it's basic or delegated login
         * @param userKey
         * @param password
         * @returns {IPromise<IUserAuthorizationPresenter>}
         */
        public postAuthOptions(optionId:string,userKey:string,password: string):ng.IPromise<IUserAuthorizationPresenter> {
            return this.apiClient.get().all("users")
                .one(userKey)
                .one("auth")
                .one("options")
                .post(optionId,{userKey:userKey,password:password},{},ApiClient.CONTENT_TYPE_APPLICATION_JSON)
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }

        /**
         * Gets a token for a user without redirecting
         * Because of the portal, all redirect methods have a different content-type: form-urlencoded
         *
         * @param optionId Identifies if it's basic or delegated login
         * @param userKey
         * @param redirectUri URI to which the delegated service will redirect after authentication
         * @returns {IPromise<IPostAuthOptionsRedirectUriResponse>}
         */
        public postAuthOptionsRedirectUri(optionId:string,userKey:string,redirectUri: string):ng.IPromise<IPostAuthOptionsRedirectUriResponse> {
            return this.apiClient.get().all("users")
                .one(userKey)
                .one("auth")
                .one("options")
                .one(optionId)
                .one("redirect")
                .post("uri",UrlEncodedUtils.toQuery({userKey:userKey,redirectUri:redirectUri}),{},ApiClient.CONTENT_TYPE_FORM_URL_ENCODED)
                .then((data:any)=> {
                    if (data) return data.plain();
                })
        }
    }
}