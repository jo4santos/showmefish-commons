"use strict";

module tpn.commons.wardenApi.users.api {

    export interface IPostAuthOptionsRedirectUriResponse {
        redirectUri: string;
    }
}