"use strict";

module tpn.commons.wardenApi.users.api {

    import IAuthOption = tpn.commons.wardenApi.users.model.IAuthOption;

    export interface IGetAuthOptionsResponse {
        options: IAuthOption[];
    }
}