"use strict";

module tpn.commons.wardenApi.users.model {

    export interface IAccessGrant {

        /**
         * Time until which a client can store this token and use it without getting a new grant.
         * After that date is advised to get another grant, as data might have changed.
         * Failing to get a new grant after this date does not lead to a token being invalid.
         */
        cacheUntil?: number;

        /**
         * Encoded token that can be used to perform requests
         */
        token?: string;
    }

}

