"use strict";

module tpn.commons.wardenApi.users.model {

    export interface IOrigin {

        /**
         * Name of the origin = ['warden', 'portal', 'okta']
         */
        name?: OriginName;

        /**
         * Unique identifier within the origin
         */
        id?: string;
    }

}

