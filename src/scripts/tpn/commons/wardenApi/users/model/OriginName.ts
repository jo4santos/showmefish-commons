"use strict";

module tpn.commons.wardenApi.users.model {

    export class OriginName {
        public static WARDEN: string = "warden";
        public static PORTAL: string = "portal";
        public static OKTA: string = "okta";
    }

}

