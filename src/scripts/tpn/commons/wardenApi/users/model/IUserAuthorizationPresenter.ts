"use strict";

module tpn.commons.wardenApi.users.model {

    export interface IUserAuthorizationPresenter {

        /**
         * The grant awarded to the entity
         */
        grant?: IAccessGrant;

        /**
         * The authorized user
         */
        entity?: IUser;

        /**
         * The grant awarded to the entity
         */
        type?: string;
    }

}

