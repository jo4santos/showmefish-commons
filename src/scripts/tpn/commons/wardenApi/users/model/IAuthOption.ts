"use strict";

module tpn.commons.wardenApi.users.model {

    export interface IAuthOption
    {
        optionId: string;

        /**
         * @see IAuthOptionMode
         */
        mode: string;
        originName: string;
    }

}