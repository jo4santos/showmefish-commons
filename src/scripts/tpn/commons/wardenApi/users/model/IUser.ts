"use strict";

module tpn.commons.wardenApi.users.model {

    import IStringKeyable = tpn.commons.shared.model.IStringKeyable;

    export interface IUser {

        /**
         * Keys that identify the user in warden
         */
        userKeys?: string[];

        /**
         * Entity where the user comes from
         */
        origin: IOrigin;

        /**
         * The tenant this user is linked to
         */
        tenantId: number;

        /**
         * Whether the user is active or not
         */
        active?: boolean;

        /**
         * Unique tenant key
         */
        tenantKey?: string;
    }

}

