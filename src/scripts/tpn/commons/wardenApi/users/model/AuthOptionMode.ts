"use strict";

module tpn.commons.wardenApi.users.model {

    export class AuthOptionMode {
        static BASIC: string = "basic";
        static DELEGATED: string = "delegated";
    }

}