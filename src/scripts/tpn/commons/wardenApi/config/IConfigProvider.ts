"use strict";

module tpn.commons.wardenApi.config {

    export interface IConfigProvider extends ng.IServiceProvider {
        getAppToken():string;
        getApiEndpoint():string;
    }
}
