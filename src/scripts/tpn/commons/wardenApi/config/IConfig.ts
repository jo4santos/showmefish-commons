"use strict";

module tpn.commons.wardenApi.config {
    export interface IConfig {
        apiEndpoint: string;
        appToken: string;
        redirectUri: string;
        callbackOriginUri: string;
    }
}
