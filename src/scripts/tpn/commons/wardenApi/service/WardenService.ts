"use strict";

module tpn.commons.wardenApi.service {

    import IAuthOption                          = tpn.commons.wardenApi.users.model.IAuthOption;
    import IUserAuthorizationPresenter          = tpn.commons.wardenApi.users.model.IUserAuthorizationPresenter;
    import IGetAuthOptionsResponse              = tpn.commons.wardenApi.users.api.IGetAuthOptionsResponse;
    import IPostAuthOptionsRedirectUriResponse  = tpn.commons.wardenApi.users.api.IPostAuthOptionsRedirectUriResponse;
    import UsersApi                             = tpn.commons.wardenApi.users.api.UsersApi;

    export class WardenService {

        protected usersApi:UsersApi;

        public static $inject = ["tcWardenApiUsersApi"];

        public constructor(usersApi:UsersApi) {
            this.usersApi = usersApi;
        }

        /**
         * Get's the login type (basic or delegated) from warden based on userKey
         *
         * @params userKey
         * @returns {IPromise<IGetAuthOptionsResponse>}
         */
        public getAuthOption(userKey: string):ng.IPromise<IAuthOption> {
            return this.usersApi.getAuthOptions(userKey).then((response:IGetAuthOptionsResponse) => {
                // FIXME: How will we determine the option to select? Why are there multiple ? Is there some kind of priorities ?
                var authOption: IAuthOption;
                authOption = response.options[0];
                return authOption;
            });
        }

        /**
         * Gets a token for a user without redirecting
         *
         * @param optionId Identifies if it's basic or delegated login
         * @param userKey
         * @param password
         * @returns {IPromise<IUserAuthorizationPresenter>}
         */
        public getBasicToken(optionId:string, userKey: string, password: string):ng.IPromise<IUserAuthorizationPresenter> {
            return this.usersApi.postAuthOptions(optionId,userKey,password).then((response:IUserAuthorizationPresenter) => {
                return response;
            });
        }

        /**
         * Gets a token for a user without redirecting
         * Because of the portal, all redirect methods have a different content-type: form-urlencoded
         *
         * @param optionId Identifies if it's basic or delegated login
         * @param userKey
         * @param redirectUri URI to which the delegated service will redirect after authentication
         * @returns {IPromise<IPostAuthOptionsRedirectUriResponse>}
         */
        public getDelegatedUri(optionId:string, userKey: string, redirectUri: string):ng.IPromise<IPostAuthOptionsRedirectUriResponse> {
            return this.usersApi.postAuthOptionsRedirectUri(optionId,userKey,redirectUri).then((response:IPostAuthOptionsRedirectUriResponse) => {
                return response;
            });
        }
    }
}