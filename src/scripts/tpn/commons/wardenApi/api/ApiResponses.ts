"use strict";

module tpn.commons.wardenApi.api {

    export class ApiResponses {

        static CONNECTION_REFUSED_CODE = 0;
        static UNSPECIFIED_ERROR_CODE = 2;
        static OK_CODE = 200;
        static NO_CONTENT_CODE = 204;
        static NOT_AUTHORIZED_CODE = 401;
        static NOT_FOUND_CODE = 404;
        static USERNAME_ALREADY_EXISTS_CODE = 1002;
        static UNAUTHORIZED_USER_CODE = 1003;
        static INVALID_TOKEN_CODE = 1004;
        static UNKNOWN_ENTITY_CODE = 1006;
        static FAILED_AUTHORIZATION_CODE = 1013;
        static INVALID_PARAMETER_CODE = 1023;
        static UNHANDLED_EXCEPTION_CODE = 1025;
        static SERVICE_ALREADY_CONNECTED_CODE = 1031;
        static PHONE_VERIFICATION_CODE = 1055;
        static NO_PHONE_AVAILABLE_CODE = 1063;
        static PHONE_ALREADY_VERIFIED_CODE = 1064;
        static NEED_TO_CHANGE_PASSWORD_CODE = 1083;
        static UNAUTHORIZED_SIGNUP_CODE = 1084;
        static NEED_TO_VERIFY_EMAIL_CODE = 1090;
        static KEY_EXISTS_CODE = 1092;

        static messages = {
            0: "Connection refused.",
            2: "Unspecified server error.",
            200: "OK",
            204: "No content returned.",
            401: "Not authorized.",
            404: "Not found.",
            1002: "The specified e-mail is already taken.",
            1003: "Invalid e-mail and password combination.",
            1004: "Invalid token provided.",
            1006: "The specified e-mail does not exist.",
            1013: "This service does not support credential authentication.",
            1023: "One of the signup fields is missing.",
            1025: "Unknown error occurred, please try again",
            1031: "The specified account is already being used by another user.",
            1055: "The verification code is incorrect.",
            1063: "No phone numbers are available in the area code.",
            1064: "I'm sorry, it looks like this phone number is already registered with another email address.",
            1083: "You must change your password to use the application.",
            1084: "Your organization is not yet enabled to use Contactive Enterprise.",
            1090: "You must verify your email to use the application.",
            1092: "User already exists."
        };

        static getMessageFor(code:number) {
            return ApiResponses.messages[code];
        }
    }
}