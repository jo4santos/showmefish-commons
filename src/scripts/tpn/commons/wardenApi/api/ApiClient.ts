module tpn.commons.wardenApi.api {

    "use strict";

    // restangular
    import IResponse    = restangular.IResponse;
    import IService     = restangular.IService;
    import IProvider    = restangular.IProvider;

    // tpn.commons.wardenApi
    import IConfigProvider = tpn.commons.wardenApi.config.IConfigProvider;

    export class ApiClient {

        public static CONTENT_TYPE_FORM_URL_ENCODED = {'Content-Type': 'application/x-www-form-urlencoded'};
        public static CONTENT_TYPE_APPLICATION_OCTET_STREAM = {'Content-Type': 'application/octet-stream'};
        public static CONTENT_TYPE_APPLICATION_JSON = {'Content-Type': 'application/json'};

        protected $log:ng.ILogService;

        protected restangular:IService;
        protected configProvider:IConfigProvider;

        public static $inject = ["$log", "Restangular", "wardenApiConfig"];

        public constructor($log:ng.ILogService,
                           restangular:IService,
                           configProvider:IConfigProvider) {
            this.$log = $log;
            this.restangular = restangular;
            this.configProvider = configProvider;
        }

        //
        // Public API
        //

        public get() {
            return this.restangular.withConfig(this.config.bind(this));
        }

        //
        // Misc
        //

        private config(provider:IProvider) {
            var token = this.configProvider.getAppToken();

            provider.setBaseUrl(this.configProvider.getApiEndpoint());
            provider.setDefaultHeaders({
                'Authorization': 'Bearer ' + token,
                'X-Long-Encoding': 'string'
            });
            provider.setResponseInterceptor(this.responseInterceptor.bind(this));
            provider.setErrorInterceptor(this.errorInterceptor.bind(this));

            // Move .options function to .restangularOptions in returned objects
            // @see https://github.com/mgonto/restangular/issues/266
            provider["restangularFields"].options = 'restangularOptions';
        }

        private responseInterceptor(data:IApiResponseData, operation:string, what:string, url:string, response:IResponse, deferred:ng.IDeferred<any>) {
            if (response.status != 200) {
                this.errorInterceptor(response, deferred, data);
            }
            else {
                if (data.status && data.status != 0) {
                    this.errorInterceptor(response, deferred, data);
                }
                else if(!data.data) {
                    return data;
                }
                else {
                    return data.data;
                }
            }
        }

        private errorInterceptor(response:IResponse, deferred:ng.IDeferred<any>, data?:IApiResponseData) {
            this.$log.error("Server error", response, data);

            var status:number = response.status;
            var msg:string = undefined;
            if (data) {
                if (data.status) {
                    status = data.status;
                }
                if (data.msg) {
                    msg = data.msg;
                }
            }

            var exception = new ApiException(status, msg);

            if (response.status == 401 || (data && data.status == 401)) {
                this.$log.warn("SERVER_UNAUTHORIZED response");
            }

            deferred.reject(exception);
        }
    }

    interface IApiResponseData {
        msg: string;
        status: number;
        data: any;
    }

}